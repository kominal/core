import { Document, model, Schema, Types } from 'mongoose';

export interface Role {
	_id?: Types.ObjectId;
	tenantId?: Types.ObjectId;
	name: string;
	permissions: string[];
	default: boolean;
}

export const RoleDatabase = model<Document & Role>(
	'Role',
	new Schema(
		{
			tenantId: Types.ObjectId,
			name: String,
			permissions: [String],
			default: Boolean,
		},
		{ minimize: false }
	)
);

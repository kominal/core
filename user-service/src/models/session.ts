import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export interface Session {
	userId: string;
	keyHash: string;
	device: string;
	created: Date;
	lastActive: Date;
}

export const SessionDatabase = model<Document & Session>(
	'Session',
	new Schema(
		{
			userId: Types.ObjectId,
			keyHash: {
				type: String,
				unique: true,
			},
			device: String,
			created: Date,
			lastActive: Date,
		},
		{ minimize: false }
	)
);

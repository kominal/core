import { Document, model, Schema } from '@kominal/lib-node-mongodb-interface';

export interface User {
	_id?: string;
	email: string;
	passwordHash: string;
	verified: boolean;
	token: string | undefined;
	roles: string[];
}

export const UserDatabase = model<Document & User>(
	'User',
	new Schema(
		{
			email: {
				type: String,
				unique: true,
			},
			passwordHash: String,
			verified: Boolean,
			token: String,
			roles: [{ type: Schema.Types.ObjectId, ref: 'Role' }],
		},
		{ minimize: false }
	)
);

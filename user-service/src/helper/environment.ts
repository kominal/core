import { getEnvironmentBoolean, getEnvironmentString } from '@kominal/lib-node-environment';

export const SALT = getEnvironmentString('SALT');
export const JWT_KEY = getEnvironmentString('JWT_KEY');

export const ENABLE_EMAIL_VERIFICATION = getEnvironmentBoolean('ENABLE_EMAIL_VERIFICATION', true);
export const ENABLE_MULTI_TENANT = getEnvironmentBoolean('ENABLE_MULTI_TENANT', false);

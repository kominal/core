import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { info, startObserver } from '@kominal/observer-node-client';
import { emailRequestVerifyRouter } from './routes/email/requestVerify';
import { emailVerifyRouter } from './routes/email/verify';
import { passwordChangeRouter } from './routes/password/change';
import { passwordRequestPasswordRouter } from './routes/password/requestReset';
import { passwordResetRouter } from './routes/password/reset';
import { roleRouter } from './routes/role';
import { sessionRouter } from './routes/session';
import { tenantsRouter } from './routes/tenants';
import { userListRouter } from './routes/user/list';
import { permissionsRouter } from './routes/user/permissions';
import { userRegisterRouter } from './routes/user/register';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start(): Promise<void> {
	startObserver();
	mongoDBInterface = new MongoDBInterface('user-service');
	await mongoDBInterface.connect();
	expressRouter = new ExpressRouter({
		healthCheck: async (): Promise<boolean> => true,
		routes: [
			emailRequestVerifyRouter,
			emailVerifyRouter,
			passwordChangeRouter,
			passwordRequestPasswordRouter,
			passwordResetRouter,
			sessionRouter,
			userListRouter,
			permissionsRouter,
			userRegisterRouter,
			roleRouter,
			tenantsRouter,
		],
	});
	await expressRouter.start();
}
start();

process.on('SIGTERM', async () => {
	info("Received system signal 'SIGTERM'. Shutting down service...");
	expressRouter?.getServer()?.close();
	await mongoDBInterface?.disconnect();
});

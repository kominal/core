import { sendMail } from '@kominal/lib-node-mailer';
import { getTranslation } from '@kominal/papyrus-node-client';

export function generateToken(length: number): string {
	let result = '';
	const characters = '0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i += 1) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

export async function sendWelcomeMail(to: string, baseUrl: string, userId: string, token: string): Promise<void> {
	try {
		const text = await getTranslation('en', 'transl.mail.emailVerification.body', {
			BASE_URL: baseUrl,
			USER_ID: userId,
			TOKEN: token,
		});
		const subject = await getTranslation('en', 'transl.mail.emailVerification.subject', {
			BASE_URL: baseUrl,
			USER_ID: userId,
			TOKEN: token,
		});

		return sendMail({
			subject,
			to,
			text,
		});
	} catch (e) {
		console.log(`sendWelcomeEmail cause Error(${e})`);
		throw new Error(e);
	}
}

export async function sendPasswordResetMail(to: string, baseUrl: string, userId: string, token: string): Promise<void> {
	const text = await getTranslation('en', 'transl.mail.passwordReset.body', {
		BASE_URL: baseUrl,
		USER_ID: userId,
		TOKEN: token,
	});
	const subject = await getTranslation('en', 'transl.mail.passwordReset.subject', {
		BASE_URL: baseUrl,
		USER_ID: userId,
		TOKEN: token,
	});
	return sendMail({
		subject,
		to,
		text,
	});
}

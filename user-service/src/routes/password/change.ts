import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { hash } from 'bcryptjs';
import { SALT } from '../../helper/environment';
import { UserDatabase } from '../../models/user';

export const passwordChangeRouter = new Router();

passwordChangeRouter.putAsUser<never, { currentPassword: string; newPassword: string }, never, never>(
	'/password/change',
	async (req, res, userId) => {
		verifyParameter(['currentPassword', 'newPassword'], req.body);
		const { currentPassword, newPassword } = req.body;

		const passwordHash = await hash(currentPassword, SALT);

		const user = await UserDatabase.findOne({ _id: userId, passwordHash }).orFail(new Error('transl.error.login.failed'));

		await user.updateOne({ passwordHash: await hash(newPassword, SALT) });

		return { statusCode: 200 };
	}
);

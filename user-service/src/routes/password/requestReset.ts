import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { generateToken, sendPasswordResetMail } from '../../helper';
import { UserDatabase } from '../../models/user';

export const passwordRequestPasswordRouter = new Router();

passwordRequestPasswordRouter.postAsGuest<never, { baseUrl: string; email: string }, never, never>(
	'/password/requestReset',
	async (req, res) => {
		verifyParameter(['email'], req.body);
		const { baseUrl } = req.body;

		const email = req.body.email.toLowerCase();

		const user = await UserDatabase.findOne({ email });

		if (!user) {
			return { statusCode: 200 };
		}

		if (!user.verified) {
			throw new Error('transl.error.email.notVerified');
		}

		const token = generateToken(16);

		await user.updateOne({ token });

		await sendPasswordResetMail(email, baseUrl, user._id, token);

		return { statusCode: 200 };
	}
);

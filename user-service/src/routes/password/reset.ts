import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { hash } from 'bcryptjs';
import { SALT } from '../../helper/environment';
import { UserDatabase } from '../../models/user';

export const passwordResetRouter = new Router();

passwordResetRouter.postAsGuest<never, { userId: string; token: string; password: string }, never, never>(
	'/password/reset',
	async (req, res) => {
		verifyParameter(['userId', 'token', 'password'], req.body);
		const { userId, token, password } = req.body;

		const passwordHash = await hash(password, SALT);
		await UserDatabase.updateOne({ _id: userId, token, verified: true }, { token: undefined, passwordHash });

		return { statusCode: 200 };
	}
);

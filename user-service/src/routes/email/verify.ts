import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { hash } from 'bcryptjs';
import { SALT } from '../../helper/environment';
import { UserDatabase } from '../../models/user';
import { createSession } from '../session';

export const emailVerifyRouter = new Router();

emailVerifyRouter.postAsGuest<never, { userId: string; token: string; key?: string; device?: string }, never, never>(
	'/email/verify',
	async (req, res) => {
		verifyParameter(['userId', 'token'], req.body);
		const { userId, token, key, device } = req.body;

		await UserDatabase.findOne({ _id: userId, token, verified: false }).orFail(new Error('transl.error.email.alreadyVerified'));

		await UserDatabase.updateOne({ _id: userId, token, verified: false }, { token: undefined, verified: true });

		if (key && device) {
			const keyHash = await hash(key, SALT);
			await createSession(userId, keyHash, device);
		}

		return { statusCode: 200 };
	}
);

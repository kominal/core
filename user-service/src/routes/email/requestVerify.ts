import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { generateToken, sendWelcomeMail } from '../../helper';
import { UserDatabase } from '../../models/user';

export const emailRequestVerifyRouter = new Router();

emailRequestVerifyRouter.postAsGuest<never, { baseUrl: string; email: string }, never, never>('/email/requestVerify', async (req, res) => {
	verifyParameter(['baseUrl', 'email'], req.body);
	const { baseUrl } = req.body;

	const email = req.body.email.toLowerCase();

	const user = await UserDatabase.findOne({ email });

	if (!user) {
		return { statusCode: 200 };
	}

	if (user.verified) {
		throw new Error('transl.error.email.alreadyVerified');
	}

	const token = generateToken(16);

	await user.updateOne({ token });

	await sendWelcomeMail(email, baseUrl, user._id, token);

	return { statusCode: 200 };
});

import { Router } from '@kominal/lib-node-express-router';
import { applyPaginationWithMapping, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { RoleDatabase } from '../../models/role';
import { User, UserDatabase } from '../../models/user';

export const userListRouter = new Router();

type UserInformation = { _id: string; email: string };

userListRouter.getAsUser<never, PaginationResponse<UserInformation>, PaginationRequest & { tenantId?: string }>(
	'/users',
	async (req, res, userId) => {
		let filter: any;
		if (req.query.tenantId) {
			const roleIds = (await RoleDatabase.find({ tenantId: req.query.tenantId }, '_id')).map((r) => r._id);
			filter = { roles: { $in: roleIds } };
		}

		const responseBody = await applyPaginationWithMapping<User, UserInformation>(
			UserDatabase,
			req.query,
			async (user) => ({
				_id: user._id,
				email: user.email,
			}),
			{
				filter,
			}
		);

		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

userListRouter.getAsUser<{ _id: string }, { _id: string; email: string }, never>('/users/:_id', async (req) => {
	const user = await UserDatabase.findById(req.params._id).orFail(new Error('transl.error.notFound.user'));

	return {
		statusCode: 200,
		responseBody: {
			_id: user._id,
			email: user.email,
		},
	};
});

userListRouter.getAsUser<{ email: string }, { _id: string; email: string }, never>('/users/findByEmail/:email', async (req) => {
	const user = await UserDatabase.findOne({ email: req.params.email }).orFail(new Error('transl.error.notFound.user'));

	return {
		statusCode: 200,
		responseBody: {
			_id: user._id,
			email: user.email,
		},
	};
});

userListRouter.deleteAsUser<{ userId: string }, never, { tenantId: string }>(
	'/users/:userId',
	async (req, res, userId) => {
		if (req.params.userId === userId) {
			throw new Error('transl.error.lockOutPrevented');
		}

		if (req.query.tenantId) {
			const roleIds = (await RoleDatabase.find({ tenantId: req.query.tenantId }, '_id')).map((r) => r._id);
			await UserDatabase.updateOne({ _id: req.params.userId }, { $pullAll: { roles: roleIds } });
		} else {
			await UserDatabase.deleteOne({ _id: req.params.userId });
		}

		return {
			statusCode: 200,
		};
	},
	{
		permissions: ['core.user-service.write'],
	}
);

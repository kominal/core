import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { RoleDatabase } from '../../models/role';
import { UserDatabase } from '../../models/user';

export const permissionsRouter = new Router();

permissionsRouter.getAsUser<{ _id: string }, string[], { tenantId?: string }>(
	'/user/:_id/permissions',
	async (req, res, userId) => {
		const permissions: string[] = [];

		const roleIds = (await RoleDatabase.find({ tenantId: req.query.tenantId }, '_id')).map((r) => String(r._id));

		const user = await UserDatabase.findById(req.params._id).orFail(new Error('transl.error.notFound.user'));

		for (const role of await RoleDatabase.find({ _id: { $in: user.roles.find((r) => roleIds.includes(String(r))) } })) {
			for (const permission of role.permissions) {
				if (!permissions.includes(permission)) {
					permissions.push(permission);
				}
			}
		}

		return { statusCode: 200, responseBody: permissions };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

permissionsRouter.putAsUser<{ _id: string }, { permissions: string[] }, never, { tenantId?: string }>(
	'/user/:_id/permissions',
	async (req, res, userId) => {
		try {
			verifyParameter(['permissions'], req.body);

			if (req.params._id === userId && !req.body.permissions.includes('core.user-service.write')) {
				throw new Error('transl.error.lockOutPrevented');
			}

			await RoleDatabase.updateOne(
				{ tenantId: req.query.tenantId, name: `Role for ${req.params._id}` },
				{ permissions: req.body.permissions, default: false },
				{ upsert: true }
			);

			const role = await RoleDatabase.findOne({ tenantId: req.query.tenantId, name: `Role for ${req.params._id}` }).orFail(
				new Error('transl.error.notFound.role')
			);

			await UserDatabase.updateOne({ _id: req.params._id, roles: { $nin: role._id } }, { $push: { roles: role._id } });

			return { statusCode: 200 };
		} catch (e) {
			console.log(e);
			throw e;
		}
	},
	{
		permissions: ['core.user-service.write'],
	}
);

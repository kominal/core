import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { hash } from 'bcryptjs';
import { generateToken, sendWelcomeMail } from '../../helper';
import { ENABLE_EMAIL_VERIFICATION, ENABLE_MULTI_TENANT, SALT } from '../../helper/environment';
import { RoleDatabase } from '../../models/role';
import { UserDatabase } from '../../models/user';

export const userRegisterRouter = new Router();

userRegisterRouter.postAsGuest<never, { baseUrl: string; email: string; password: string }, { verifyEmail: boolean }, never>(
	'/user/register',
	async (req) => {
		verifyParameter(['baseUrl', 'email', 'password'], req.body);
		const { baseUrl, password } = req.body;

		const email = req.body.email.toLowerCase();

		const regexp = new RegExp(
			// eslint-disable-next-line
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		);

		if (!email.match(regexp)) {
			throw new Error('transl.error.email.invalid');
		}

		if (await UserDatabase.exists({ email })) {
			throw new Error('transl.error.email.inUse');
		}
		const passwordHash = await hash(password, SALT);

		const token = generateToken(16);
		const user = await UserDatabase.create({
			email,
			passwordHash,
			verified: !ENABLE_EMAIL_VERIFICATION,
			token,
			roles: ENABLE_MULTI_TENANT ? [] : (await RoleDatabase.find({ default: true })).map((p) => p._id),
		});

		if (!ENABLE_MULTI_TENANT && (await UserDatabase.countDocuments()) === 1) {
			await RoleDatabase.updateOne(
				{ name: `Role for ${user._id}` },
				{ permissions: ['core.user-service.write'], default: false },
				{ upsert: true }
			);

			const role = await RoleDatabase.findOne({ name: `Role for ${user._id}` }).orFail(new Error('transl.error.notFound.role'));

			await UserDatabase.updateOne({ _id: user._id, roles: { $nin: role._id } }, { $push: { roles: role._id } });
		}

		if (ENABLE_EMAIL_VERIFICATION) {
			await sendWelcomeMail(email, baseUrl, user._id, token);
		}

		return { statusCode: 200, responseBody: { verifyEmail: ENABLE_EMAIL_VERIFICATION } };
	}
);

import { Router } from '@kominal/lib-node-express-router';
import { RoleDatabase } from '../models/role';
import { UserDatabase } from '../models/user';

export const tenantsRouter = new Router();

tenantsRouter.deleteAsService<{ tenantId: string }, never, never>('/tenants/:tenantId', async (req, res) => {
	const roleIds = (await RoleDatabase.find({ tenantId: req.params.tenantId }, '_id')).map((r) => String(r._id));

	await UserDatabase.updateMany({ roles: { $in: roleIds } }, { $pullAll: { roles: roleIds } });

	await RoleDatabase.deleteMany({ _id: { $in: roleIds } });

	return {
		statusCode: 200,
	};
});

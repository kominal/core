import { ComponentType } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MissingTranslationHandler, TranslateLoader, TranslateModule, TranslateParser, TranslateService } from '@ngx-translate/core';
import {
	CHANGE_PASSWORD_REDIRECT_URL,
	CORE_BASE_URL,
	CORE_ENVIRONMENT_ID,
	CORE_PROJECT_ID,
	CORE_TENANT_ID,
	DEFAULT_LANGUAGE,
	GUEST_HOME_URL,
	LANGUAGES,
	SERVICE_NAME,
	SINGLE_TENANT_MODE,
	SNACK_BAR_COMPONENT,
	SNACK_BAR_LOG_LEVEL,
	USER_HOME_URL,
	USE_BROWSER_LANGUAGE,
} from './core/classes/injection-tokens';
import { CoreMissingTranslationHandler } from './core/classes/missing-translation-handler';
import { TranslationInitFactory } from './core/classes/translation-factory';
import { TranslationLoader } from './core/classes/translation-loader';
import { TranslationPaginatorIntl } from './core/classes/translation-paginator';
import { PermissionsDirective } from './core/directives/permissions/permissions.directive';
import { GuestGuard } from './core/guards/guest/guest.guard';
import { UserGuard } from './core/guards/user/user.guard';
import { AuthenticationInterceptor } from './core/interceptors/authentication.interceptor';
import { PermissionsPipe } from './core/pipes/permissions/permissions.pipe';
import { Level } from './core/services/analytics.service';
import { RouterService } from './core/services/router.service';
import { SnackbarComponent } from './modules/components/snackbar/snackbar.component';
import { SnackbarModule } from './modules/components/snackbar/snackbar.module';
import { MaterialModule } from './modules/material/material.module';

export const translateModuleRoot = TranslateModule.forRoot({
	loader: {
		provide: TranslateLoader,
		useClass: TranslationLoader,
		deps: [HttpClient, Injector],
	},
	missingTranslationHandler: {
		provide: MissingTranslationHandler,
		useClass: CoreMissingTranslationHandler,
		deps: [HttpClient, Injector],
	},
	extend: true,
	isolate: false,
});

export function registerTranslationModule(): ModuleWithProviders<TranslateModule> {
	return TranslateModule.forRoot({
		loader: {
			provide: TranslateLoader,
			useClass: TranslationLoader,
			deps: [HttpClient, Injector],
		},
		missingTranslationHandler: {
			provide: MissingTranslationHandler,
			useClass: CoreMissingTranslationHandler,
			deps: [HttpClient, Injector],
		},
		extend: true,
		isolate: false,
	});
}

@NgModule({
	declarations: [PermissionsDirective, PermissionsPipe],
	imports: [CommonModule, HttpClientModule, MaterialModule, SnackbarModule],
	exports: [PermissionsDirective, PermissionsPipe],
})
export class CoreAngularClientModule {
	public static forRoot(config?: {
		coreBaseUrl?: string;
		coreTenantId?: string;
		coreProjectId?: string;
		coreEnvironmentId?: string;
		serviceName?: string;
		snackBarComponent?: ComponentType<any> | false;
		guestHomeUrl?: string;
		userHomeUrl?: string;
		changePasswordRedirectUrl?: string;
		rolesEnabled?: boolean;
		singleTenantMode?: boolean;
		languages?: string[];
		defaultLanguage?: string;
		useBrowserLanguage?: string;
		snackBarLogLevel?: Level[];
	}): ModuleWithProviders<CoreAngularClientModule> {
		return {
			ngModule: CoreAngularClientModule,
			providers: [
				{ provide: CORE_BASE_URL, useValue: config?.coreBaseUrl || 'https://core.kominal.app' },
				{ provide: CORE_TENANT_ID, useValue: config?.coreTenantId },
				{ provide: CORE_PROJECT_ID, useValue: config?.coreProjectId },
				{ provide: CORE_ENVIRONMENT_ID, useValue: config?.coreEnvironmentId },
				{ provide: SERVICE_NAME, useValue: config?.serviceName },
				{ provide: SNACK_BAR_COMPONENT, useValue: config?.snackBarComponent || SnackbarComponent },
				{ provide: GUEST_HOME_URL, useValue: config?.guestHomeUrl },
				{ provide: USER_HOME_URL, useValue: config?.userHomeUrl },
				{ provide: CHANGE_PASSWORD_REDIRECT_URL, useValue: config?.changePasswordRedirectUrl },
				{ provide: SINGLE_TENANT_MODE, useValue: config?.singleTenantMode },
				{ provide: LANGUAGES, useValue: config?.languages },
				{ provide: DEFAULT_LANGUAGE, useValue: config?.defaultLanguage },
				{ provide: USE_BROWSER_LANGUAGE, useValue: config?.useBrowserLanguage },
				{ provide: SNACK_BAR_LOG_LEVEL, useValue: config?.snackBarLogLevel },
				UserGuard,
				GuestGuard,
				PermissionsPipe,
				PermissionsDirective,
				AuthenticationInterceptor,
				{
					provide: HTTP_INTERCEPTORS,
					useClass: AuthenticationInterceptor,
					multi: true,
				},
				{
					provide: APP_INITIALIZER,
					useFactory: TranslationInitFactory,
					deps: [TranslateService, LANGUAGES, DEFAULT_LANGUAGE, USE_BROWSER_LANGUAGE, RouterService],
					multi: true,
				},
				{
					provide: MatPaginatorIntl,
					useClass: TranslationPaginatorIntl,
					deps: [TranslateService, TranslateParser],
				},
			],
		};
	}
}

import { Clipboard } from '@angular/cdk/clipboard';
import { Directive, HostListener, Input } from '@angular/core';
import { AnalyticsService } from '../../core/services/analytics.service';

@Directive({
	selector: '[clipboard]',
})
export class ClipboardDirective {
	@Input()
	public clipboard!: string;

	public constructor(private cdkClipboard: Clipboard, private analyticsService: AnalyticsService) {}

	@HostListener('click')
	public onClick(): void {
		if (this.cdkClipboard.copy(this.clipboard)) {
			this.analyticsService.debug('transl.simple.copiedToClipboard');
		} else {
			this.analyticsService.debug('transl.error.copyToClipboardFailed');
		}
	}
}

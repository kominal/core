import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { CardListComponent } from './card-list.component';

@NgModule({
	declarations: [CardListComponent],
	imports: [CommonModule, MatCardModule, MatIconModule],
	exports: [CardListComponent],
})
export class CardListModule {}

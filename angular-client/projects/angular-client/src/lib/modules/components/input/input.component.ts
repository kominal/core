import { AfterContentInit, Component, Input, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';
@Component({
	selector: 'lib-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.scss'],
})
export class InputComponent<T> extends DefaultControlValueAccessor<T> implements AfterContentInit {
	public Object = Object;

	@Input()
	public label: string | undefined;

	@Input()
	public hint: string | undefined;

	@Input()
	public required = false;

	@Input()
	public type = 'text';

	@Input()
	public tabOrder = '';

	@Input()
	public autocorrect = 'off';

	@Input()
	public autocapitalize = 'none';

	@Input()
	public enableCopyButton = false;

	@Input()
	public appearance: MatFormFieldAppearance = 'outline';

	@Input()
	public placeholder: string | undefined;

	@Input()
	public prefixIcon: string | undefined;

	@Input()
	public min: number | undefined;

	@Input()
	public max: number | undefined;

	public hidePassword = true;

	public control!: FormControl;

	public constructor(@Self() @Optional() public ngControl: NgControl) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
	}
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { ClipboardModule } from '../../clipboard/clipboard.module';
import { InputComponent } from './input.component';

@NgModule({
	declarations: [InputComponent],
	imports: [
		CommonModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		ClipboardModule,
	],
	exports: [InputComponent],
})
export class InputModule {}

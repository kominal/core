import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { TranslateModule } from '@ngx-translate/core';
import { ToggleButtonComponent } from './toggle-button.component';

@NgModule({
	declarations: [ToggleButtonComponent],
	imports: [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule, MatButtonToggleModule],
	exports: [ToggleButtonComponent],
})
export class ToggleButtonModule {}

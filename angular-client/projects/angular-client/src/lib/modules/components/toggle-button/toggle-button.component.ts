import { AfterContentInit, Component, Input, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';

@Component({
	selector: 'lib-toggle-button',
	templateUrl: './toggle-button.component.html',
	styleUrls: ['./toggle-button.component.scss'],
})
export class ToggleButtonComponent<T> extends DefaultControlValueAccessor<T> implements AfterContentInit {
	@Input()
	public options: T[] | undefined;

	@Input()
	public selectionKey: string | undefined;

	@Input()
	public displayKey: string | undefined;

	@Input()
	public translationPrefix: string | undefined;

	@Input()
	public disableRipple = true;

	public control!: FormControl;

	public constructor(@Self() @Optional() public ngControl: NgControl, private translateService: TranslateService) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
	}

	public getDisplayValue(option: any): string {
		if (!option) {
			return '';
		}

		if (!this.displayKey) {
			if (this.translationPrefix) {
				return this.translateService.instant(this.translationPrefix + option);
			}
			return option;
		}

		const displayValue = option[this.displayKey];

		if (this.translationPrefix && displayValue) {
			return this.translateService.instant(this.translationPrefix + displayValue);
		}

		return displayValue || '';
	}

	public get(option: any, key: string): any {
		return option[key];
	}
}

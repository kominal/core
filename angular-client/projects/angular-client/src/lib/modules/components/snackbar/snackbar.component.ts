import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { Level } from '../../../core/services/analytics.service';

@Component({
	selector: 'lib-snackbar',
	templateUrl: './snackbar.component.html',
	styleUrls: ['./snackbar.component.scss'],
})
export class SnackbarComponent {
	public Level = Level;

	public constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateModule } from '@ngx-translate/core';
import { SnackbarComponent } from './snackbar.component';

@NgModule({
	declarations: [SnackbarComponent],
	imports: [CommonModule, TranslateModule, MatSnackBarModule, MatIconModule],
	exports: [SnackbarComponent],
})
export class SnackbarModule {}

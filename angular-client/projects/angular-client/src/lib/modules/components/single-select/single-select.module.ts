import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { TranslateModule } from '@ngx-translate/core';
import { SingleSelectComponent } from './single-select.component';

@NgModule({
	declarations: [SingleSelectComponent],
	imports: [CommonModule, MatInputModule, FormsModule, ReactiveFormsModule, TranslateModule, MatAutocompleteModule, MatSelectModule],
	exports: [SingleSelectComponent],
})
export class SingleSelectModule {}

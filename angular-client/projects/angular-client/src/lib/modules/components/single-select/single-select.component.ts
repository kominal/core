import { AfterContentInit, Component, Input, OnChanges, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';

interface Option {
	name: string;
	value: any;
	optionColor: string | undefined;
}

@UntilDestroy()
@Component({
	selector: 'lib-single-select',
	templateUrl: './single-select.component.html',
	styleUrls: ['./single-select.component.scss'],
})
export class SingleSelectComponent<T> extends DefaultControlValueAccessor<T> implements AfterContentInit, OnChanges {
	public Object = Object;

	@Input()
	public label: string | undefined | null;

	@Input()
	public hint: string | undefined | null;

	@Input()
	public required = false;

	@Input()
	public tabOrder = '';

	@Input()
	public options: T[] | undefined | null;

	@Input()
	public selectionKey: string | undefined | null;

	@Input()
	public displayKey: string | undefined | null;

	@Input()
	public optionColorKey: string | undefined | null;

	@Input()
	public translationPrefix: string | undefined | null;

	@Input()
	public translateLabel = true;

	@Input()
	public appearance: MatFormFieldAppearance = 'outline';

	@Input()
	public searchable = false;

	public filterControl = new FormControl();

	public internalOptions: Option[] = [];

	public filteredOptions: Option[] = [];

	public control!: FormControl;

	public constructor(@Self() @Optional() public ngControl: NgControl, private translateService: TranslateService) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngOnChanges(): void {
		this.updateOptions();
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
		this.filterControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
			this.updateOptions();
		});
		this.updateOptions();
	}

	public updateOptions(): void {
		if (!this.options || !this.control) {
			this.internalOptions = [];
			this.filteredOptions = [];
			return;
		}

		this.internalOptions = this.options.map((option) => ({
			name: this.getDisplayValue(option),
			value: this.selectionKey ? (option as any)[this.selectionKey] : option,
			optionColor: this.optionColorKey ? (option as any)[this.optionColorKey] : undefined,
		}));

		const filter = this.filterControl.value;

		if (filter && typeof filter === 'string') {
			const filterValue = filter?.toLowerCase();
			this.filteredOptions = this.internalOptions.filter(({ name }) => name.toLowerCase().includes(filterValue));
		} else {
			this.filteredOptions = this.internalOptions;
		}
	}

	public getDisplayValue(option: any): string {
		if (!option) {
			return '';
		}

		if (!this.displayKey) {
			if (this.translationPrefix) {
				return this.translateService.instant(this.translationPrefix + option);
			}
			return option;
		}

		const displayValue = option[this.displayKey];

		if (this.translationPrefix && displayValue) {
			return this.translateService.instant(this.translationPrefix + displayValue);
		}

		return displayValue || '';
	}
}

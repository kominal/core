import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AnalyticsService } from '../../../core/services/analytics.service';

export interface ConfirmDialogData {
	type?: string;
	identifier?: string;
	confirm?: string;
	confirmColor?: 'primary' | 'accent' | 'warn' | null;
	observable?: Observable<boolean>;
}

@UntilDestroy()
@Component({
	selector: 'lib-confirm-dialog',
	templateUrl: './confirm-dialog.component.html',
	styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
	public loading = false;

	public form = new FormGroup<{}>({});

	public constructor(
		public dialogRef: MatDialogRef<ConfirmDialogComponent>,
		private analyticsService: AnalyticsService,
		@Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData
	) {}

	public onSubmit(): void {
		this.loading = true;
		this.form.disable({ emitEvent: false });

		if (this.data.observable) {
			this.data.observable
				.pipe(
					untilDestroyed(this),
					catchError((e) => {
						this.analyticsService.handleError(e);
						return of(false);
					})
				)
				.subscribe((s) => {
					if (s) {
						this.dialogRef.close(true);
					} else {
						this.loading = false;
						this.form.enable({ emitEvent: false });
					}
				});
		} else {
			this.dialogRef.close(false);
		}
	}
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { TranslateModule } from '@ngx-translate/core';
import { LoadingButtonModule } from '../loading-button/loading-button.module';
import { ConfirmDialogComponent } from './confirm-dialog.component';

@NgModule({
	declarations: [ConfirmDialogComponent],
	imports: [CommonModule, MatButtonModule, TranslateModule, MatDialogModule, FormsModule, ReactiveFormsModule, LoadingButtonModule],
	exports: [ConfirmDialogComponent],
})
export class ConfirmDialogModule {}

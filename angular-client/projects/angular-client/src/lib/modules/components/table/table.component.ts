import {
	AfterContentInit,
	Component,
	ContentChildren,
	EventEmitter,
	Host,
	Input,
	Optional,
	Output,
	QueryList,
	ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatColumnDef, MatRowDef, MatTable } from '@angular/material/table';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { ColumnDefinition } from '../../../core/models/column-definition';
import { PaginationRequest } from '../../../core/models/pagination-request';
import { EMPTY_PAGINATION_RESPONSE, PaginationResponse } from '../../../core/models/pagination-response';
import { AnalyticsService } from '../../../core/services/analytics.service';

@UntilDestroy()
@Component({
	selector: 'lib-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss'],
})
export class TableComponent<T = any> implements AfterContentInit {
	@Input()
	public columnsToDisplay!: string[];

	@Input()
	public autoColumns: (string | ColumnDefinition)[] = [];

	@Input()
	public data!: Subject<PaginationResponse<T>>;

	@Input()
	public pageSizeOptions = [8, 16, 32, 64];

	@Input()
	public updateDebounce = 300;

	@Input()
	public filterLabel = 'transl.simple.filter';

	@Input()
	public filterPlaceholder: string | undefined;

	@Input()
	public dataSourceFunction?: (paginationRequest: PaginationRequest) => Promise<PaginationResponse<T>>;

	@Output()
	public update = new EventEmitter<PaginationRequest>();

	@ViewChild(MatTable, { static: true })
	public table!: MatTable<T>;

	@ViewChild(MatPaginator, { static: true })
	public paginator!: MatPaginator;

	@ContentChildren(MatColumnDef)
	private columnDefs!: QueryList<MatColumnDef>;

	@ContentChildren(MatRowDef)
	private rowDefs!: QueryList<MatRowDef<any>>;

	public dataSource = new BehaviorSubject<T[]>([]);

	public resultsLength = 0;

	public isLoadingResults = true;

	public expandedElement: any;

	public filter = new FormControl();

	public currentFilter: string = '';

	public requestedUrl: string | undefined;

	public updateSubject = new Subject<boolean>();

	public constructor(
		@Optional() @Host() public matSort: MatSort,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private analyticsService: AnalyticsService
	) {}

	public ngAfterContentInit(): void {
		this.columnDefs.forEach((columnDef) => this.table.addColumnDef(columnDef));
		this.rowDefs.forEach((rowDefs) => this.table.addRowDef(rowDefs));

		const [pageSize] = this.pageSizeOptions;

		this.paginator.pageSize = pageSize;

		if (!this.data) {
			this.data = new Subject<PaginationResponse<T>>();
		}

		this.data.pipe(untilDestroyed(this)).subscribe((data) => {
			this.dataSource.next(data.items);
			this.resultsLength = data.length;
			this.isLoadingResults = false;
		});

		this.updateSubject
			.pipe(
				untilDestroyed(this),
				filter((v) => v),
				debounceTime(this.updateDebounce)
			)
			.subscribe(() => {
				this.updateURL();
			});

		this.updateSubject
			.pipe(
				untilDestroyed(this),
				filter((v) => !v)
			)
			.subscribe(() => {
				this.updateURL();
			});

		this.paginator.page.pipe(untilDestroyed(this)).subscribe(() => {
			this.updateSubject.next(true);
		});

		this.matSort?.sortChange.pipe(untilDestroyed(this)).subscribe(() => {
			this.paginator.firstPage();
			this.updateSubject.next(true);
		});

		this.filter.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
			const filterValue = this.filter.value?.trim().toLowerCase();

			if (this.currentFilter === filterValue) {
				return;
			}

			this.currentFilter = filterValue;
			this.paginator.firstPage();
			this.updateSubject.next(true);
		});

		this.router.events
			.pipe(
				untilDestroyed(this),
				filter((event) => event instanceof NavigationEnd)
			)
			.subscribe(() => this.applyPaginationRequest());

		this.applyPaginationRequest();
	}

	public applyPaginationRequest(): void {
		const queryParams = this.activatedRoute.snapshot.queryParams as PaginationRequest;

		if (queryParams.pageIndex) {
			this.paginator.pageIndex = queryParams.pageIndex;
		}
		if (queryParams.pageSize) {
			this.paginator.pageSize = queryParams.pageSize;
		}
		if (queryParams.active && this.matSort) {
			this.matSort.active = queryParams.active;
		}
		if (queryParams.direction && this.matSort) {
			this.matSort.direction = queryParams.direction;
		}
		if (queryParams.filter) {
			this.currentFilter = queryParams.filter.trim().toLowerCase();
			this.filter.setValue(this.currentFilter);
		}

		this.triggerUpdate(true);
	}

	public updateURL(): void {
		const { pageIndex, pageSize } = this.paginator;
		const { active, direction } = this.matSort;

		this.router.navigate([], {
			relativeTo: this.activatedRoute,
			queryParams: {
				page: pageIndex !== 0 ? pageIndex : null,
				itemsPerPage: pageSize !== this.pageSizeOptions[0] ? pageSize : null,
				sort: active || null,
				order: direction || null,
				filter: this.currentFilter && this.currentFilter.length > 0 ? this.currentFilter : null,
			},
			queryParamsHandling: 'merge',
		});
	}

	public async triggerUpdate(checkCurrentUrl?: boolean): Promise<void> {
		this.isLoadingResults = true;

		if (checkCurrentUrl === true && this.requestedUrl === this.router.url) {
			return;
		}

		this.requestedUrl = this.router.url;

		const paginationRequest: PaginationRequest = {
			pageIndex: this.paginator.pageIndex,
			pageSize: this.paginator.pageSize,
			active: this.matSort?.active,
			direction: this.matSort?.direction,
			filter: this.currentFilter,
		};

		if (this.dataSourceFunction) {
			let paginationResponse = EMPTY_PAGINATION_RESPONSE;
			try {
				paginationResponse = await this.dataSourceFunction(paginationRequest);
			} catch (e) {
				this.analyticsService.handleError(e);
			}
			this.data.next(paginationResponse);
		} else {
			this.update.emit(paginationRequest);
		}
	}

	public getColumnName(column: string | ColumnDefinition): string {
		return typeof column === 'string' ? column : column.name;
	}

	public getDateFormat(column: string | ColumnDefinition): string | undefined {
		if (typeof column !== 'string') {
			return column.dateFormat;
		}
		return undefined;
	}
}

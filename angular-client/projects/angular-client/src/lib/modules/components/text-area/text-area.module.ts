import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { TextAreaComponent } from './text-area.component';

@NgModule({
	declarations: [TextAreaComponent],
	imports: [CommonModule, MatInputModule, FormsModule, ReactiveFormsModule, TranslateModule],
	exports: [TextAreaComponent],
})
export class TextAreaModule {}

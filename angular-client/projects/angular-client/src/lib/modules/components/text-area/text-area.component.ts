import { AfterContentInit, Component, Input, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';

@Component({
	selector: 'lib-text-area',
	templateUrl: './text-area.component.html',
	styleUrls: ['./text-area.component.scss'],
})
export class TextAreaComponent<T> extends DefaultControlValueAccessor<T> implements AfterContentInit {
	public Object = Object;

	@Input()
	public label: string | undefined;

	@Input()
	public hint: string | undefined;

	@Input()
	public required = false;

	@Input()
	public tabOrder = '';

	@Input()
	public appearance: MatFormFieldAppearance = 'outline';

	@Input()
	public cdkAutosizeMinRows: number | undefined;

	@Input()
	public cdkAutosizeMaxRows: number | undefined;

	public control!: FormControl;

	public constructor(@Self() @Optional() public ngControl: NgControl) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
	}
}

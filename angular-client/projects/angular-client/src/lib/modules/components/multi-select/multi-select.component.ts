import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { AfterContentInit, Component, ElementRef, Input, OnChanges, Optional, Self, ViewChild } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';

interface Option {
	name: string;
	value: any;
	chipColor: string | undefined;
	optionColor: string | undefined;
}

@UntilDestroy()
@Component({
	selector: 'lib-multi-select',
	templateUrl: './multi-select.component.html',
	styleUrls: ['./multi-select.component.scss'],
})
export class MultiSelectComponent<T> extends DefaultControlValueAccessor<T[]> implements AfterContentInit, OnChanges {
	public Object = Object;

	@Input()
	public options: T[] | undefined | null;

	@Input()
	public selectionKey: string | undefined | null;

	@Input()
	public displayKey: string | undefined | null;

	@Input()
	public chipColorKey: string | undefined | null;

	@Input()
	public optionColorKey: string | undefined | null;

	@Input()
	public label: string | undefined | null;

	@Input()
	public hint: string | undefined | null;

	@Input()
	public required = false;

	@Input()
	public tabOrder = '';

	@Input()
	public translationPrefix: string | undefined | null;

	@Input()
	public appearance: MatFormFieldAppearance = 'outline';

	@Input()
	public translateLabel = true;

	@Input()
	public allowCustom = false;

	public separatorKeysCodes = [ENTER, COMMA] as const;

	public filterControl = new FormControl();

	public internalOptions: Option[] = [];

	public filteredOptions: Option[] = [];

	@ViewChild('input')
	public input!: ElementRef<HTMLInputElement>;

	public control!: FormControl;

	public constructor(@Self() @Optional() public ngControl: NgControl, private translateService: TranslateService) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
		this.filterControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
			this.updateOptions();
		});
		this.updateOptions();
	}

	public ngAfterViewInit(): void {
		this.control.statusChanges.subscribe((status) => {
			if (status === 'DISABLED') {
				this.filterControl.disable();
			} else {
				this.filterControl.enable();
			}
		});
	}

	public ngOnChanges(): void {
		this.updateOptions();
	}

	public updateOptions(): void {
		if (!this.options || !this.control) {
			this.internalOptions = [];
			this.filteredOptions = [];
			return;
		}

		this.internalOptions = this.options.map((option) => ({
			name: this.getDisplayValue(option),
			value: this.selectionKey ? (option as any)[this.selectionKey] : option,
			chipColor: this.chipColorKey ? (option as any)[this.chipColorKey] : undefined,
			optionColor: this.optionColorKey ? (option as any)[this.optionColorKey] : undefined,
		}));

		const selectableOptions = this.internalOptions.filter((option) => !this.control.value.includes(option.value));

		const filter = this.filterControl.value;

		if (filter && typeof filter === 'string') {
			const filterValue = filter?.toLowerCase();
			this.filteredOptions = selectableOptions.filter(({ name }) => name.toLowerCase().includes(filterValue));
		} else {
			this.filteredOptions = selectableOptions;
		}
	}

	public remove(option: Option): void {
		const index = this.value?.indexOf(option.value) ?? -1;

		if (index >= 0 && this.value) {
			const newValue = [...this.value];

			newValue.splice(index, 1);
			this.value = newValue;
		}

		this.input.nativeElement.value = '';
		this.filterControl.setValue(null);
	}

	public add(event: MatChipInputEvent): void {
		if (!this.allowCustom) {
			return;
		}

		const value = (event.value || '').trim() as any;

		if (!this.value) {
			this.value = [value];
		} else if (!this.value.includes(value)) {
			this.value = [...this.value, value];
		}

		this.input.nativeElement.value = '';
		this.filterControl.setValue(null);
	}

	public selected(event: MatAutocompleteSelectedEvent): void {
		const _id = event.option.value as T;

		if (!this.value) {
			this.value = [_id];
		} else if (!this.value.includes(_id)) {
			this.value = [...this.value, _id];
		}

		this.input.nativeElement.value = '';
		this.filterControl.setValue(null);
	}

	public bySelectionKeyValue(value: any): (option: Option) => option is Option {
		return (option): option is Option => option.value === value;
	}

	private getDisplayValue(option: any): string {
		if (!option) {
			return '';
		}

		if (!this.displayKey) {
			if (this.translationPrefix) {
				return this.translateService.instant(this.translationPrefix + option);
			}
			return option;
		}

		const displayValue = option[this.displayKey];

		if (this.translationPrefix && displayValue) {
			return this.translateService.instant(this.translationPrefix + displayValue);
		}

		return displayValue || '';
	}
}

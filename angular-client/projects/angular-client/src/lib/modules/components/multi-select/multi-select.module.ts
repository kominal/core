import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { TranslateModule } from '@ngx-translate/core';
import { MultiSelectComponent } from './multi-select.component';

@NgModule({
	declarations: [MultiSelectComponent],
	imports: [
		CommonModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		MatChipsModule,
		MatIconModule,
		MatAutocompleteModule,
		MatSelectModule,
	],
	exports: [MultiSelectComponent],
})
export class MultiSelectModule {}

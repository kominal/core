import { AfterContentInit, Component, Input, Optional, Self } from '@angular/core';
import { FormControl, NgControl } from '@angular/forms';
import { DefaultControlValueAccessor } from '../../../core/classes/default-control-value-accessor';

@Component({
	selector: 'lib-color-selector',
	templateUrl: './color-selector.component.html',
	styleUrls: ['./color-selector.component.scss'],
})
export class ColorSelectorComponent<T> extends DefaultControlValueAccessor<T> implements AfterContentInit {
	public control!: FormControl;

	@Input()
	public colors: string[] = [];

	@Input()
	public defaultColor: string | undefined | null;

	public constructor(@Self() @Optional() public ngControl: NgControl) {
		super();
		this.ngControl.valueAccessor = this;
	}

	public ngAfterContentInit(): void {
		this.control = this.ngControl.control as FormControl;
	}

	public selectRandomColor(): void {
		this.control?.setValue(this.colors[Math.floor(Math.random() * this.colors.length)]);
	}

	public reset(): void {
		this.control?.setValue(this.defaultColor);
	}
}

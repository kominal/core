import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ColorSelectorComponent } from './color-selector.component';

@NgModule({
	declarations: [ColorSelectorComponent],
	imports: [CommonModule, TranslateModule, FormsModule, ReactiveFormsModule],
})
export class ColorSelectorModule {}

import { Component, Input } from '@angular/core';

@Component({
	selector: 'lib-loading-button',
	templateUrl: './loading-button.component.html',
	styleUrls: ['./loading-button.component.scss'],
})
export class LoadingButtonComponent {
	@Input()
	public disabled = false;

	@Input()
	public color: 'primary' | 'accent' | 'warn' | null | undefined;

	@Input()
	public loading = false;

	@Input()
	public tabOrder: string | null | undefined;
}

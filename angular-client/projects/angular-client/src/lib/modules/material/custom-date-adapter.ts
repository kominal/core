import { Injectable } from '@angular/core';
import { NativeDateAdapter } from '@angular/material/core';

@Injectable({ providedIn: 'root' })
export class CustomDateAdapter extends NativeDateAdapter {
	public getFirstDayOfWeek(): number {
		return 1;
	}
}

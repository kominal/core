export function generateToken(length: number): string {
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const charactersLength = characters.length;
	let result = '';
	for (let i = 0; i < length; i += 1) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

export const AUTHENTICATION_REQUIRED_HEADER = 'X-AUTHENTICATION-REQUIRED';

export const AUTHENTICATION_REQUIRED = {
	[AUTHENTICATION_REQUIRED_HEADER]: 'true',
};

export const AUTHENTICATION_REQUIRED_OPTIONS = {
	headers: AUTHENTICATION_REQUIRED,
};

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Component({ template: '' })
export abstract class DefaultControlValueAccessor<T> implements ControlValueAccessor {
	@Input()
	public set value(value: T | undefined) {
		if (JSON.stringify(value) !== JSON.stringify(this.actualValue)) {
			this.actualValue = value;
			this.onChange(value);
			this.onTouch(value);
			this.valueChange.emit(value);
		}
	}

	public get value(): T | undefined {
		return this.actualValue;
	}

	protected actualValue: T | undefined;

	@Output()
	public readonly valueChange: EventEmitter<T | undefined> = new EventEmitter<T | undefined>();

	@Input()
	public disabled = false;

	// eslint-disable-next-line @typescript-eslint/no-empty-function
	protected onChange: any = () => {};

	// eslint-disable-next-line @typescript-eslint/no-empty-function
	protected onTouch: any = () => {};

	public registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	public registerOnTouched(fn: any): void {
		this.onTouch = fn;
	}

	public setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	public writeValue(value: T | undefined): void {
		this.value = value;
	}
}

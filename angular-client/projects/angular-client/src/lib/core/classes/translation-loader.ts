import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { CORE_BASE_URL, CORE_PROJECT_ID, CORE_TENANT_ID } from './injection-tokens';

export class TranslationLoader implements TranslateLoader {
	public constructor(private httpClient: HttpClient, private injector: Injector) {}

	public getTranslation(lang: string): Observable<Object> {
		const baseUrl = this.injector.get(CORE_BASE_URL);
		const tenantId = this.injector.get(CORE_TENANT_ID);
		const projectId = this.injector.get(CORE_PROJECT_ID);
		return this.httpClient.get(`${baseUrl}/controller/tenants/${tenantId}/projects/${projectId}/translations/languages/${lang}`);
	}
}

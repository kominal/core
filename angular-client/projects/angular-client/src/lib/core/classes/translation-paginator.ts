import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateParser, TranslateService } from '@ngx-translate/core';

export class TranslationPaginatorIntl extends MatPaginatorIntl {
	private rangeLabelIntl: string = '';

	public constructor(private translateService: TranslateService, private translateParser: TranslateParser) {
		super();
		this.translateService
			.get([
				'transl.pagination.itemsPerPage',
				'transl.pagination.previousPage',
				'transl.pagination.nextPage',
				'transl.pagination.range',
			])
			.subscribe((translation) => {
				this.itemsPerPageLabel = translation['transl.pagination.itemsPerPage'];
				this.nextPageLabel = translation['transl.pagination.nextPage'];
				this.previousPageLabel = translation['transl.pagination.previousPage'];
				this.rangeLabelIntl = translation['transl.pagination.range'];
				this.changes.next();
			});
	}

	public getRangeLabel = (page: number, pageSize: number, length: number): string => {
		length = Math.max(length, 0);
		const startIndex = page * pageSize;
		const endIndex = startIndex + length;
		return this.translateParser.interpolate(this.rangeLabelIntl, { startIndex: startIndex + 1, endIndex, length });
	};
}

import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';
import { MissingTranslationHandler, MissingTranslationHandlerParams } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { MissingTranslation } from '../models/missing-translation';
import { CORE_BASE_URL, CORE_PROJECT_ID, CORE_TENANT_ID } from './injection-tokens';

export class CoreMissingTranslationHandler implements MissingTranslationHandler {
	public triggerSubject = new Subject<void>();

	public missingTranslations: MissingTranslation[] = [];

	public forwardedMissingTranslations: MissingTranslation[] = [];

	public constructor(httpClient: HttpClient, injector: Injector) {
		this.triggerSubject.pipe(debounceTime(60000)).subscribe(async () => {
			try {
				const forwarding = [...this.missingTranslations];
				this.forwardedMissingTranslations.push(...forwarding);
				this.missingTranslations = [];

				const baseUrl = injector.get(CORE_BASE_URL);
				const tenantId = injector.get(CORE_TENANT_ID);
				const projectId = injector.get(CORE_PROJECT_ID);
				await httpClient
					.post(`${baseUrl}/controller/tenants/${tenantId}/projects/${projectId}/translations/missing`, forwarding)
					.toPromise();
			} catch (e) {
				console.log(e);
			}
		});
	}

	public handle(params: MissingTranslationHandlerParams): void {
		const { key } = params;
		const language = params.translateService.currentLang || params.translateService.defaultLang || 'en';
		if (
			!this.missingTranslations.find((m) => m.language === language && m.key === key) &&
			!this.forwardedMissingTranslations.find((m) => m.language === language && m.key === key)
		) {
			this.missingTranslations.push({ language, key });
			this.triggerSubject.next();
		}
	}
}

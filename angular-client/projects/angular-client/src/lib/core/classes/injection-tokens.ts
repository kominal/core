import { ComponentType } from '@angular/cdk/portal';
import { InjectionToken } from '@angular/core';
import { Level } from '../services/analytics.service';

export const CORE_BASE_URL = new InjectionToken<string | undefined>('CORE_BASE_URL');
export const CORE_TENANT_ID = new InjectionToken<string | undefined>('CORE_TENANT_ID');
export const CORE_PROJECT_ID = new InjectionToken<string | undefined>('CORE_PROJECT_ID');
export const CORE_ENVIRONMENT_ID = new InjectionToken<string | undefined>('CORE_ENVIRONMENT_ID');
export const GUEST_HOME_URL = new InjectionToken<string | undefined>('GUEST_HOME_URL');
export const USER_HOME_URL = new InjectionToken<string | undefined>('USER_HOME_URL');
export const CHANGE_PASSWORD_REDIRECT_URL = new InjectionToken<string | undefined>('CHANGE_PASSWORD_REDIRECT_URL');
export const SINGLE_TENANT_MODE = new InjectionToken<boolean | undefined>('CORE_SINGLE_TENANT_MODE');
export const SERVICE_NAME = new InjectionToken<string | undefined>('CORE_SERVICE_NAME');
export const SNACK_BAR_COMPONENT = new InjectionToken<ComponentType<any> | boolean | undefined>('CORE_SNACK_BAR_COMPONENT');
export const DEFAULT_LANGUAGE = new InjectionToken<string[] | undefined>('PAPYRUS_DEFAULT_LANGUAGE');
export const USE_BROWSER_LANGUAGE = new InjectionToken<string[] | undefined>('PAPYRUS_USE_BROWSER_LANGUAGE');
export const LANGUAGES = new InjectionToken<string[] | undefined>('PAPYRUS_LANGUAGES');
export const SNACK_BAR_LOG_LEVEL = new InjectionToken<Level[] | undefined>('CORE_SNACK_BAR_LOG_LEVEL');

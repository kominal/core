import { TranslateService } from '@ngx-translate/core';

export function TranslationInitFactory(
	translateService: TranslateService,
	languages: string[] | undefined,
	defaultLanguage: string | undefined,
	useBrowserLanguage: boolean | undefined
): () => void {
	const defaultPapyrusLanguage = defaultLanguage || 'en';

	const selectableLanguages = languages || ['en'];
	translateService.setDefaultLang(defaultPapyrusLanguage);

	const language = localStorage.getItem('language');
	const browserLang = translateService.getBrowserLang();

	if (language && selectableLanguages.includes(language)) {
		translateService.use(language);
	} else if (useBrowserLanguage === true && browserLang && selectableLanguages.includes(browserLang)) {
		translateService.use(browserLang);
	} else {
		translateService.use(defaultPapyrusLanguage);
	}

	// eslint-disable-next-line @typescript-eslint/no-empty-function
	return (): void => {};
}

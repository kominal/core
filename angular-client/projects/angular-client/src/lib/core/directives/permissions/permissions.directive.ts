import { ChangeDetectorRef, Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Directive({
	selector: '[permissions]',
})
export class PermissionsDirective implements OnInit, OnDestroy {
	@Input()
	public permissions?: string[];

	private subscription: Subscription | undefined;

	public constructor(
		private userService: UserService,
		private viewContainer: ViewContainerRef,
		private changeDetector: ChangeDetectorRef,
		private templateRef: TemplateRef<any>
	) {}

	public ngOnInit(): void {
		this.viewContainer.clear();
		this.subscription = this.userService.currentPermissions$
			.pipe(filter((userPermissions) => !!userPermissions))
			.subscribe((userPermissions) => {
				let allowed = false;
				for (const permission of this.permissions || []) {
					if (userPermissions?.includes(permission)) {
						allowed = true;
						break;
					}
				}

				if (allowed && this.viewContainer.length === 0) {
					this.viewContainer.createEmbeddedView(this.templateRef);
					this.changeDetector.markForCheck();
				} else if (!allowed && this.viewContainer.length > 0) {
					this.viewContainer.clear();
					this.changeDetector.markForCheck();
				}
			});
	}

	public ngOnDestroy(): void {
		this.subscription?.unsubscribe();
	}
}

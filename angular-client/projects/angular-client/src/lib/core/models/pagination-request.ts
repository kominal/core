export interface PaginationRequest {
	pageIndex?: number;
	pageSize?: number;
	active?: string;
	direction?: 'asc' | 'desc' | '';
	filter?: string | any;
}

export function toPaginationParams(
	paginationRequest?: PaginationRequest
): {
	[param: string]: string | string[];
} {
	if (!paginationRequest) {
		return {};
	}

	const params: any = {};

	if (paginationRequest.pageIndex !== undefined) {
		params.pageIndex = paginationRequest.pageIndex?.toString();
	}

	if (paginationRequest.pageSize !== undefined) {
		params.pageSize = paginationRequest.pageSize?.toString();
	}

	if (paginationRequest.active) {
		params.active = paginationRequest.active?.toString();
	}

	if (paginationRequest.direction) {
		params.direction = paginationRequest.direction?.toString();
	}

	if (paginationRequest.filter) {
		if (typeof paginationRequest.filter === 'object') {
			for (const [field, filters] of Object.entries<any>(paginationRequest.filter)) {
				if (filters === undefined) {
					params[field] = 'undefined';
				} else if (filters === null) {
					params[field] = 'null';
				} else if (typeof filters === 'object') {
					for (const [operator, value] of Object.entries<any>(filters)) {
						if (value === undefined) {
							params[`${field}:${operator}`] = 'undefined';
						} else if (value === null) {
							params[`${field}:${operator}`] = 'null';
						} else {
							params[`${field}:${operator}`] = value?.toString();
						}
					}
				} else {
					params[field] = filters?.toString();
				}
			}
		} else {
			params.filter = paginationRequest.filter?.toString();
		}
	}

	return params;
}

export function toHttpClientOptions(
	paginationRequest?: PaginationRequest
): {
	params: {
		[param: string]: string | string[];
	};
} {
	return {
		params: toPaginationParams(paginationRequest),
	};
}

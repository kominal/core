export interface MissingTranslation {
	language: string;
	key: string;
}

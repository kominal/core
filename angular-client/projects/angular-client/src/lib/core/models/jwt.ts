export interface JWT {
	userId: string;
	jwt: string;
	jwtCreated: number;
	jwtExpires: number;
	payload: {
		permissions: string[];
		tenants: {
			[key: string]: {
				permissions: string[];
			};
		};
	};
}

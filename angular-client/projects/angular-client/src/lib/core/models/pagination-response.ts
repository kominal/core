export interface PaginationResponse<T> {
	items: T[];
	length: number;
}

export const EMPTY_PAGINATION_RESPONSE: PaginationResponse<any> = {
	items: [],
	length: 0,
};

export function emptyPage<T>(): PaginationResponse<T> {
	return EMPTY_PAGINATION_RESPONSE;
}

export interface ColumnDefinition {
	name: string;
	dateFormat?: string;
}

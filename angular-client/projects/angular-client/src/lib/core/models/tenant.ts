export interface BaseTenant {
	_id: string;
	name: string;
	slug: string;
	[key: string]: any;
}

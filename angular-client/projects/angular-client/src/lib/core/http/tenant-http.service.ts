import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { BaseTenant } from '../models/tenant';
import { CrudHttpService } from './crud-http.service';

@Injectable({
	providedIn: 'root',
})
export class TenantHttpService<T extends BaseTenant> extends CrudHttpService<T> {
	public constructor(httpClient: HttpClient) {
		super(httpClient, '/controller', 'tenants');
	}

	public getBySlug(slug: BaseTenant['slug']): Observable<T> {
		return this.httpClient.get<T>(`${this.baseUrl}/${this.collection}/by-slug/${slug}`, AUTHENTICATION_REQUIRED_OPTIONS);
	}
}

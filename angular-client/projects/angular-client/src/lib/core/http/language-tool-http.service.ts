import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CORE_BASE_URL } from '../classes/injection-tokens';

@Injectable({
	providedIn: 'root',
})
export class LanguageToolHttpService {
	public constructor(@Inject(CORE_BASE_URL) private coreBaseUrl: string, protected httpClient: HttpClient) {}

	public checkText(text: string, language: string): Observable<any> {
		const body = new HttpParams().set('text', text).set('language', language);
		return this.httpClient.post<any>(`${this.coreBaseUrl}/language-tool/v2/check`, body);
	}
}

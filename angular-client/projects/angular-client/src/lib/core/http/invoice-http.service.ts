import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Invoice, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { CORE_BASE_URL, CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_TENANT_ID } from '../classes/injection-tokens';
import { PaginationRequest, toPaginationParams } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';

@Injectable({
	providedIn: 'root',
})
export class InvoiceHttpService {
	// eslint-disable-next-line max-len
	private environmentBaseUrl = `${this.coreBaseUrl}/controller/tenants/${this.coreTenantId}/projects/${this.coreProjectId}/environments/${this.coreEnvironmentId}`;

	public constructor(
		@Inject(CORE_BASE_URL) private coreBaseUrl: string,
		@Inject(CORE_TENANT_ID) private coreTenantId: string,
		@Inject(CORE_PROJECT_ID) private coreProjectId: string,
		@Inject(CORE_ENVIRONMENT_ID) private coreEnvironmentId: string,
		protected httpClient: HttpClient
	) {}

	public list(tenantId: Tenant['_id'], paginationRequest?: PaginationRequest): Observable<PaginationResponse<Invoice>> {
		return this.httpClient.get<PaginationResponse<Invoice>>(`${this.environmentBaseUrl}/tenants/${tenantId}/invoices`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public read(tenantId: Tenant['_id'], documentId: Invoice['_id']): Observable<Invoice> {
		return this.httpClient.get<Invoice>(
			`${this.environmentBaseUrl}/tenants/${tenantId}/invoices/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public download(tenantId: Tenant['_id'], documentId: Invoice['_id']): Observable<Blob> {
		return this.httpClient.get(`${this.environmentBaseUrl}/tenants/${tenantId}/invoices/${documentId}/download`, {
			responseType: 'blob',
			headers: AUTHENTICATION_REQUIRED,
		});
	}
}

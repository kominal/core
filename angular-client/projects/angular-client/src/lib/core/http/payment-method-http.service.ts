import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { PaymentMethod, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { CORE_BASE_URL, CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_TENANT_ID } from '../classes/injection-tokens';
import { PaginationRequest, toPaginationParams } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';

@Injectable({
	providedIn: 'root',
})
export class PaymentMethodHttpService {
	// eslint-disable-next-line max-len
	private environmentBaseUrl = `${this.coreBaseUrl}/controller/tenants/${this.coreTenantId}/projects/${this.coreProjectId}/environments/${this.coreEnvironmentId}`;

	public constructor(
		@Inject(CORE_BASE_URL) private coreBaseUrl: string,
		@Inject(CORE_TENANT_ID) private coreTenantId: string,
		@Inject(CORE_PROJECT_ID) private coreProjectId: string,
		@Inject(CORE_ENVIRONMENT_ID) private coreEnvironmentId: string,
		protected httpClient: HttpClient
	) {}

	public initialize(tenantId: Tenant['_id'], paymentMethodType: string): Observable<any> {
		return this.httpClient.post<any>(
			`${this.environmentBaseUrl}/tenants/${tenantId}/payment-methods/initialize/${paymentMethodType}`,
			{},
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public list(tenantId: Tenant['_id'], paginationRequest?: PaginationRequest): Observable<PaginationResponse<PaymentMethod>> {
		return this.httpClient.get<PaginationResponse<PaymentMethod>>(`${this.environmentBaseUrl}/tenants/${tenantId}/payment-methods`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public create(tenantId: Tenant['_id'], document: Partial<PaymentMethod>): Observable<{ _id: PaymentMethod['_id'] }> {
		return this.httpClient.post<{ _id: PaymentMethod['_id'] }>(
			`${this.environmentBaseUrl}/tenants/${tenantId}/payment-methods`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public read(tenantId: Tenant['_id'], documentId: PaymentMethod['_id']): Observable<PaymentMethod> {
		return this.httpClient.get<PaymentMethod>(
			`${this.environmentBaseUrl}/tenants/${tenantId}/payment-methods/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], documentId: PaymentMethod['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`${this.environmentBaseUrl}/tenants/${tenantId}/payment-methods/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

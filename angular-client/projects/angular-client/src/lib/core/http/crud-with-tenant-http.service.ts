import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { PaginationRequest, toPaginationParams } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';

export class CrudWithTenantHttpService<T extends { _id: string }> {
	public constructor(protected httpClient: HttpClient, protected baseUrl: string, protected collection: string) {}

	public list(tenantId: string, paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.httpClient.get<PaginationResponse<T>>(`${this.baseUrl}/tenants/${tenantId}/${this.collection}`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: string, documentId: string): Observable<T> {
		return this.httpClient.get<T>(
			`${this.baseUrl}/tenants/${tenantId}/${this.collection}/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(tenantId: string, document: Partial<T>): Observable<{ _id: T['_id'] }> {
		return this.httpClient.post<{ _id: string }>(
			`${this.baseUrl}/tenants/${tenantId}/${this.collection}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: string, document: Partial<T>): Observable<void> {
		return this.httpClient.put<void>(
			`${this.baseUrl}/tenants/${tenantId}/${this.collection}/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: string, documentId: string): Observable<void> {
		return this.httpClient.delete<void>(
			`${this.baseUrl}/tenants/${tenantId}/${this.collection}/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { PaginationRequest, toPaginationParams } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';

export class CrudHttpService<T extends { _id: string }> {
	public constructor(protected httpClient: HttpClient, protected baseUrl: string, protected collection: string) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.httpClient.get<PaginationResponse<T>>(`${this.baseUrl}/${this.collection}`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(documentId: T['_id']): Observable<T> {
		return this.httpClient.get<T>(`${this.baseUrl}/${this.collection}/${documentId}`, AUTHENTICATION_REQUIRED_OPTIONS);
	}

	public create(document: Partial<T>): Observable<{ _id: T['_id'] }> {
		return this.httpClient.post<{ _id: T['_id'] }>(`${this.baseUrl}/${this.collection}`, document, AUTHENTICATION_REQUIRED_OPTIONS);
	}

	public update(document: Partial<T>): Observable<void> {
		return this.httpClient.put<void>(`${this.baseUrl}/${this.collection}/${document._id}`, document, AUTHENTICATION_REQUIRED_OPTIONS);
	}

	public delete(documentId: T['_id']): Observable<void> {
		return this.httpClient.delete<void>(`${this.baseUrl}/${this.collection}/${documentId}`, AUTHENTICATION_REQUIRED_OPTIONS);
	}
}

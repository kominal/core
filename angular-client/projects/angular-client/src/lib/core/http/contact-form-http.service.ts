import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ContactFormHttpService {
	public constructor(private httpClient: HttpClient) {}

	public send(source: string, payload: any): Observable<void> {
		return this.httpClient.post<void>(`https://core.kominal.app/contact-form-service/contact/${source}`, payload);
	}
}

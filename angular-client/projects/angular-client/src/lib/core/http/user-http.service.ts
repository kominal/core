import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '../classes/helper';
import { CORE_BASE_URL, CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_TENANT_ID } from '../classes/injection-tokens';
import { JWT } from '../models/jwt';
import { PaginationRequest, toPaginationParams } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';
import { User } from '../models/user';

@Injectable({
	providedIn: 'root',
})
export class UserHttpService {
	public constructor(
		@Inject(CORE_BASE_URL) private coreBaseUrl: string,
		@Inject(CORE_TENANT_ID) private coreTenantId: string,
		@Inject(CORE_PROJECT_ID) private coreProjectId: string,
		@Inject(CORE_ENVIRONMENT_ID) private coreEnvironmentId: string,
		private httpClient: HttpClient
	) {}

	private getTenantUrl(): string {
		return `${this.coreBaseUrl}/controller/tenants/${this.coreTenantId}`;
	}

	private getProjectUrl(): string {
		return `${this.getTenantUrl()}/projects/${this.coreProjectId}`;
	}

	private getEnvironmentUrl(): string {
		return `${this.getProjectUrl()}/environments/${this.coreEnvironmentId}`;
	}

	public register(baseUrl: string, email: string, password: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getTenantUrl()}/users`, { baseUrl, email, password });
	}

	public createSession(email: string, password: string, device: string, key: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getEnvironmentUrl()}/sessions`, { email, password, device, key });
	}

	public refreshSession(key: string): Observable<JWT> {
		return this.httpClient.get<any>(`${this.getEnvironmentUrl()}/sessions`, {
			headers: {
				Authorization: `Bearer ${key}`,
			},
		});
	}

	public removeSession(key: string): Observable<void> {
		return this.httpClient.delete<void>(`${this.getEnvironmentUrl()}/sessions`, {
			headers: {
				Authorization: `Bearer ${key}`,
			},
		});
	}

	public requestPasswordReset(baseUrl: string, email: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getTenantUrl()}/users/password/request-reset`, { baseUrl, email });
	}

	public resetPassword(userId: string, token: string, password: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getTenantUrl()}/users/password/reset`, { userId, token, password });
	}

	public changePassword(currentPassword: string, newPassword: string): Observable<void> {
		return this.httpClient.put<void>(
			`${this.getTenantUrl()}/users/password/change`,
			{
				currentPassword,
				newPassword,
			},
			{ headers: AUTHENTICATION_REQUIRED }
		);
	}

	public requestEmailVerify(baseUrl: string, email: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getTenantUrl()}/users/email/request-verify`, { baseUrl, email });
	}

	public verifyEmail(userId: string, token: string, device: string, key: string): Observable<void> {
		return this.httpClient.post<void>(`${this.getTenantUrl()}/users/email/verify`, { userId, token, device, key });
	}

	public getUsers(tenantId: string, paginationRequest?: PaginationRequest): Observable<PaginationResponse<User>> {
		return this.httpClient.get<PaginationResponse<User>>(`${this.coreBaseUrl}/controller/tenants/${tenantId}/users`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public listPermissions(tenantId: string, userId: string): Observable<string[]> {
		return this.httpClient.get<string[]>(
			`${this.coreBaseUrl}/controller/tenants/${tenantId}/users/${userId}/permissions`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public setPermissions(tenantId: string, userId: string, permissions: string[]): Observable<string[]> {
		return this.httpClient.put<string[]>(
			`${this.coreBaseUrl}/controller/tenants/${tenantId}/users/${userId}/permissions`,
			{ permissions },
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public getUser(userId: string): Observable<User> {
		return this.httpClient.get<User>(`${this.getTenantUrl()}/users/${userId}`, {
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public findUserByEmail(email: string): Observable<User> {
		return this.httpClient.get<User>(`${this.getTenantUrl()}/users/find-by-email/${encodeURIComponent(email)}`, {
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public deleteUser(userId: string, tenantId?: string): Observable<void> {
		return this.httpClient.delete<void>(`${this.getTenantUrl()}/users/${userId}`, {
			params: this.toParams({ tenantId }),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public toParams(params: { [param: string]: any }): { [param: string]: string } | undefined {
		const cleanedParams: { [param: string]: string } = {};
		for (const key of Object.keys(params)) {
			if (params[key]) {
				cleanedParams[key] = params[key];
			}
		}
		return Object.keys(cleanedParams).length > 0 ? cleanedParams : undefined;
	}
}

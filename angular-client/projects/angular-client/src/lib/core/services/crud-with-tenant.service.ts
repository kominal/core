import { Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { CrudWithTenantHttpService } from '../http/crud-with-tenant-http.service';
import { PaginationRequest } from '../models/pagination-request';
import { EMPTY_PAGINATION_RESPONSE, PaginationResponse } from '../models/pagination-response';
import { AnalyticsService } from './analytics.service';
import { DialogService } from './dialog.service';
import { UserService } from './user.service';

export class CrudWithTenantService<T extends { _id: string }, H extends CrudWithTenantHttpService<T> = CrudWithTenantHttpService<T>> {
	public constructor(
		protected httpService: H,
		protected userService: UserService,
		protected dialogService: DialogService,
		protected analyticsService: AnalyticsService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.list(tenant!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<T | undefined> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.get(tenant!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.create(tenant!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<T>): Observable<boolean> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.update(tenant!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(
		document: Partial<T> & { _id: T['_id'] },
		options?: { confirm?: boolean; type?: string; identifier?: string }
	): Observable<boolean> {
		const observable = this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.httpService.delete(tenant!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		if (options?.confirm) {
			return this.dialogService.openConfirmDeleteDialog(observable, options.type || 'unknown', options.identifier || 'unknown');
		}
		return observable;
	}
}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { TenantHttpService } from '../http/tenant-http.service';
import { PaginationRequest } from '../models/pagination-request';
import { EMPTY_PAGINATION_RESPONSE, PaginationResponse } from '../models/pagination-response';
import { BaseTenant } from '../models/tenant';
import { DialogService } from './dialog.service';

@Injectable({
	providedIn: 'root',
})
export class TenantService<T extends BaseTenant = BaseTenant> {
	public constructor(
		private httpService: TenantHttpService<T>,
		private dialogService: DialogService // protected analyticsService: AnalyticsService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<T>> {
		return this.httpService.list(paginationRequest).pipe(
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of(EMPTY_PAGINATION_RESPONSE)
			)
		);
	}

	public get(documentId: T['_id']): Observable<T | undefined> {
		return this.httpService.get(documentId).pipe(
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of(undefined)
			)
		);
	}

	public getBySlug(tenantIdentifier: string): Observable<T | undefined> {
		return this.httpService.getBySlug(tenantIdentifier).pipe(
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of(undefined)
			)
		);
	}

	public create(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return this.httpService.create(tenant).pipe(
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of<false>(false)
			)
		);
	}

	public update(tenant: Partial<T>): Observable<boolean> {
		return this.httpService.update(tenant).pipe(
			map(() => true),
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of(false)
			)
		);
	}

	public createOrUpdate(tenant: Partial<T>): Observable<false | { _id: T['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<T> & { _id: T['_id'] }, options?: { confirm?: boolean }): Observable<boolean> {
		const observable = this.httpService.delete(document._id).pipe(
			map(() => true),
			catchError((e) =>
				// this.analyticsService.handleError(e);
				of<false>(false)
			)
		);
		if (options?.confirm) {
			return this.dialogService.openConfirmDeleteDialog(observable, 'tenant', document.name || 'unknown').pipe(
				filter((v) => v),
				switchMap(() => observable)
			);
		}
		return observable;
	}
}

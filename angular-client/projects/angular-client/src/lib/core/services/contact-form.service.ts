import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ContactFormHttpService } from '../http/contact-form-http.service';

@Injectable({
	providedIn: 'root',
})
export class ContactFormService {
	public constructor(private contactFormHttpService: ContactFormHttpService) {}

	public send(source: string, payload: any): Observable<void> {
		return this.contactFormHttpService.send(source, payload);
	}
}

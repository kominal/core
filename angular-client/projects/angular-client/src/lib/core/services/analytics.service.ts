import { ComponentType } from '@angular/cdk/portal';
import { Inject, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Client, Message } from 'paho-mqtt';
import {
	CORE_ENVIRONMENT_ID,
	CORE_PROJECT_ID,
	CORE_TENANT_ID,
	SERVICE_NAME,
	SNACK_BAR_COMPONENT,
	SNACK_BAR_LOG_LEVEL,
} from '../classes/injection-tokens';
import { UserService } from './user.service';

// eslint-disable-next-line no-shadow
export enum Level {
	DEBUG = 'DEBUG',
	INFO = 'INFO',
	WARN = 'WARN',
	ERROR = 'ERROR',
}

@Injectable({
	providedIn: 'root',
})
export class AnalyticsService {
	public mqttConnection: Client;

	public taskId = this.generateToken(24);

	public remoteLoggingEnabled = false;

	public connected = false;

	public messageCache: Message[] = [];

	public constructor(
		@Inject(CORE_TENANT_ID) private tenantId: string,
		@Inject(CORE_PROJECT_ID) private projectId: string,
		@Inject(CORE_ENVIRONMENT_ID) private environmentId: string,
		@Inject(SERVICE_NAME) private serviceName: string,
		@Inject(SNACK_BAR_COMPONENT) private snackBarComponent: ComponentType<any> | boolean,
		@Inject(SNACK_BAR_LOG_LEVEL) private snackBarLogLevel: Level[] | undefined,
		private translateService: TranslateService,
		private snackBar: MatSnackBar,
		userService: UserService
	) {
		this.remoteLoggingEnabled = !!tenantId && !!projectId && !!environmentId && !!serviceName;
		this.mqttConnection = new Client('mqtt.core.kominal.app', 8084, '/mqtt');

		userService.tokenSubject.subscribe((jwt) => {
			if (jwt) {
				this.mqttConnection.connect({
					useSSL: true,
					userName: jwt.userId,
					password: jwt.jwt,
					onSuccess: () => {
						this.connected = true;
						for (const message of this.messageCache) {
							this.mqttConnection.send(message);
						}
						this.messageCache = [];
					},
				});
			}
		});

		this.mqttConnection.onConnectionLost = (): void => {
			this.connected = false;
		};
	}

	private generateToken(length: number): string {
		const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		const charactersLength = characters.length;
		let result = '';
		for (let i = 0; i < length; i += 1) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	private print(level: Level, message: any): void {
		try {
			let color = '#ffff00';
			if (level === Level.DEBUG) {
				color = '#00b0ff';
			} else if (level === Level.INFO) {
				color = '#bada55';
			} else if (level === Level.WARN) {
				color = '#ffd700';
			} else if (level === Level.ERROR) {
				color = '#f44336';
			}

			if (typeof message === 'string') {
				if (message.startsWith('transl.')) {
					message = this.translateService.instant(message);
				}
			} else {
				message = JSON.stringify(message);
			}

			const consoleMessage = `${new Date().toLocaleTimeString(undefined, { hour12: false })} | [%c${level}%c] ${
				typeof message === 'string' ? message : JSON.stringify(message)
			}`;

			if (level === Level.DEBUG) {
				console.debug(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.INFO) {
				console.info(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.WARN) {
				console.warn(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.ERROR) {
				console.error(consoleMessage, `color: ${color}`, '');
			} else {
				console.log(consoleMessage, `color: ${color}`, '');
			}

			if ((this.snackBarLogLevel || [Level.WARN, Level.ERROR])?.includes(level)) {
				if (this.snackBarComponent && this.snackBarComponent !== true) {
					this.snackBar.openFromComponent(this.snackBarComponent, { data: { level, message }, duration: 3000 });
				} else {
					this.snackBar.open(message, '', { duration: 3000 });
				}
			}
		} catch (e) {
			console.log(e);
		}
	}

	public sendAnalytics(type: string, level: Level, content: any): void {
		if (!this.remoteLoggingEnabled) {
			return;
		}
		const message = new Message(
			JSON.stringify({
				time: new Date(),
				type,
				level,
				content,
			})
		);
		message.destinationName = `${this.tenantId}/${this.projectId}/${this.environmentId}/${this.serviceName}/${this.taskId}/analytics`;

		if (this.connected) {
			this.mqttConnection.send(message);
		} else {
			this.messageCache.push(message);
		}
	}

	public async log(level: Level, message: any): Promise<void> {
		this.print(level, message);
		if (!this.remoteLoggingEnabled) {
			return;
		}
		this.sendAnalytics('LOG', level, message);
	}

	public async debug(message: any): Promise<void> {
		return this.log(Level.DEBUG, message);
	}

	public async info(message: any): Promise<void> {
		return this.log(Level.INFO, message);
	}

	public async warn(message: any): Promise<void> {
		return this.log(Level.WARN, message);
	}

	public async error(message: any): Promise<void> {
		return this.log(Level.ERROR, message);
	}

	public async handleError(e: any): Promise<void> {
		if (typeof e === 'string') {
			return this.error(e);
		}
		if (e.error?.error) {
			return this.error(e.error.error);
		}
		if (e.message) {
			console.trace(e);
			return this.error(e.message);
		}
		console.trace(e);
		return this.error('transl.error.unknown');
	}
}

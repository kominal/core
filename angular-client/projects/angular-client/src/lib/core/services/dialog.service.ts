import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent, ConfirmDialogData } from '../../modules/components/confirm-dialog/confirm-dialog.component';

@Injectable({
	providedIn: 'root',
})
export class DialogService {
	public constructor(private matDialog: MatDialog) {}

	public openConfirmDialog(data: ConfirmDialogData): Observable<boolean> {
		return this.matDialog
			.open<ConfirmDialogComponent, ConfirmDialogData>(ConfirmDialogComponent, {
				data,
				width: '500px',
			})
			.afterClosed();
	}

	public openConfirmDeleteDialog(observable: Observable<any>, type: string, identifier: string): Observable<boolean> {
		return this.openConfirmDialog({
			type: `delete.${type}`,
			identifier,
			observable,
			confirm: 'transl.simple.delete',
			confirmColor: 'warn',
		});
	}
}

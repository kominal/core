import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LanguageToolHttpService } from '../http/language-tool-http.service';

@Injectable({
	providedIn: 'root',
})
export class LanguageToolService {
	public constructor(private languageToolHttpService: LanguageToolHttpService) {}

	public checkText(text: string, language: string): Observable<any> {
		return this.languageToolHttpService.checkText(text, language);
	}
}

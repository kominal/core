import { Injectable } from '@angular/core';
import { Invoice } from '@kominal/core-common';
import { Observable, of } from 'rxjs';
import { catchError, filter, first, switchMap } from 'rxjs/operators';
import { InvoiceHttpService } from '../http/invoice-http.service';
import { PaginationRequest } from '../models/pagination-request';
import { EMPTY_PAGINATION_RESPONSE, PaginationResponse } from '../models/pagination-response';
import { AnalyticsService } from './analytics.service';
import { UserService } from './user.service';

@Injectable({
	providedIn: 'root',
})
export class InvoiceService {
	public constructor(
		private userService: UserService,
		private analyticsService: AnalyticsService,
		private invoiceHttpService: InvoiceHttpService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Invoice>> {
		return this.userService.currentTenant$.pipe(
			filter((tenant) => !!tenant),
			switchMap((tenant) => this.invoiceHttpService.list(tenant!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public read(documentId: string): Observable<Invoice | undefined> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.invoiceHttpService.read(tenant!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public download(documentId: string): Observable<Blob | undefined> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.invoiceHttpService.download(tenant!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}
}

import { Injectable } from '@angular/core';
import { PaymentMethod } from '@kominal/core-common';
import { Observable, of } from 'rxjs';
import { catchError, filter, first, map, switchMap } from 'rxjs/operators';
import { PaymentMethodHttpService } from '../http/payment-method-http.service';
import { PaginationRequest } from '../models/pagination-request';
import { EMPTY_PAGINATION_RESPONSE, PaginationResponse } from '../models/pagination-response';
import { AnalyticsService } from './analytics.service';
import { DialogService } from './dialog.service';
import { UserService } from './user.service';

@Injectable({
	providedIn: 'root',
})
export class PaymentMethodService {
	public constructor(
		private userService: UserService,
		private analyticsService: AnalyticsService,
		private dialogService: DialogService,
		private paymentMethodHttpService: PaymentMethodHttpService
	) {}

	public initialize(paymentMethodType: string): Observable<any> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.paymentMethodHttpService.initialize(tenant!._id, paymentMethodType)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of();
			})
		);
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<PaymentMethod>> {
		return this.userService.currentTenant$.pipe(
			filter((tenant) => !!tenant),
			switchMap((tenant) => this.paymentMethodHttpService.list(tenant!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public create(document: Partial<PaymentMethod>): Observable<false | { _id: PaymentMethod['_id'] }> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.paymentMethodHttpService.create(tenant!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public read(documentId: string): Observable<PaymentMethod | undefined> {
		return this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.paymentMethodHttpService.read(tenant!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public delete(document: PaymentMethod & { _id: PaymentMethod['_id'] }): Observable<boolean> {
		const observable = this.userService.currentTenant$.pipe(
			first((tenant) => !!tenant),
			switchMap((tenant) => this.paymentMethodHttpService.delete(tenant!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'service', document._id);
	}
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, ChildActivationStart, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, shareReplay } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class RouterService {
	public pathParameter$: Observable<{ [key: string]: string }>;

	public constructor(private router: Router) {
		this.pathParameter$ = this.router.events.pipe(
			filter((e) => e instanceof ChildActivationStart),
			map((e) => e as ChildActivationStart),
			map((e: ChildActivationStart) => {
				const params: Params = {};
				const collectParams = (snapshot: ActivatedRouteSnapshot): void => {
					Object.assign(params, snapshot.params);
					snapshot.children.forEach(collectParams);
				};
				collectParams(e.snapshot.root);
				return params;
			}),
			shareReplay(1)
		);
		this.pathParameter$.subscribe();
	}

	public observePathParameter(name: string): Observable<string | undefined> {
		return this.pathParameter$.pipe(
			map((params) => params[name]),
			distinctUntilChanged(),
			shareReplay(1)
		);
	}
}

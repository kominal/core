import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { hash } from 'bcryptjs';
import { BehaviorSubject, combineLatest, from, Observable, of, Subject } from 'rxjs';
import { catchError, concatMap, distinctUntilChanged, filter, map, shareReplay, switchAll, switchMap, tap } from 'rxjs/operators';
import { generateToken } from '../classes/helper';
import { CHANGE_PASSWORD_REDIRECT_URL, GUEST_HOME_URL, SINGLE_TENANT_MODE, USER_HOME_URL } from '../classes/injection-tokens';
import { UserHttpService } from '../http/user-http.service';
import { JWT } from '../models/jwt';
import { PaginationRequest } from '../models/pagination-request';
import { PaginationResponse } from '../models/pagination-response';
import { BaseTenant } from '../models/tenant';
import { User } from '../models/user';
import { RouterService } from './router.service';
import { TenantService } from './tenant.service';

@Injectable({
	providedIn: 'root',
})
export class UserService<T extends BaseTenant = BaseTenant> {
	private SALT = '$2a$10$BGS4LQdHK7nFhRvW5YM7b.';

	public loginSubject = new Subject();

	public tokenSubject = new BehaviorSubject<(JWT & { jwtReceived: number }) | undefined>(undefined);

	public logoutSubject = new Subject<void>();

	public tenants$ = new Observable<T[] | undefined>();

	public reloadTenants$ = new BehaviorSubject<void>(undefined);

	public currentTenantSlug$ = new Observable<string | undefined>();

	public currentTenant$ = new Observable<T | undefined>();

	public reloadCurrentTenant$ = new BehaviorSubject<void>(undefined);

	public currentPermissions$ = new Observable<string[] | undefined>();

	private refreshing = new BehaviorSubject<boolean>(false);

	public constructor(
		private userHttpService: UserHttpService,
		@Inject(GUEST_HOME_URL) private guestHomeUrl: string | undefined,
		@Inject(USER_HOME_URL) private userHomeUrl: string | undefined,
		@Inject(CHANGE_PASSWORD_REDIRECT_URL) private changePasswordRedirectUrl: string | undefined,
		@Inject(SINGLE_TENANT_MODE) singleTenantMode: boolean | undefined,
		private tenantService: TenantService<T>,
		// private analyticsService: AnalyticsService,
		private router: Router,
		routerService: RouterService
	) {
		this.tenants$ = combineLatest([this.tokenSubject, this.reloadTenants$]).pipe(
			distinctUntilChanged(([p], [n]) => {
				if ((!!p && !n) || (!p && !!n)) {
					return false;
				}
				return Object.keys(p?.payload.tenants || {}).join(',') === Object.keys(n?.payload.tenants || {}).join(',');
			}),
			switchMap(([token]) => (token ? this.tenantService.list().pipe(map((r) => r.items)) : of(undefined))),
			shareReplay(1)
		);

		this.currentTenantSlug$ = routerService.observePathParameter('tenantSlug');

		this.currentTenant$ = combineLatest([this.currentTenantSlug$, this.reloadCurrentTenant$]).pipe(
			concatMap(([tenantSlug]) => (tenantSlug ? [of(undefined), this.tenantService.getBySlug(tenantSlug)] : [of(undefined)])),
			switchAll(),
			shareReplay(1)
		);

		this.currentPermissions$ = combineLatest([this.currentTenant$, this.tokenSubject]).pipe(
			map(([tenant, token]) => {
				if (token) {
					if (singleTenantMode) {
						return token.payload.permissions;
					}
					if (tenant && token.payload.tenants[tenant._id]) {
						return token.payload.tenants[tenant._id].permissions;
					}
				}
				return undefined;
			})
		);

		setTimeout(() => {
			this.currentTenant$.subscribe();
			if (this.isLoggedIn()) {
				this.refresh();
			}
		}, 10);
	}

	public register(email: string, password: string): Observable<void> {
		return from(hash(password, this.SALT)).pipe(
			// eslint-disable-next-line no-restricted-globals
			switchMap((passwordHash) => this.userHttpService.register(location.origin, email, passwordHash)),
			tap(() => {
				// this.analyticsService.info('transl.simple.verifyEmail');
				this.router.navigate(['/authentication/login']);
			})
		);
	}

	public verifyEmail(userId: string, token: string, options?: { redirect?: boolean }): Observable<void> {
		const key = generateToken(32);
		return from(hash(key, this.SALT)).pipe(
			switchMap((keyHash) =>
				this.userHttpService.verifyEmail(userId, token, `${navigator.platform}, ${navigator.appCodeName}`, keyHash)
			),
			tap(() => localStorage.setItem('sessionKey', key)),
			switchMap(() => from(this.refresh())),
			tap(() => {
				if (options?.redirect === true) {
					this.router.navigate([this.userHomeUrl || '/']);
				}
			})
		);
	}

	public login(email: string, password: string, options?: { redirect?: boolean }): Observable<void> {
		const key = generateToken(32);
		return combineLatest([from(hash(password, this.SALT)), from(hash(key, this.SALT))]).pipe(
			switchMap(([passwordHash, keyHash]) =>
				this.userHttpService.createSession(email, passwordHash, `${navigator.platform}, ${navigator.appCodeName}`, keyHash)
			),
			tap(() => localStorage.setItem('sessionKey', key)),
			switchMap(() => from(this.refresh())),
			tap(() => {
				if (options?.redirect === true) {
					this.router.navigate([this.userHomeUrl || '/']);
				}
			})
		);
	}

	public async refresh(): Promise<void> {
		const sessionKey = localStorage.getItem('sessionKey');
		if (!sessionKey) {
			throw new Error("Can't refresh when not logged in.");
		}

		if (this.refreshing.value) {
			await new Promise<void>((resolve) => this.refreshing.pipe(filter((v) => !v)).subscribe((v) => resolve()));
			return;
		}

		this.refreshing.next(true);
		try {
			// this.analyticsService.info('Refreshing session...');

			const jwt = await this.userHttpService.refreshSession(await hash(sessionKey, this.SALT)).toPromise();
			if (jwt) {
				this.tokenSubject.next({ ...jwt, jwtReceived: Date.now() });
			}
		} catch (error) {
			// this.analyticsService.handleError(error);
			this.logout();
		}

		this.refreshing.next(false);
	}

	public async getJWT(): Promise<string> {
		let token = this.tokenSubject.value;
		if (!token || Date.now() > token.jwtReceived + (token.jwtExpires - token.jwtCreated) - 15000) {
			await this.refresh();

			token = this.tokenSubject.value;
			if (!token || Date.now() > token.jwtReceived + (token.jwtExpires - token.jwtCreated) - 15000) {
				throw new Error('JWT is undefined');
			}
		}

		return token.jwt;
	}

	public isLoggedIn(): boolean {
		return !!localStorage.getItem('sessionKey');
	}

	public changePassword(currentPassword: string, newPassword: string): Observable<void> {
		return combineLatest([from(hash(currentPassword, this.SALT)), from(hash(newPassword, this.SALT))]).pipe(
			switchMap(([currentPasswordHash, newPasswordHash]) =>
				this.userHttpService.changePassword(currentPasswordHash, newPasswordHash)
			),
			tap(() => {
				// this.analyticsService.info('transl.simple.passwordChanged');
				this.router.navigate([this.changePasswordRedirectUrl || '/']);
			})
		);
	}

	public requestPasswordReset(email: string): Observable<void> {
		// eslint-disable-next-line no-restricted-globals
		return this.userHttpService.requestPasswordReset(location.origin, email).pipe(
			tap(() => {
				// this.analyticsService.info('transl.simple.passwordResetRequested');
				this.router.navigate(['/authentication/login']);
			})
		);
	}

	public requestEmailVerify(email: string): Observable<void> {
		// eslint-disable-next-line no-restricted-globals
		return this.userHttpService.requestEmailVerify(location.origin, email).pipe(
			tap(() => {
				// this.analyticsService.info('transl.simple.verificationEmailRequested');
				this.router.navigate(['/authentication/login']);
			})
		);
	}

	public resetPassword(userId: string, token: string, password: string): Observable<void> {
		return from(hash(password, this.SALT)).pipe(
			switchMap((passwordHash) => this.userHttpService.resetPassword(userId, token, passwordHash))
		);
	}

	public logout(): Observable<void> {
		// this.analyticsService.info('transl.simple.logout');
		const sessionKey = localStorage.getItem('sessionKey');
		localStorage.removeItem('sessionKey');

		this.router.navigate([this.guestHomeUrl || '/']);
		this.tokenSubject.next(undefined);
		this.logoutSubject.next();

		if (sessionKey) {
			return from(hash(sessionKey, this.SALT)).pipe(
				switchMap((sessionKeyHash) => this.userHttpService.removeSession(sessionKeyHash)),
				catchError((e) =>
					// this.analyticsService.handleError(e)
					of(undefined)
				)
			);
		}
		return of();
	}

	public getUsers(tenantId: string, pagination?: PaginationRequest): Observable<PaginationResponse<User>> {
		return this.userHttpService.getUsers(tenantId, pagination);
	}

	public listPermissions(tenantId: string, userId: string): Observable<string[]> {
		return this.userHttpService.listPermissions(tenantId, userId);
	}

	public setPermissions(tenantId: string, userId: string, permissions: string[]): Observable<string[]> {
		return this.userHttpService.setPermissions(tenantId, userId, permissions);
	}

	public getUser(userId: string): Observable<User> {
		return this.userHttpService.getUser(userId);
	}

	public findUserByEmail(email: string): Observable<User> {
		return this.userHttpService.findUserByEmail(email);
	}

	public deleteUser(userId: string, tenantId?: string): Observable<void> {
		return this.userHttpService.deleteUser(userId, tenantId);
	}
}

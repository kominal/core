import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilesPipe } from './files.pipe';

@NgModule({
	declarations: [FilesPipe],
	imports: [CommonModule],
	exports: [FilesPipe],
})
export class FilesModule {}

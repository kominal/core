import { Injector, Pipe, PipeTransform } from '@angular/core';
import { CORE_BASE_URL, CORE_PROJECT_ID, CORE_TENANT_ID } from '../../classes/injection-tokens';

@Pipe({
	name: 'files',
})
export class FilesPipe implements PipeTransform {
	public constructor(private injector: Injector) {}

	public transform(key: string): string {
		return `${this.injector.get(CORE_BASE_URL)}/controller/tenants/${this.injector.get(CORE_TENANT_ID)}/projects/${this.injector.get(
			CORE_PROJECT_ID
		)}/files/byKey/${key}`;
	}
}

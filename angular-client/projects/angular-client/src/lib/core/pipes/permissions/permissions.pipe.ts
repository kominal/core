import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

@Pipe({
	name: 'permissions',
})
export class PermissionsPipe implements PipeTransform {
	public constructor(private userService: UserService) {}

	public transform(permissions: string[]): Observable<boolean> {
		return this.userService.currentPermissions$.pipe(
			filter((userPermissions) => !!userPermissions),
			map((userPermissions) => {
				for (const permission of permissions) {
					if (userPermissions?.includes(permission)) {
						return true;
					}
				}
				return false;
			})
		);
	}
}

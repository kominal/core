import { Inject, Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { USER_HOME_URL } from '../../classes/injection-tokens';
import { UserService } from '../../services/user.service';

@Injectable({
	providedIn: 'root',
})
export class GuestGuard implements CanActivate {
	public constructor(private injector: Injector, @Inject(USER_HOME_URL) private userHomeUrl: string | undefined) {}

	public async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
		if (this.injector.get(UserService).isLoggedIn()) {
			this.injector.get(Router).navigate([this.userHomeUrl || '/']);
			return false;
		}
		return true;
	}
}

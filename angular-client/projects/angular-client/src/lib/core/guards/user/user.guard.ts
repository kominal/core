import { Inject, Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { GUEST_HOME_URL } from '../../classes/injection-tokens';
import { UserService } from '../../services/user.service';

@Injectable({
	providedIn: 'root',
})
export class UserGuard implements CanActivate {
	public constructor(private injector: Injector, @Inject(GUEST_HOME_URL) private guestHomeUrl: string | undefined) {}

	public canActivate(route: ActivatedRouteSnapshot): boolean | Observable<boolean> {
		const userService = this.injector.get(UserService);
		if (!userService.isLoggedIn()) {
			this.injector.get(Router).navigate([this.guestHomeUrl || '/']);
			return false;
		}

		if (!route.data?.permissions) {
			return true;
		}

		const permissions = route.data?.permissions as string[];

		return userService.currentPermissions$.pipe(
			filter((userPermissions) => !!userPermissions),
			map((currentPermissions) => {
				for (const permission of permissions) {
					if (currentPermissions?.includes(permission)) {
						return true;
					}
				}
				return false;
			})
		);
	}
}

import { Router } from '@kominal/lib-node-express-router';
import { sendMail } from '@kominal/lib-node-mailer';
import { RECEIVER_ADDRESS } from '../helper/environment';

export const contactRouter = new Router();

contactRouter.postAsGuest<{ source: string }, any, never>('/contact/:source', async (req) => {
	try {
		await sendMail({
			subject: `Received message in contact form ${req.params.source}`,
			to: RECEIVER_ADDRESS,
			text: JSON.stringify(req.body),
		});
	} catch (e) {
		console.log(`sendMailFailed cause Error(${e})`);
		throw new Error('transl.error.mailSentFailed');
	}
	return { statusCode: 200 };
});

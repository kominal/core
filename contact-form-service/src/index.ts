import { ExpressRouter } from '@kominal/lib-node-express-router';
import { info, startObserver } from '@kominal/observer-node-client';
import { exit } from 'process';
import { contactRouter } from './routes/contact';

let expressRouter: ExpressRouter;

async function start(): Promise<void> {
	startObserver();
	expressRouter = new ExpressRouter({
		healthCheck: async (): Promise<boolean> => true,
		routes: [contactRouter],
	});
	await expressRouter.start();
}
start();

process.on('SIGTERM', async () => {
	info("Received system signal 'SIGTERM'. Shutting down service...");
	expressRouter?.getServer()?.close();
	exit(0);
});

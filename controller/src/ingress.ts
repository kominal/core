import { Analytics } from '@kominal/core-common';
import { handleError, warn } from '@kominal/core-node-client';
import { AsyncClient, connect } from 'async-mqtt';
import { CORE_MQTT_BROKER, CORE_SERVICE_TOKEN } from './helper/environment';
import { AnalyticsDatabase } from './models/analytics';
import { EnvironmentDatabase } from './models/environment';
import { ServiceDatabase } from './models/service';

export async function startMQTTIngress(): Promise<AsyncClient> {
	await new Promise((resolve) => setTimeout(resolve, 20));
	const mqttClient = connect(CORE_MQTT_BROKER, {
		username: 'SERVICE',
		password: CORE_SERVICE_TOKEN,
	});
	mqttClient.on('message', async (topic, buffer) => {
		try {
			const parts = topic.split('/');
			if (parts.length !== 6) {
				return;
			}
			const [tenantId, projectId, environmentId, serviceSlug, taskId, messageType] = parts;

			let service = await ServiceDatabase.findOne({
				tenantId,
				projectId,
				environmentId,
				slug: serviceSlug,
			});

			if (!service) {
				const environment = await EnvironmentDatabase.findOne({
					_id: environmentId,
					tenantId,
					projectId,
				});

				if (!environment) {
					warn('Could not find environment to create service');
					return;
				}

				service = await ServiceDatabase.create({
					tenantId: environment.tenantId,
					projectId: environment.projectId,
					environmentId: environment._id,
					name: serviceSlug
						.split(/_| |-/)
						.map((w) => w.charAt(0).toUpperCase() + w.slice(1))
						.join(' '),
					slug: serviceSlug,
				});
			}

			const body = JSON.parse(buffer.toString('utf-8'));

			if (messageType === 'analytics') {
				const { time, type, content }: Pick<Analytics, 'time' | 'type' | 'level' | 'content'> = body;
				await AnalyticsDatabase.create({
					tenantId: service.tenantId,
					projectId: service.projectId,
					environmentId: service.environmentId,
					serviceId: service._id,
					taskId,
					time,
					type,
					content,
				});
			} else {
				warn(`Received invalid message: ${JSON.stringify(body)}`);
			}
		} catch (e) {
			warn(`Received invalid topic: ${topic}`);
			handleError(e);
		}
	});
	mqttClient.on('error', (e) => handleError(e));
	await mqttClient.subscribe('#');
	return mqttClient;
}

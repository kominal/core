import { Service } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { ServiceDatabase } from '../models/service';

export const servicesRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/services');

servicesRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string }, PaginationResponse<Service>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Service>(ServiceDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

servicesRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, Service, never>(
	':documentId',
	async (req) => {
		const service = await ServiceDatabase.findOne({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		})
			.lean<Service>()
			.orFail(new Error('transl.error.notFound.service'));

		return {
			statusCode: 200,
			responseBody: service,
		};
	}
);

servicesRouter.postAsUser<{ tenantId: string; projectId: string; environmentId: string }, Partial<Service>, { _id: string }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const service = await ServiceDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			created: new Date(),
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: service._id,
			},
		};
	}
);

servicesRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; documentId: string },
	Partial<Service>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await ServiceDatabase.updateMany(
		{
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		},
		update
	);
	return {
		statusCode: 200,
	};
});

servicesRouter.deleteAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, never, never>(
	':documentId',
	async (req) => {
		await ServiceDatabase.deleteMany({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

servicesRouter.getAsGuest<{ tenantId: string; projectId: string; environmentId: string; slug: string }, Service, never>(
	'by-slug/:slug',
	async (req) => ({
		statusCode: 200,
		responseBody: await ServiceDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			slug: req.params.slug,
		})
			.lean()
			.orFail(new Error('transl.error.notFound.service')),
	})
);

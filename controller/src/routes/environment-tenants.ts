import { Environment, Tenant } from '@kominal/core-common';
import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { getTenant } from '../helper/helper';
import { EnvironmentDatabase } from '../models/environment';
import { RoleDatabase } from '../models/role';
import { TenantDatabase } from '../models/tenant';
import { UserDatabase } from '../models/user';

export const environmentTenantsRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/tenants');

environmentTenantsRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string },
	PaginationResponse<Tenant>,
	PaginationRequest
>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Tenant>(TenantDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
		});

		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

environmentTenantsRouter.getAsGuest<{ tenantId: string; projectId: string; environmentId: string; slug: string }, Tenant, never>(
	'by-slug/:slug',
	async (req) => ({
		statusCode: 200,
		responseBody: await TenantDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			slug: req.params.slug,
		})
			.lean()
			.orFail(new Error('transl.error.notFound.tenant')),
	})
);

environmentTenantsRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, Tenant, never>(
	':documentId',
	async (req) => ({
		statusCode: 200,
		responseBody: await TenantDatabase.findOne({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		})
			.lean()
			.orFail(new Error('transl.error.notFound.tenant')),
	})
);

environmentTenantsRouter.postAsGuest<
	{ tenantId: string; projectId: string; environmentId: string },
	{ name: Tenant['name']; slug: Tenant['slug']; permissions: string[] },
	{ _id: string },
	never
>(undefined, async (req, _res) => {
	verifyParameter(['name', 'slug'], req.body);

	const environment = await EnvironmentDatabase.findOne({
		_id: req.params.environmentId,
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
	})
		.lean()
		.orFail(new Error('transl.error.notFound.environment'));

	const environmentToken = req.headers.authorization;
	const userId = req.headers.userid;

	if (!environmentToken || !userId || !environment.tokens.includes(environmentToken)) {
		throw new Error('transl.error.invalidEnvironmentToken');
	}

	const slug = req.body.slug.toLowerCase();

	if (!slug.match(new RegExp(/^[a-zA-Z0-9_-]*$/))) {
		throw new Error('transl.error.tenantSlugInvalid');
	}

	if (await TenantDatabase.exists({ slug })) {
		throw new Error('transl.error.tenantSlugAlreadyInUse');
	}

	const tenant = await TenantDatabase.create({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		environmentId: req.params.environmentId,
		name: req.body.name,
		slug,
	});

	const role = await RoleDatabase.create({
		tenantId: tenant._id,
		name: `Role for ${userId}`,
		permissions: req.body.permissions,
		default: false,
	});

	await UserDatabase.updateOne({ _id: userId }, { $push: { roles: role._id } });

	return {
		statusCode: 200,
		responseBody: {
			_id: tenant._id,
		},
	};
});

environmentTenantsRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; documentId: string },
	Tenant & { tenantId: never },
	never,
	never
>(
	':documentId',
	async (req) => {
		delete req.body.tenantId;

		if (req.body.slug) {
			const slug = req.body.slug.toLowerCase();

			if (!slug.match(new RegExp(/^[a-zA-Z0-9_-]*$/))) {
				throw new Error('transl.error.tenantSlugInvalid');
			}
			const tenant = await TenantDatabase.findById(req.params.documentId);
			if (tenant && tenant.slug !== slug) {
				if (await TenantDatabase.exists({ slug })) {
					throw new Error('transl.error.tenantSlugAlreadyInUse');
				}
			}
		}

		await TenantDatabase.updateMany(
			{
				_id: req.params.documentId,
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
			{
				...req.body,
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			}
		);
		return {
			statusCode: 200,
		};
	},
	{ permissions: ['core.settings.write'] }
);

environmentTenantsRouter.deleteAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, never, never>(
	':documentId',
	async (req, _res, _userId) => {
		const tenant = await getTenant(req.params.tenantId, req.params.projectId, req.params.environmentId, req.params.documentId);

		await TenantDatabase.deleteMany({ _id: tenant._id });

		return {
			statusCode: 200,
		};
	},
	{ permissions: ['core.tenant.delete'] }
);

import { Report } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { ReportDatabase } from '../models/report';

export const reportsRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/reports');

reportsRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string }, PaginationResponse<Report>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Report>(ReportDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

reportsRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, Report, never>(
	':documentId',
	async (req) => {
		const report = await ReportDatabase.findOne({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		})
			.lean<Report>()
			.orFail(new Error('transl.error.notFound.report'));

		return {
			statusCode: 200,
			responseBody: report,
		};
	}
);

reportsRouter.postAsUser<{ tenantId: string; projectId: string; environmentId: string }, Partial<Report>, { _id: string }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const report = await ReportDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			created: new Date(),
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: report._id,
			},
		};
	}
);

reportsRouter.putAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, Partial<Report>, never, never>(
	':documentId',
	async (req) => {
		const update: any = { ...req.body };
		update.tenantId = req.params.tenantId;
		update.projectId = req.params.projectId;

		await ReportDatabase.updateMany(
			{
				_id: req.params.documentId,
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
			update
		);
		return {
			statusCode: 200,
		};
	}
);

reportsRouter.deleteAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, never, never>(
	':documentId',
	async (req) => {
		await ReportDatabase.deleteMany({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

import { Project } from '@kominal/core-common';
import { CrudWithTenantRouter } from '@kominal/lib-node-express-router';
import { ProjectDatabase } from '../models/project';

export const projectsRouter = new CrudWithTenantRouter<Project>(ProjectDatabase, 'projects');

projectsRouter.getAsGuest<{ tenantId: string; slug: string }, Project, never>('by-slug/:slug', async (req) => ({
	statusCode: 200,
	responseBody: await ProjectDatabase.findOne({ tenantId: req.params.tenantId, slug: req.params.slug })
		.lean()
		.orFail(new Error('transl.error.notFound.project')),
}));

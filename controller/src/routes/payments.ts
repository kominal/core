import { Payment } from '@kominal/core-common';
import { CrudWithTenantRouter } from '@kominal/lib-node-express-router';
import { PaymentDatabase } from '../models/payment';

export const paymentsRouter = new CrudWithTenantRouter<Payment>(PaymentDatabase, 'payments');

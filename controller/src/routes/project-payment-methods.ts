import { PaymentMethod } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import Stripe from 'stripe';
import { getTenant } from '../helper/helper';
import { PaymentMethodDatabase } from '../models/payment-method';
import { TenantDatabase } from '../models/tenant';

export const projectPaymentMethodsRouter = new Router(
	'/tenants/:tenantId/projects/:projectId/environments/:environmentId/tenants/:projectTenantId/payment-methods'
);

projectPaymentMethodsRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string },
	PaginationResponse<PaymentMethod>,
	PaginationRequest
>(undefined, async (req) => {
	const responseBody = await applyPagination<PaymentMethod>(PaymentMethodDatabase, req.query, {
		filter: {
			tenantId: req.params.projectTenantId,
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

projectPaymentMethodsRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string; documentId: string },
	PaymentMethod,
	never
>(':documentId', async (req) => {
	const paymentMethod = await PaymentMethodDatabase.findOne({
		_id: req.params.documentId,
		tenantId: req.params.projectTenantId,
	})
		.lean<PaymentMethod>()
		.orFail(new Error('transl.error.notFound.paymentMethod'));

	return {
		statusCode: 200,
		responseBody: paymentMethod,
	};
});

projectPaymentMethodsRouter.postAsUser<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string },
	Partial<PaymentMethod>,
	{ _id: string },
	never
>(undefined, async (req) => {
	delete req.body._id;

	const paymentMethod = await PaymentMethodDatabase.create({
		...req.body,
		tenantId: req.params.projectTenantId,
		created: new Date(),
	});

	return {
		statusCode: 200,
		responseBody: {
			_id: paymentMethod._id,
		},
	};
});

projectPaymentMethodsRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string; documentId: string },
	Partial<PaymentMethod>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await PaymentMethodDatabase.updateMany(
		{
			_id: req.params.documentId,
			tenantId: req.params.projectTenantId,
		},
		update
	);
	return {
		statusCode: 200,
	};
});

projectPaymentMethodsRouter.deleteAsUser<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string; documentId: string },
	never,
	never
>(':documentId', async (req) => {
	await PaymentMethodDatabase.deleteMany({
		_id: req.params.documentId,
		tenantId: req.params.projectTenantId,
	} as any);

	return {
		statusCode: 200,
	};
});

projectPaymentMethodsRouter.postAsGuest<
	{
		tenantId: string;
		projectId: string;
		environmentId: string;
		projectTenantId: string;
		paymentMethodType: string;
	},
	never,
	any
>('initialize/:paymentMethodType', async (req) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const projectTenant = await getTenant(req.params.tenantId, req.params.projectId, req.params.environmentId, req.params.projectTenantId);

	if (req.params.paymentMethodType === 'STRIPE') {
		if (!tenant.stripePublishableKey || !tenant.stripeSecretKey) {
			throw new Error('transl.error.paymentMethodNotConfigured');
		}

		const stripe = new Stripe(tenant.stripeSecretKey, {
			apiVersion: '2020-08-27',
		});

		if (!projectTenant.stripeCustomerId) {
			const customer = await stripe.customers.create({
				name: Types.ObjectId(projectTenant._id).toHexString(),
			});
			projectTenant.stripeCustomerId = customer.id;
			await TenantDatabase.updateMany({ _id: projectTenant._id }, { stripeCustomerId: customer.id });
		}

		const setupIntent = await stripe.setupIntents.create({
			customer: projectTenant.stripeCustomerId,
			usage: 'off_session',
			payment_method_types: ['sofort', 'sepa_debit', 'card'],
		});
		const clientSecret = setupIntent.client_secret;
		return {
			statusCode: 200,
			responseBody: { publishableKey: tenant.stripePublishableKey, customerId: projectTenant.stripeCustomerId, clientSecret },
		};
	}

	throw new Error('transl.error.notFound.paymentMethodType');
});

import { User } from '@kominal/core-common';
import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { applyPaginationWithMapping, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { hash } from 'bcryptjs';
import { SALT } from '../helper/environment';
import { generateToken, getEnvironment, getProject, sendPasswordResetMail, sendWelcomeMail } from '../helper/helper';
import { RoleDatabase } from '../models/role';
import { TenantDatabase } from '../models/tenant';
import { UserDatabase } from '../models/user';
import { createSession } from './sessions';

type UserInformation = { _id: string; email: string };

export const usersRouter = new Router('/tenants/:tenantId');

usersRouter.postAsGuest<never, { baseUrl: string; email: string }, never, never>('users/email/request-verify', async (req, res) => {
	verifyParameter(['baseUrl', 'email'], req.body);
	const { baseUrl } = req.body;

	const email = req.body.email.toLowerCase();

	const user = await UserDatabase.findOne({ email });

	if (!user) {
		return { statusCode: 200 };
	}

	if (user.verified) {
		throw new Error('transl.error.email.alreadyVerified');
	}

	const token = generateToken(16);

	await user.updateOne({ token });

	await sendWelcomeMail(email, baseUrl, user._id, token);

	return { statusCode: 200 };
});

usersRouter.postAsGuest<
	{ tenantId: string; projectId: string; environmentId: string },
	{ userId: string; environmentId: string; token: string; key?: string; device?: string },
	never,
	never
>('projects/:projectId/environments/:environmentId/users/email/verify', async (req, res) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const project = await getProject(tenant._id, req.params.projectId);
	const environment = await getEnvironment(tenant._id, req.params.projectId, req.params.environmentId);

	verifyParameter(['userId', 'environmentId', 'token'], req.body);
	const { userId, token, key, device } = req.body;

	await UserDatabase.findOne({ _id: userId, token, verified: false }).orFail(new Error('transl.error.email.alreadyVerified'));

	await UserDatabase.updateOne({ _id: userId, token, verified: false }, { token: undefined, verified: true });

	if (key && device) {
		const keyHash = await hash(key, SALT);
		await createSession(userId, environment._id, keyHash, device);
	}

	return { statusCode: 200 };
});

usersRouter.putAsUser<never, { currentPassword: string; newPassword: string }, never, never>(
	'users/password/change',
	async (req, res, userId) => {
		verifyParameter(['currentPassword', 'newPassword'], req.body);
		const { currentPassword, newPassword } = req.body;

		const passwordHash = await hash(currentPassword, SALT);

		const user = await UserDatabase.findOne({ _id: userId, passwordHash }).orFail(new Error('transl.error.login.failed'));

		await user.updateOne({ passwordHash: await hash(newPassword, SALT) });

		return { statusCode: 200 };
	}
);

usersRouter.postAsGuest<never, { baseUrl: string; email: string }, never, never>('users/password/request-reset', async (req, res) => {
	verifyParameter(['email'], req.body);
	const { baseUrl } = req.body;

	const email = req.body.email.toLowerCase();

	const user = await UserDatabase.findOne({ email });

	if (!user) {
		return { statusCode: 200 };
	}

	if (!user.verified) {
		throw new Error('transl.error.email.notVerified');
	}

	const token = generateToken(16);

	await user.updateOne({ token });

	try {
		await sendPasswordResetMail(email, baseUrl, user._id, token);
		return { statusCode: 200 };
	} catch (e) {
		console.log(e);
		throw e;
	}
});

usersRouter.postAsGuest<never, { userId: string; token: string; password: string }, never, never>(
	'users/password/reset',
	async (req, res) => {
		verifyParameter(['userId', 'token', 'password'], req.body);
		const { userId, token, password } = req.body;

		const passwordHash = await hash(password, SALT);
		await UserDatabase.updateOne({ _id: userId, token, verified: true }, { token: undefined, passwordHash });

		return { statusCode: 200 };
	}
);

usersRouter.getAsUser<{ tenantId: string }, PaginationResponse<UserInformation>, PaginationRequest>(
	'users',
	async (req) => {
		const roleIds = (await RoleDatabase.find({ tenantId: req.params.tenantId }, '_id')).map((r) => r._id);
		const filter = { roles: { $in: roleIds } };

		const responseBody = await applyPaginationWithMapping<User, UserInformation>(
			UserDatabase,
			req.query,
			async (user) => ({
				_id: user._id,
				email: user.email,
			}),
			{
				filter,
			}
		);

		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

usersRouter.getAsUser<{ _id: string }, { _id: string; email: string }, never>('users/:_id', async (req) => {
	const user = await UserDatabase.findById(req.params._id).orFail(new Error('transl.error.notFound.user'));

	return {
		statusCode: 200,
		responseBody: {
			_id: user._id,
			email: user.email,
		},
	};
});

usersRouter.getAsUser<{ tenantId: string; email: string }, { _id: string; email: string }, never>(
	'users/find-by-email/:email',
	async (req) => {
		const user = await UserDatabase.findOne({ tenantId: req.params.tenantId, email: req.params.email }).orFail(
			new Error('transl.error.notFound.user')
		);

		return {
			statusCode: 200,
			responseBody: {
				_id: user._id,
				email: user.email,
			},
		};
	}
);

usersRouter.deleteAsUser<{ userId: string }, never, { tenantId: string }>(
	'users/:userId',
	async (req, res, userId) => {
		if (req.params.userId === userId) {
			throw new Error('transl.error.lockOutPrevented');
		}

		if (req.query.tenantId) {
			const roleIds = (await RoleDatabase.find({ tenantId: req.query.tenantId }, '_id')).map((r) => r._id);
			await UserDatabase.updateOne({ _id: req.params.userId }, { $pullAll: { roles: roleIds } });
		} else {
			await UserDatabase.deleteOne({ _id: req.params.userId });
		}

		return {
			statusCode: 200,
		};
	},
	{
		permissions: ['core.user-service.write'],
	}
);

usersRouter.getAsUser<{ tenantId: string; userId: string }, string[], never>(
	'users/:userId/permissions',
	async (req) => {
		const permissions: string[] = [];

		const roleIds = (await RoleDatabase.find({ tenantId: req.params.tenantId }, '_id')).map((r) => String(r._id));

		const user = await UserDatabase.findById(req.params.userId).orFail(new Error('transl.error.notFound.user'));

		for (const role of await RoleDatabase.find({ _id: { $in: user.roles.find((r) => roleIds.includes(String(r))) } })) {
			for (const permission of role.permissions) {
				if (!permissions.includes(permission)) {
					permissions.push(permission);
				}
			}
		}

		return { statusCode: 200, responseBody: permissions };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

usersRouter.putAsUser<{ tenantId: string; userId: string }, { permissions: string[] }, never>(
	'users/:userId/permissions',
	async (req, res, userId) => {
		verifyParameter(['permissions'], req.body);

		if (req.params.userId === userId && !req.body.permissions.includes('core.user-service.write')) {
			throw new Error('transl.error.lockOutPrevented');
		}

		await RoleDatabase.updateOne(
			{ tenantId: req.params.tenantId, name: `Role for ${req.params.userId}` },
			{ permissions: req.body.permissions, default: false },
			{ upsert: true }
		);

		const role = await RoleDatabase.findOne({ tenantId: req.params.tenantId, name: `Role for ${req.params.userId}` }).orFail(
			new Error('transl.error.notFound.role')
		);

		await UserDatabase.updateOne({ _id: req.params.userId, roles: { $nin: role._id } }, { $push: { roles: role._id } });

		return { statusCode: 200 };
	},
	{
		permissions: ['core.user-service.write'],
	}
);

usersRouter.postAsGuest<{ tenantId: string }, { baseUrl: string; email: string; password: string }, { verifyEmail: boolean }, never>(
	'users',
	async (req) => {
		verifyParameter(['baseUrl', 'email', 'password'], req.body);

		const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));

		const { baseUrl, password } = req.body;

		const email = req.body.email.toLowerCase();

		const regexp = new RegExp(
			// eslint-disable-next-line
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		);

		if (!email.match(regexp)) {
			throw new Error('transl.error.email.invalid');
		}

		if (await UserDatabase.exists({ email })) {
			throw new Error('transl.error.email.inUse');
		}
		const passwordHash = await hash(password, SALT);

		const token = generateToken(16);
		const user = await UserDatabase.create({
			tenantId: tenant._id,
			email,
			passwordHash,
			verified: !tenant.enableEmailVerification,
			token,
			roles: tenant.enableEmailVerification ? [] : (await RoleDatabase.find({ default: true })).map((p) => p._id),
		});

		/*
		if (!ENABLE_MULTI_TENANT && (await UserDatabase.countDocuments()) === 1) {
			await RoleDatabase.updateOne(
				{ name: `Role for ${user._id}` },
				{ permissions: ['core.user-service.write'], default: false },
				{ upsert: true }
			);

			const role = await RoleDatabase.findOne({ name: `Role for ${user._id}` }).orFail(new Error('transl.error.notFound.role'));

			await UserDatabase.updateOne({ _id: user._id, roles: { $nin: role._id } }, { $push: { roles: role._id } });
		}
		*/

		if (tenant.enableEmailVerification) {
			await sendWelcomeMail(email, baseUrl, user._id, token);
		}

		return { statusCode: 200, responseBody: { verifyEmail: tenant.enableEmailVerification } };
	}
);

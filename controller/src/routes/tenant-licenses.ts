import { Router } from '@kominal/lib-node-express-router';
import { getTenant } from '../helper/helper';
import { LicenseDatabase } from '../models/license';
import { TenantDatabase } from '../models/tenant';

export const tenantLicensesRouter = new Router(
	'/tenants/:tenantId/projects/:projectId/environments/:environmentId/tenants/:projectTenantId/licenses'
);

tenantLicensesRouter.getAsGuest<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string },
	{
		licenseId: string;
		added: Date;
		removed?: Date;
	}[],
	never
>(undefined, async (req) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const projectTenant = await getTenant(req.params.tenantId, req.params.projectId, req.params.environmentId, req.params.projectTenantId);

	return {
		statusCode: 200,
		responseBody: projectTenant.licenses || [],
	};
});

tenantLicensesRouter.putAsGuest<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string },
	{ licenseId: string },
	never
>(undefined, async (req) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const projectTenant = await getTenant(req.params.tenantId, req.params.projectId, req.params.environmentId, req.params.projectTenantId);

	const licenses = await LicenseDatabase.find({ tenantId: req.params.tenantId, projectId: req.params.projectId });
	const license = licenses.find((l) => String(l._id) === req.body.licenseId);

	if (!license) {
		throw new Error('transl.error.notFound.license');
	}

	const tenantLicenses = projectTenant.licenses || [];

	tenantLicenses.push({ licenseId: license._id, added: new Date() });

	await projectTenant.updateOne({ licenses: tenantLicenses });

	return {
		statusCode: 200,
	};
});

tenantLicensesRouter.deleteAsGuest<
	{ tenantId: string; projectId: string; environmentId: string; projectTenantId: string; licenseId: string },
	{ licenseId: string },
	never
>(':licenseId', async (req) => {
	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const projectTenant = await getTenant(req.params.tenantId, req.params.projectId, req.params.environmentId, req.params.projectTenantId);

	const tenantLicenses = projectTenant.licenses || [];

	const license = tenantLicenses.find((l) => l.licenseId === req.params.licenseId);

	if (license) {
		license.removed = new Date();
	}

	await projectTenant.updateOne({ licenses: tenantLicenses });

	return {
		statusCode: 200,
	};
});

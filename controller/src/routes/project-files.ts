import { File } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { removeFromCache } from '../helper/file-cache';
import { FileDatabase } from '../models/file';

export const projectFilesRouter = new Router('/tenants/:tenantId/projects/:projectId/files');

projectFilesRouter.getAsGuest<{ tenantId: string; projectId: string; key: string }, any, never>('byKey/:key', async (req, res) => {
	const file = await FileDatabase.findOne({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		key: req.params.key,
	});

	if (!file) {
		return {
			statusCode: 404,
		};
	}

	const { value } = file;

	const [type, content] = value.split(',');

	const img = Buffer.from(content, 'base64');
	return res
		.writeHead(200, {
			'Content-Type': type.split(';')[0].split(':')[1],
			'Content-Length': img.length,
		})
		.end(img);
});

projectFilesRouter.getAsUser<{ tenantId: string; projectId: string }, PaginationResponse<File>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<File>(FileDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
			},
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

projectFilesRouter.getAsUser<{ fileId: string }, File, never>(':fileId', async (req) => ({
	statusCode: 200,
	responseBody: await FileDatabase.findOne({ _id: req.params.fileId }).lean().orFail(new Error('transl.error.notFound.file')),
}));

projectFilesRouter.postAsUser<{ tenantId: string; projectId: string }, Partial<File>, { _id: string }, never>(undefined, async (req) => {
	delete req.body._id;
	delete req.body.tenantId;
	delete req.body.projectId;

	const file = await FileDatabase.create({
		...req.body,
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
	});

	removeFromCache(`${req.params.tenantId}|${req.params.projectId}`);

	return {
		statusCode: 200,
		responseBody: {
			_id: file._id,
		},
	};
});

projectFilesRouter.putAsUser<{ tenantId: string; projectId: string; fileId: string }, File & { tenantId: never }, never, never>(
	':fileId',
	async (req) => {
		delete req.body.tenantId;
		await FileDatabase.updateMany({ tenantId: req.params.tenantId, projectId: req.params.projectId, _id: req.params.fileId }, req.body);
		removeFromCache(`${req.params.tenantId}|${req.params.projectId}`);
		return {
			statusCode: 200,
		};
	}
);

projectFilesRouter.deleteAsUser<{ tenantId: string; projectId: string; fileId: string }, never, never>(':fileId', async (req) => {
	await FileDatabase.deleteMany({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		_id: req.params.fileId,
	});
	removeFromCache(`${req.params.tenantId}|${req.params.projectId}`);
	return {
		statusCode: 200,
	};
});

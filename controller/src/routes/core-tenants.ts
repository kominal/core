import { Tenant } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { TenantDatabase } from '../models/tenant';

export const coreTenantsRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/core-tenants');

coreTenantsRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string }, PaginationResponse<Tenant>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Tenant>(TenantDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
				environmentId: req.params.environmentId,
			},
		});

		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

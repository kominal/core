import { Invoice } from '@kominal/core-common';
import { CrudWithTenantRouter } from '@kominal/lib-node-express-router';
import { InvoiceDatabase } from '../models/invoice';

export const invoicesRouter = new CrudWithTenantRouter<Invoice>(InvoiceDatabase, 'invoice');

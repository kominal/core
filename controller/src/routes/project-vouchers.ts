import { License, Project, Tenant } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { LicenseDatabase } from '../models/license';

export const projectVouchersRouter = new Router('/tenants/:tenantId/projects/:projectId/vouchers');

projectVouchersRouter.getAsUser<{ tenantId: string; projectId: string }, PaginationResponse<License>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<License>(LicenseDatabase, req.query, {
			filter: { tenantId: req.params.tenantId, projectId: req.params.projectId },
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

projectVouchersRouter.getAsUser<{ tenantId: string; projectId: Project['_id']; documentId: License['_id'] }, License, never>(
	':documentId',
	async (req) => {
		const license = await LicenseDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			_id: req.params.documentId,
		})
			.lean<License>()
			.orFail(new Error('transl.error.notFound.license'));

		return {
			statusCode: 200,
			responseBody: license,
		};
	}
);

projectVouchersRouter.postAsUser<{ tenantId: string; projectId: string }, Partial<License>, { _id: License['_id'] }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const environment = await LicenseDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			created: new Date(),
		} as any);

		return {
			statusCode: 200,
			responseBody: {
				_id: environment._id,
			},
		};
	}
);

projectVouchersRouter.putAsUser<
	{ tenantId: Tenant['_id']; projectId: Project['_id']; documentId: License['_id'] },
	Partial<License>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await LicenseDatabase.updateMany(
		{ _id: req.params.documentId, projectId: req.params.projectId, tenantId: req.params.tenantId } as any,
		update
	);
	return {
		statusCode: 200,
	};
});

projectVouchersRouter.deleteAsUser<{ tenantId: Tenant['_id']; projectId: Project['_id']; documentId: License['_id'] }, never, never>(
	':documentId',
	async (req) => {
		await LicenseDatabase.deleteMany({
			_id: req.params.documentId,
			projectId: req.params.projectId,
			tenantId: req.params.tenantId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

projectVouchersRouter.getAsGuest<{ tenantId: string; projectId: string; slug: string }, License, never>('by-slug/:slug', async (req) => ({
	statusCode: 200,
	responseBody: await LicenseDatabase.findOne({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		slug: req.params.slug,
	})
		.lean()
		.orFail(new Error('transl.error.notFound.license')),
}));

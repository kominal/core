import { Session } from '@kominal/core-common';
import { Router, verifyParameter } from '@kominal/lib-node-express-router';
import { Payload } from '@kominal/lib-node-express-router/models/payload';
import { Document } from '@kominal/lib-node-mongodb-interface';
import { hash } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import { SALT } from '../helper/environment';
import { getEnvironment, getProject } from '../helper/helper';
import { RoleDatabase } from '../models/role';
import { SessionDatabase } from '../models/session';
import { TenantDatabase } from '../models/tenant';
import { UserDatabase } from '../models/user';

export function createSession(userId: string, environmentId: string, keyHash: string, device: string): Promise<Document & Session> {
	return SessionDatabase.create({
		userId,
		environmentId,
		keyHash,
		device,
		created: new Date(),
		lastActive: new Date(),
	});
}

export const sessionsRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/sessions');

sessionsRouter.postAsGuest<
	{ tenantId: string; projectId: string; environmentId: string },
	{ email: string; password: string; device: string; key: string },
	never,
	never
>(undefined, async (req) => {
	verifyParameter(['email', 'password', 'device', 'key'], req.body);

	const { password, device, key } = req.body;

	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));
	const project = await getProject(tenant._id, req.params.projectId);
	const environment = await getEnvironment(tenant._id, req.params.projectId, req.params.environmentId);

	const email = req.body.email.toLowerCase();

	const passwordHash = await hash(password, SALT);
	const user = await UserDatabase.findOne({ tenantId: tenant._id, email, passwordHash }).orFail(new Error('transl.error.login.failed'));

	if (!user.verified) {
		throw new Error('transl.error.email.notVerified');
	}

	const keyHash = await hash(key, SALT);
	await createSession(user._id, environment._id, keyHash, device);

	return { statusCode: 200 };
});

sessionsRouter.getAsGuest<
	{ tenantId: string; projectId: string; environmentId: string; documentId: string },
	{ userId: string; payload: Payload; jwt: string; jwtExpires: number },
	never
>(undefined, async (req, res) => {
	if (!req.headers.authorization || !req.headers.authorization.split(' ')[1]) {
		throw new Error('transl.error.session.invalid');
	}

	const tenant = await TenantDatabase.findById(req.params.tenantId).orFail(new Error('transl.error.notFound.tenant'));

	const keyHash = await hash(req.headers.authorization.split(' ')[1], SALT);

	const session = await SessionDatabase.findOne({ keyHash }).orFail(new Error('transl.error.session.invalid'));

	await session.updateOne({ lastActive: new Date() });

	const { userId } = session;
	const user = await UserDatabase.findById(session.userId).orFail(new Error('transl.error.user.notfound'));

	const jwtCreated = Date.now();
	const exp = jwtCreated + 60 * 10 * 1000;

	const payload: Payload = {
		permissions: [],
		tenants: {},
	};

	const roles = await Promise.all(user.roles.map((id) => RoleDatabase.findById(id)));

	for (const role of roles) {
		if (role) {
			if (role.tenantId) {
				const { tenantId } = role;
				if (!payload.tenants[tenantId]) {
					payload.tenants[tenantId] = { permissions: [] };
				}
			}

			for (const permission of role.permissions) {
				const permissions = role.tenantId ? payload.tenants[role.tenantId].permissions : payload.permissions;
				if (!permissions.includes(permission)) {
					permissions.push(permission);
				}
			}
		}
	}

	const jwt = sign(
		{
			userId,
			exp,
			payload,
		},
		tenant.jsonWebTokenKey
	);

	return {
		statusCode: 200,
		responseBody: {
			userId,
			payload,
			jwt,
			jwtCreated,
			jwtExpires: exp,
		},
	};
});

sessionsRouter.deleteAsGuest<never, never, never>(undefined, async (req, res) => {
	if (!req.headers.authorization || !req.headers.authorization.split(' ')[1]) {
		throw new Error('transl.error.session.invalid');
	}

	const key = await hash(req.headers.authorization.split(' ')[1], SALT);
	const keyHash = await hash(key, SALT);

	await SessionDatabase.deleteMany({ keyHash });

	return { statusCode: 200 };
});

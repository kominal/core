import { Tenant } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_TENANT_ID } from '../helper/environment';
import { TenantDatabase } from '../models/tenant';

export const tenantsRouter = new Router('/tenants');

tenantsRouter.getAsUser<never, PaginationResponse<Tenant>, PaginationRequest>(undefined, async (req, _res, _userId, payload) => {
	if (!payload) {
		throw new Error('transl.error.missing.payload');
	}

	const responseBody = await applyPagination<Tenant>(TenantDatabase, req.query, {
		projection: 'slug name',
		filter: {
			tenantId: CORE_TENANT_ID,
			projectId: CORE_PROJECT_ID,
			environmentId: CORE_ENVIRONMENT_ID,
			_id: { $in: Object.keys(payload.tenants) },
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

tenantsRouter.getAsGuest<{ tenantSlug: string }, Tenant, never>('by-slug/:tenantSlug', async (req) => ({
	statusCode: 200,
	responseBody: await TenantDatabase.findOne({
		tenantId: CORE_TENANT_ID,
		projectId: CORE_PROJECT_ID,
		environmentId: CORE_ENVIRONMENT_ID,
		slug: req.params.tenantSlug,
	})
		.lean()
		.orFail(new Error('transl.error.notFound.tenant')),
}));

tenantsRouter.getAsUser<{ documentId: string }, Tenant, never>(':documentId', async (req) => ({
	statusCode: 200,
	responseBody: await TenantDatabase.findOne({
		_id: req.params.documentId,
		tenantId: CORE_TENANT_ID,
		projectId: CORE_PROJECT_ID,
		environmentId: CORE_ENVIRONMENT_ID,
	})
		.lean()
		.orFail(new Error('transl.error.notFound.tenant')),
}));

tenantsRouter.putAsUser<{ tenantId: string }, Tenant & { tenantId: never }, never, never>(':tenantId', async (req) => {
	delete req.body.tenantId;

	if (req.body.slug) {
		const slug = req.body.slug.toLowerCase();

		if (!slug.match(new RegExp(/^[a-zA-Z0-9_-]*$/))) {
			throw new Error('transl.error.tenantSlugInvalid');
		}
		const tenant = await TenantDatabase.findById(req.params.tenantId);
		if (tenant && tenant.slug !== slug) {
			if (await TenantDatabase.exists({ slug })) {
				throw new Error('transl.error.tenantSlugAlreadyInUse');
			}
		}
	}

	await TenantDatabase.updateMany({ _id: req.params.tenantId }, req.body);
	return {
		statusCode: 200,
	};
});

import { Translation } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse, Types } from '@kominal/lib-node-mongodb-interface';
import { removeCache, translationCache } from '../helper/translation-cache';
import { TenantDatabase } from '../models/tenant';
import { TranslationDatabase } from '../models/translation';

export const projectTranslationsRouter = new Router('/tenants/:tenantId/projects/:projectId/translations');

projectTranslationsRouter.getAsGuest<{ tenantId: string; projectId: string; language: string }, any, never>(
	'languages/:language',
	async (req) => {
		const { tenantId, projectId, language } = req.params;

		const cacheKey = `${tenantId}|${projectId}|${language}`;

		const cache = translationCache.get(cacheKey);

		if (cache) {
			return { statusCode: 200, responseBody: cache };
		}

		const translations: { values: { value: string }; key: string }[] = await TranslationDatabase.aggregate([
			{ $unwind: '$values' },
			{
				$match: {
					tenantId: Types.ObjectId(tenantId),
					projectId: Types.ObjectId(projectId),
					'values.language': language,
				},
			},
		]);
		const keys = translations.map((t) => t.key);
		const globalTransactions: { values: { value: string }; key: string }[] = await TranslationDatabase.aggregate([
			{ $unwind: '$values' },
			{
				$match: {
					tenantId: Types.ObjectId(tenantId),
					projectId: { $exists: false },
					'values.language': language,
					key: { $nin: keys },
				},
			},
		]);

		const responseBody: any = {};

		for (const translation of [...translations, ...globalTransactions]) {
			const parts = translation.key.split('.');

			let step = responseBody;
			for (let index = 0; index < parts.length; index += 1) {
				const part = parts[index];
				if (index === parts.length - 1) {
					step[part] = translation.values.value || translation.key;
				} else if (!step[part] || typeof step[part] === 'string') {
					step[part] = {};
				}
				step = step[part];
			}
		}

		translationCache.set(cacheKey, responseBody);

		return { statusCode: 200, responseBody };
	}
);

projectTranslationsRouter.getAsGuest<
	{ tenantId: string; projectId: string; language: string; key: string },
	{ translation: string },
	never
>('languages/:language/keys/:key', async (req) => {
	let translation = '';

	const projectTranslation = await TranslationDatabase.findOne({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		key: req.params.key,
	});

	if (projectTranslation) {
		translation = projectTranslation.values.find((t) => t.language === req.params.language)?.value || '';
	} else {
		const globalTranslation = await TranslationDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: { $exists: false },
			key: req.params.key,
		});

		if (globalTranslation) {
			translation = globalTranslation.values.find((t) => t.language === req.params.language)?.value || '';
		} else {
			await TranslationDatabase.create({
				tenantId: req.params.tenantId,
				projectName: req.params.projectId,
				key: req.params.key,
				type: 'SINGLE',
				values: [],
			});
		}
	}

	return {
		statusCode: 200,
		responseBody: {
			translation,
		},
	};
});

projectTranslationsRouter.getAsUser<{ tenantId: string; projectId: string }, PaginationResponse<Translation>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Translation>(TranslationDatabase, req.query, {
			filter: {
				tenantId: req.params.tenantId,
				projectId: req.params.projectId,
			},
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

projectTranslationsRouter.getAsUser<{ translationId: string }, Translation, never>(':translationId', async (req) => ({
	statusCode: 200,
	responseBody: await TranslationDatabase.findOne({ _id: req.params.translationId })
		.lean()
		.orFail(new Error('transl.error.notFound.translation')),
}));

projectTranslationsRouter.postAsUser<{ tenantId: string; projectId: string }, Partial<Translation>, { _id: string }, never>(
	undefined,
	async (req) => {
		delete req.body._id;
		delete req.body.tenantId;
		delete req.body.projectId;

		const translation = await TranslationDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
		});

		removeCache(`${req.params.tenantId}|${req.params.projectId}`);

		return {
			statusCode: 200,
			responseBody: {
				_id: translation._id,
			},
		};
	}
);

projectTranslationsRouter.postAsGuest<{ tenantId: string; projectId: string }, { language: string; key: string }[], never, never>(
	'missing',
	async (req) => {
		try {
			if (!TenantDatabase.exists({ _id: req.params.tenantId })) {
				throw new Error('transl.error.tenant.notFound');
			}

			for (const { key } of req.body) {
				if (
					!(await TranslationDatabase.exists({
						tenantId: req.params.tenantId,
						projectId: req.params.projectId,
						key,
					}))
				) {
					await TranslationDatabase.create({
						tenantId: req.params.tenantId,
						projectId: req.params.projectId,
						key,
						type: 'SINGLE',
						values: [],
					});
				}
			}
		} catch (e) {
			console.log(e);
		}
		removeCache(`${req.params.tenantId}|${req.params.projectId}`);

		return {
			statusCode: 200,
		};
	}
);

projectTranslationsRouter.putAsUser<
	{ tenantId: string; projectId: string; translationId: string },
	Translation & { tenantId: never },
	never,
	never
>(':translationId', async (req) => {
	delete req.body.tenantId;
	await TranslationDatabase.updateMany(
		{ tenantId: req.params.tenantId, projectId: req.params.projectId, _id: req.params.translationId },
		req.body
	);
	removeCache(`${req.params.tenantId}|${req.params.projectId}`);
	return {
		statusCode: 200,
	};
});

projectTranslationsRouter.deleteAsUser<{ tenantId: string; projectId: string; translationId: string }, never, never>(
	':translationId',
	async (req) => {
		await TranslationDatabase.deleteMany({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			_id: req.params.translationId,
		});
		removeCache(`${req.params.tenantId}|${req.params.projectId}`);
		return {
			statusCode: 200,
		};
	}
);

import { Invoice } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { InvoiceDatabase } from '../models/invoice';

export const projectInvoicesRouter = new Router(
	'/tenants/:tenantId/projects/:projectId/environments/:environmentId/tenants/:projectTenantId/invoices'
);

projectInvoicesRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string },
	PaginationResponse<Invoice>,
	PaginationRequest
>(undefined, async (req) => {
	const responseBody = await applyPagination<Invoice>(InvoiceDatabase, req.query, {
		filter: {
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

projectInvoicesRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, Invoice, never>(
	':documentId',
	async (req) => {
		const invoice = await InvoiceDatabase.findOne({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		})
			.lean<Invoice>()
			.orFail(new Error('transl.error.notFound.invoice'));

		return {
			statusCode: 200,
			responseBody: invoice,
		};
	}
);

projectInvoicesRouter.postAsUser<{ tenantId: string; projectId: string; environmentId: string }, Partial<Invoice>, { _id: string }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const invoice = await InvoiceDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			created: new Date(),
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: invoice._id,
			},
		};
	}
);

projectInvoicesRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; documentId: string },
	Partial<Invoice>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await InvoiceDatabase.updateMany(
		{
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		},
		update
	);
	return {
		statusCode: 200,
	};
});

projectInvoicesRouter.deleteAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, never, never>(
	':documentId',
	async (req) => {
		await InvoiceDatabase.deleteMany({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

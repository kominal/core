import { User } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPaginationWithMapping, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { UserDatabase } from '../models/user';

type UserInformation = { _id: string; email: string };

export const coreUsersRouter = new Router('/tenants/:tenantId/core-users');

coreUsersRouter.getAsUser<{ tenantId: string }, PaginationResponse<UserInformation>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPaginationWithMapping<User, UserInformation>(
			UserDatabase,
			req.query,
			async (user) => ({
				_id: user._id,
				email: user.email,
			}),
			{
				filter: { tenantId: req.params.tenantId },
			}
		);

		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write', 'core.user-service.read'],
	}
);

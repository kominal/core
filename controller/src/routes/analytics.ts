import { Analytics } from '@kominal/core-common/models/analytics';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { AnalyticsDatabase } from '../models/analytics';

export const analyticsRouter = new Router(
	'/tenants/:tenantId/projects/:projectId/environments/:environmentId/services/:serviceId/analytics'
);

export function createTimeFilter(query: { start?: string; end?: string }): Promise<any> {
	const filter: any = {};
	if (query.start) {
		filter.time = filter.time || {};
		filter.time.$gt = new Date(query.start);
	}
	if (query.end) {
		filter.time = filter.time || {};
		filter.time.$lt = new Date(query.end);
	}
	return filter;
}

analyticsRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string; serviceId: string },
	PaginationResponse<Analytics>,
	PaginationRequest & { start?: string; end?: string; maxId?: string }
>(undefined, async (req) => {
	const idFilter = req.query.maxId ? { _id: { $lt: req.query.maxId } } : {};
	const responseBody = await applyPagination<Analytics>(AnalyticsDatabase, req.query, {
		filter: {
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			serviceId: req.params.serviceId,
			...createTimeFilter(req.query),
			...idFilter,
		},
		sorting: { time: -1 },
		count: false,
	});
	return { statusCode: 200, responseBody };
});

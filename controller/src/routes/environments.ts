import { Environment, Project, Tenant } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { EnvironmentDatabase } from '../models/environment';

export const environmentsRouter = new Router('/tenants/:tenantId/projects/:projectId/environments');

environmentsRouter.getAsUser<{ tenantId: string; projectId: string }, PaginationResponse<Environment>, PaginationRequest>(
	undefined,
	async (req) => {
		const responseBody = await applyPagination<Environment>(EnvironmentDatabase, req.query, {
			filter: { tenantId: req.params.tenantId, projectId: req.params.projectId },
		});

		return {
			statusCode: 200,
			responseBody,
		};
	}
);

environmentsRouter.getAsUser<{ tenantId: string; projectId: Project['_id']; documentId: Environment['_id'] }, Environment, never>(
	':documentId',
	async (req) => {
		const environment = await EnvironmentDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			_id: req.params.documentId,
		})
			.lean<Environment>()
			.orFail(new Error('transl.error.notFound.environment'));

		return {
			statusCode: 200,
			responseBody: environment,
		};
	}
);

environmentsRouter.postAsUser<{ tenantId: string; projectId: string }, Partial<Environment>, { _id: Environment['_id'] }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const environment = await EnvironmentDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			created: new Date(),
		} as any);

		return {
			statusCode: 200,
			responseBody: {
				_id: environment._id,
			},
		};
	}
);

environmentsRouter.putAsUser<
	{ tenantId: Tenant['_id']; projectId: Project['_id']; documentId: Environment['_id'] },
	Partial<Environment>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await EnvironmentDatabase.updateMany(
		{ _id: req.params.documentId, projectId: req.params.projectId, tenantId: req.params.tenantId } as any,
		update
	);
	return {
		statusCode: 200,
	};
});

environmentsRouter.deleteAsUser<{ tenantId: Tenant['_id']; projectId: Project['_id']; documentId: Environment['_id'] }, never, never>(
	':documentId',
	async (req) => {
		await EnvironmentDatabase.deleteMany({
			_id: req.params.documentId,
			projectId: req.params.projectId,
			tenantId: req.params.tenantId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

environmentsRouter.getAsGuest<{ tenantId: string; projectId: string; slug: string }, Environment, never>('by-slug/:slug', async (req) => ({
	statusCode: 200,
	responseBody: await EnvironmentDatabase.findOne({
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		slug: req.params.slug,
	})
		.lean()
		.orFail(new Error('transl.error.notFound.environment')),
}));

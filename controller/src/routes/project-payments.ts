import { Payment, PaymentMethod } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { PaymentDatabase } from '../models/payment';
import { PaymentMethodDatabase } from '../models/payment-method';

export const projectPaymentsRouter = new Router(
	'/tenants/:tenantId/projects/:projectId/environments/:environmentId/tenants/:projectTenantId/payments'
);

projectPaymentsRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string },
	PaginationResponse<Payment>,
	PaginationRequest
>(undefined, async (req) => {
	const responseBody = await applyPagination<Payment>(PaymentDatabase, req.query, {
		filter: {
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

projectPaymentsRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, PaymentMethod, never>(
	':documentId',
	async (req) => {
		const paymentMethod = await PaymentMethodDatabase.findOne({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		})
			.lean<PaymentMethod>()
			.orFail(new Error('transl.error.notFound.paymentMethod'));

		return {
			statusCode: 200,
			responseBody: paymentMethod,
		};
	}
);

projectPaymentsRouter.postAsUser<{ tenantId: string; projectId: string; environmentId: string }, Partial<Payment>, { _id: string }, never>(
	undefined,
	async (req) => {
		delete req.body._id;

		const payment = await PaymentDatabase.create({
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			created: new Date(),
		});

		return {
			statusCode: 200,
			responseBody: {
				_id: payment._id,
			},
		};
	}
);

projectPaymentsRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; documentId: string },
	Partial<Payment>,
	never,
	never
>(':documentId', async (req) => {
	const update: any = { ...req.body };
	update.tenantId = req.params.tenantId;
	update.projectId = req.params.projectId;

	await PaymentDatabase.updateMany(
		{
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		},
		update
	);
	return {
		statusCode: 200,
	};
});

projectPaymentsRouter.deleteAsUser<{ tenantId: string; projectId: string; environmentId: string; documentId: string }, never, never>(
	':documentId',
	async (req) => {
		await PaymentDatabase.deleteMany({
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
		} as any);

		return {
			statusCode: 200,
		};
	}
);

projectPaymentsRouter.getAsGuest<{ tenantId: string; projectId: string; environmentId: string; slug: string }, Payment, never>(
	'by-slug/:slug',
	async (req) => ({
		statusCode: 200,
		responseBody: await PaymentDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			slug: req.params.slug,
		})
			.lean()
			.orFail(new Error('transl.error.notFound.payment')),
	})
);

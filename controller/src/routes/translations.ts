import { Translation } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { removeCache } from '../helper/translation-cache';
import { TranslationDatabase } from '../models/translation';

export const translationsRouter = new Router('/tenants/:tenantId/translations');

translationsRouter.getAsUser<{ tenantId: string }, PaginationResponse<Translation>, PaginationRequest>(undefined, async (req) => {
	const responseBody = await applyPagination<Translation>(TranslationDatabase, req.query, {
		filter: {
			tenantId: req.params.tenantId,
			projectId: { $exists: false },
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

translationsRouter.getAsUser<{ tenantId: string; translationId: string }, Translation, never>(':translationId', async (req) => ({
	statusCode: 200,
	responseBody: await TranslationDatabase.findOne({ tenantId: req.params.tenantId, _id: req.params.translationId })
		.lean()
		.orFail(new Error('transl.error.notFound.translation')),
}));

translationsRouter.postAsUser<{ tenantId: string }, Partial<Translation>, { _id: string }, never>(undefined, async (req) => {
	delete req.body._id;
	delete req.body.tenantId;

	const translation = await TranslationDatabase.create({
		...req.body,
		tenantId: req.params.tenantId,
	});
	removeCache(req.params.tenantId);

	return {
		statusCode: 200,
		responseBody: {
			_id: translation._id,
		},
	};
});

translationsRouter.putAsUser<{ tenantId: string; translationId: string }, Translation & { tenantId: never }, never, never>(
	':translationId',
	async (req) => {
		delete req.body.tenantId;
		await TranslationDatabase.updateMany({ tenantId: req.params.tenantId, _id: req.params.translationId }, req.body);
		removeCache(req.params.tenantId);
		return {
			statusCode: 200,
		};
	}
);

translationsRouter.deleteAsUser<{ tenantId: string; translationId: string }, never, never>(':translationId', async (req) => {
	await TranslationDatabase.deleteMany({ tenantId: req.params.tenantId, _id: req.params.translationId });
	removeCache(req.params.tenantId);
	return {
		statusCode: 200,
	};
});

import { Check } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { CheckDatabase } from '../models/check';

export const checksRouter = new Router('/tenants/:tenantId/projects/:projectId/environments/:environmentId/services/:serviceId/checks');

checksRouter.getAsUser<
	{ tenantId: string; projectId: string; environmentId: string; serviceId: string },
	PaginationResponse<Check>,
	PaginationRequest
>(undefined, async (req) => {
	const responseBody = await applyPagination<Check>(CheckDatabase, req.query, {
		filter: {
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			serviceId: req.params.serviceId,
		},
	});

	return {
		statusCode: 200,
		responseBody,
	};
});

checksRouter.getAsUser<{ tenantId: string; projectId: string; environmentId: string; serviceId: string; documentId: string }, Check, never>(
	':documentId',
	async (req) => {
		const check = await CheckDatabase.findOne({
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			serviceId: req.params.serviceId,
			_id: req.params.documentId,
		})
			.lean<Check>()
			.orFail(new Error('transl.error.notFound.check'));

		return {
			statusCode: 200,
			responseBody: check,
		};
	}
);

checksRouter.postAsUser<
	{ tenantId: string; projectId: string; environmentId: string; serviceId: string },
	Partial<Check>,
	{ _id: string },
	never
>(undefined, async (req) => {
	delete req.body._id;

	const check = await CheckDatabase.create({
		...req.body,
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		environmentId: req.params.environmentId,
		serviceId: req.params.serviceId,
		created: new Date(),
	} as any);

	return {
		statusCode: 200,
		responseBody: {
			_id: check._id,
		},
	};
});

checksRouter.putAsUser<
	{ tenantId: string; projectId: string; environmentId: string; serviceId: string; documentId: string },
	Partial<Check>,
	never,
	never
>(':documentId', async (req) => {
	delete req.body._id;

	await CheckDatabase.updateMany(
		{
			_id: req.params.documentId,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			serviceId: req.params.serviceId,
		} as any,
		{
			...req.body,
			tenantId: req.params.tenantId,
			projectId: req.params.projectId,
			environmentId: req.params.environmentId,
			serviceId: req.params.serviceId,
		}
	);
	return {
		statusCode: 200,
	};
});

checksRouter.deleteAsUser<
	{ tenantId: string; projectId: string; environmentId: string; serviceId: string; documentId: string },
	never,
	never
>(':documentId', async (req) => {
	await CheckDatabase.deleteMany({
		_id: req.params.documentId,
		tenantId: req.params.tenantId,
		projectId: req.params.projectId,
		environmentId: req.params.environmentId,
		serviceId: req.params.serviceId,
	});

	return {
		statusCode: 200,
	};
});

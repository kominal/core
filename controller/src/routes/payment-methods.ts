import { PaymentMethod } from '@kominal/core-common';
import { CrudWithTenantRouter } from '@kominal/lib-node-express-router';
import { PaymentMethodDatabase } from '../models/payment-method';

export const paymentMethodsRouter = new CrudWithTenantRouter<PaymentMethod>(PaymentMethodDatabase, 'payment-methods');

import { Role } from '@kominal/core-common';
import { Router } from '@kominal/lib-node-express-router';
import { applyPagination, PaginationRequest, PaginationResponse } from '@kominal/lib-node-mongodb-interface';
import { RoleDatabase } from '../models/role';

export const rolesRouter = new Router();

rolesRouter.getAsUser<never, PaginationResponse<any>, PaginationRequest>(
	'/roles',
	async (req) => {
		const responseBody = await applyPagination<any>(RoleDatabase, req.query);
		return { statusCode: 200, responseBody };
	},
	{
		permissions: ['core.user-service.write'],
	}
);

rolesRouter.postAsUser<never, Role, never, never>(
	'/roles',
	async (req) => {
		try {
			await new RoleDatabase(req.body).save();
			return { statusCode: 200 };
		} catch (e) {
			return { statusCode: 406, error: 'transl.error.role.save' };
		}
	},
	{
		permissions: ['core.user-service.write'],
	}
);

rolesRouter.putAsUser<{ _id: string }, Role, never, never>(
	'/roles/:_id',
	async (req) => {
		try {
			await RoleDatabase.updateMany({ _id: req.params }, req.body);
			return { statusCode: 200 };
		} catch (e) {
			return { statusCode: 406, error: 'transl.error.role.update' };
		}
	},
	{
		permissions: ['core.user-service.write'],
	}
);

rolesRouter.deleteAsUser<{ _id: string }, never, never>(
	'/roles/:_id',
	async (req) => {
		try {
			await RoleDatabase.deleteMany({ _id: req.params._id });
			return { statusCode: 200 };
		} catch (e) {
			return { statusCode: 406, error: 'transl.error.role.save' };
		}
	},
	{
		permissions: ['core.user-service.write'],
	}
);

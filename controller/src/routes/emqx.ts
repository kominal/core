import { debug } from '@kominal/core-node-client';
import { Router } from '@kominal/lib-node-express-router';
import { EnvironmentDatabase } from '../models/environment';
import { ServiceDatabase } from '../models/service';

export const emqxRouter = new Router();

emqxRouter.getAsGuest<never, never, { username: string; password: string }>('/emqx/auth', async (req, res) => ({ statusCode: 200 }));

emqxRouter.getAsGuest<never, never, { username: string; password: string }>('/emqx/service', async (req) => {
	console.log('EMQX SERVICE', req.body, req.params, req.query);
	const { username, password } = req.query;
	if (username === 'SERVICE') {
		return { statusCode: 200 };
	}
	return { statusCode: 401 };
});

emqxRouter.getAsGuest<
	never,
	never,
	{
		access: '1' | '2'; // 1 = sub, 2 = pub
		username: 'JWT' | 'ENVIRONMENT_TOKEN' | 'SERVICE_TOKEN' | undefined;
		password: string | undefined;
		clientid: string;
		ipaddr: string;
		topic: string;
	}
>('/emqx/acl', async (req) => {
	console.log('ACL', req.body, req.params, req.query);
	/**
	 * username: JWT | ENVRIONMENT_TOKEN | SERVICE_TOKEN | (empty)
	 * password: actual auth
	 * check if auth method fits the environtment/service/user allow
	 * check how super requests are handled
	 */

	const unauthenticated: string[] = ['analytics'];
	const authenticated: { queue: string; sub: boolean; pub: boolean }[] = [];
	const [tenantId, projectId, environmentId, serviceId, task, queue] = req.query.topic.split('/');
	debug(`${queue} ${req.query.username} ${req.query.password}`);

	if (unauthenticated.includes(queue) && req.query.access === '2') {
		return { statusCode: 200 };
	}

	if (req.query.username === 'ENVIRONMENT_TOKEN' && req.query.password) {
		const environment = await EnvironmentDatabase.findById(environmentId);
		if (environment?.tokens.includes(req.query.password)) {
			return { statusCode: 200 };
		}
	}
	if (req.query.username === 'SERVICE_TOKEN' && req.query.password) {
		const service = await ServiceDatabase.findById(serviceId);
		if (service?.tokens.includes(req.query.password)) {
			return { statusCode: 200 };
		}
	}

	return { statusCode: 401 };
});

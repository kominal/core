export const fileCache = new Map<string, any>();

export function removeFromCache(cacheKey: string): void {
	for (const key of fileCache.keys()) {
		if (key.startsWith(cacheKey)) {
			fileCache.delete(key);
		}
	}
}

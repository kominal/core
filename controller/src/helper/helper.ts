import { Environment, Project, Tenant } from '@kominal/core-common';
import { getTranslation } from '@kominal/core-node-client/translations';
import { sendMail } from '@kominal/lib-node-mailer';
import { Document } from '@kominal/lib-node-mongodb-interface';
import { EnvironmentDatabase } from '../models/environment';
import { ProjectDatabase } from '../models/project';
import { TenantDatabase } from '../models/tenant';

export function generateToken(length: number): string {
	let result = '';
	const characters = '0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i += 1) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

export async function sendWelcomeMail(to: string, baseUrl: string, userId: string, token: string): Promise<void> {
	try {
		const text = await getTranslation('en', 'transl.mail.emailVerification.body', {
			BASE_URL: baseUrl,
			USER_ID: userId,
			TOKEN: token,
		});
		const subject = await getTranslation('en', 'transl.mail.emailVerification.subject', {
			BASE_URL: baseUrl,
			USER_ID: userId,
			TOKEN: token,
		});

		return sendMail({
			subject,
			to,
			text,
		});
	} catch (e) {
		console.log(`sendWelcomeEmail cause Error(${e})`);
		throw new Error(e);
	}
}

export async function sendPasswordResetMail(to: string, baseUrl: string, userId: string, token: string): Promise<void> {
	const text = await getTranslation('en', 'transl.mail.passwordReset.body', {
		BASE_URL: baseUrl,
		USER_ID: userId,
		TOKEN: token,
	});
	const subject = await getTranslation('en', 'transl.mail.passwordReset.subject', {
		BASE_URL: baseUrl,
		USER_ID: userId,
		TOKEN: token,
	});
	return sendMail({
		subject,
		to,
		text,
	});
}

export function createTimeFilter(query: { start?: string; end?: string }): Promise<any> {
	const filter: any = {};
	if (query.start) {
		filter.time = filter.time || {};
		filter.time.$gt = new Date(query.start);
	}
	if (query.end) {
		filter.time = filter.time || {};
		filter.time.$lt = new Date(query.end);
	}
	return filter;
}

export async function getTenant(
	tenantId: string,
	projectId: string,
	environmentId: string,
	documentId: string
): Promise<Document<any> & Tenant> {
	return TenantDatabase.findOne({
		_id: documentId,
		tenantId,
		projectId,
		environmentId,
	}).orFail(new Error('transl.error.notFound.tenant'));
}

export async function getProject(tenantId: string, projectId: string): Promise<Project> {
	return ProjectDatabase.findOne({ _id: projectId, tenantId }).orFail(new Error('transl.error.notFound.project'));
}

export async function getEnvironment(tenantId: string, projectId: string, environmentId: string): Promise<Environment> {
	return EnvironmentDatabase.findOne({
		_id: environmentId,
		projectId,
		tenantId,
	}).orFail(new Error('transl.error.notFound.environment'));
}

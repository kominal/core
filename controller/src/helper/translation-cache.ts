export const translationCache = new Map<string, any>();

export function removeCache(cacheKey: string): void {
	for (const key of translationCache.keys()) {
		if (key.startsWith(cacheKey)) {
			translationCache.delete(key);
		}
	}
}

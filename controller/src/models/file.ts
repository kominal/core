import { File } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const FileDatabase = model<Document & File>(
	'File',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			type: String,
			key: String,
			value: Schema.Types.Mixed,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, key: 1 }, { unique: true })
);

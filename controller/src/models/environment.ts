import { Environment } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const EnvironmentDatabase = model<Document & Environment>(
	'Environment',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			name: String,
			slug: String,
			tokens: [String],
		},
		{ minimize: false }
	).index({ tenantId: 1, slug: 1 }, { unique: true })
);

import { Translation } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const TranslationDatabase = model<Document & Translation>(
	'Translation',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			type: String,
			key: String,
			values: [Schema.Types.Mixed],
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, key: 1 }, { unique: true })
);

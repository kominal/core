import { Report } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const ReportDatabase = model<Document & Report>(
	'Report',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			emails: [String],
			events: [Schema.Types.Mixed],
			lastUpdated: Date,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, environmentId: 1 })
);

import { Tenant } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const TenantDatabase = model<Document & Tenant>(
	'Tenant',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			name: String,
			slug: String,
			enableEmailVerification: Boolean,
			jsonWebTokenKey: String,
			stripePublishableKey: String,
			stripeSecretKey: String,
			stripeCustomerId: String,
			licenses: [Schema.Types.Mixed],
			vouchers: [Schema.Types.Mixed],
			invoiceNumber: Number,
			defaultPaymentMethodId: { type: Types.ObjectId, ref: 'PaymentMethod' },
			companyName: String,
			street: String,
			postcode: String,
			city: String,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, environmentId: 1, slug: 1 }, { unique: true })
);

import { PaymentMethod } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const PaymentMethodDatabase = model<Document & PaymentMethod>(
	'PaymentMethod',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			type: String,
			name: String,
			companyName: String,
			street: String,
			details: Schema.Types.Mixed,
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);

import { Role } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const RoleDatabase = model<Document & Role>(
	'Role',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			name: String,
			permissions: [String],
			default: Boolean,
		},
		{ minimize: false }
	)
);

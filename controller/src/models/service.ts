import { Service } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const ServiceDatabase = model<Document & Service>(
	'Service',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			name: String,
			slug: String,
			tokens: [String],
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, environmentId: 1, slug: 1 }, { unique: true })
);

import { Check } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const CheckDatabase = model<Document & Check>(
	'Check',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			serviceId: { type: Types.ObjectId, ref: 'Service' },
			type: String,
			configuration: Schema.Types.Mixed,
			status: String,
			interval: Number,
			active: Boolean,
			lastActivity: Date,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, environmentId: 1, serviceId: 1 })
);

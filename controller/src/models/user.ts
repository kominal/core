import { User } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const UserDatabase = model<Document & User>(
	'User',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			email: String,
			passwordHash: String,
			verified: Boolean,
			token: String,
			roles: [{ type: Types.ObjectId, ref: 'Role' }],
		},
		{ minimize: false }
	).index({ tenantId: 1, email: 1 }, { unique: true })
);

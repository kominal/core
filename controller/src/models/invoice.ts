import { Invoice } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const InvoiceDatabase = model<Document & Invoice>(
	'Invoice',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			number: String,
			time: Date,
			status: String,
			lines: [Schema.Types.Mixed],
		},
		{ minimize: false }
	).index({ tenantId: 1 })
);

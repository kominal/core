import { License } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const LicenseDatabase = model<Document & License>(
	'License',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			name: String,
			price: Number,
			tax: Number,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, slug: 1 }, { unique: true })
);

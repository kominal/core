import { Session } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const SessionDatabase = model<Document & Session>(
	'Session',
	new Schema(
		{
			userId: { type: Types.ObjectId, ref: 'User' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			keyHash: String,
			device: String,
			created: Date,
			lastActive: Date,
		},
		{ minimize: false }
	).index({ tenantId: 1, projectId: 1, keyHash: 1 }, { unique: true })
);

import { Analytics } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const AnalyticsDatabase = model<Document & Analytics>(
	'Analytics',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			projectId: { type: Types.ObjectId, ref: 'Project' },
			environmentId: { type: Types.ObjectId, ref: 'Environment' },
			serviceId: { type: Types.ObjectId, ref: 'Service' },
			checkId: { type: Types.ObjectId, ref: 'Check' },
			taskId: String,
			time: Date,
			type: String,
			level: String,
			content: Schema.Types.Mixed,
		},
		{ minimize: false }
	)
		.index({ time: 1 }, { expireAfterSeconds: 1209600 })
		.index({ time: 1, tenantId: 1, projectId: 1, environmentId: 1, serviceId: 1, level: 1, taskId: 1 })
);

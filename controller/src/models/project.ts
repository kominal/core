import { Project } from '@kominal/core-common';
import { Document, model, Schema, Types } from '@kominal/lib-node-mongodb-interface';

export const ProjectDatabase = model<Document & Project>(
	'Project',
	new Schema(
		{
			tenantId: { type: Types.ObjectId, ref: 'Tenant' },
			name: String,
			slug: String,
		},
		{ minimize: false }
	).index({ tenantId: 1, slug: 1 }, { unique: true })
);

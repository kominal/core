import { Check } from '@kominal/core-common';
import { Level } from '@kominal/core-node-client';
import { Scheduler } from '@kominal/lib-node-scheduler';
import axios from 'axios';
import { AnalyticsDatabase } from '../models/analytics';
import { CheckDatabase } from '../models/check';

export class CheckScheduler extends Scheduler {
	private index = 0;

	public async run(): Promise<void> {
		const checks = await CheckDatabase.find({
			active: true,
		});

		const checksToRun = checks.filter((check) => this.index % check.interval === 0);
		this.index += 10;

		for (const check of checksToRun) {
			if (check.type === 'PING') {
				await this.runPingCheck(check);
			}
		}
	}

	private async runPingCheck(check: Check): Promise<void> {
		const { url, timeout } = check.configuration;

		if (!url || !timeout) {
			return;
		}

		let level = Level.ERROR;
		const content: any = {};
		const start = Date.now();
		try {
			const { status } = await axios.get(url);
			content.duration = Date.now() - start;
			content.status = status;
			if (status === 200) {
				level = Level.INFO;
			}
		} catch (e) {
			content.error = e;
		}

		await AnalyticsDatabase.create({
			tenantId: check.tenantId,
			projectId: check.projectId,
			environmentId: check.environmentId,
			serviceId: check.serviceId,
			checkId: check._id,
			type: check.type,
			time: new Date(),
			level,
			content,
		});
	}
}

import { Scheduler } from '@kominal/lib-node-scheduler';
import Stripe from 'stripe';
import { InvoiceDatabase } from '../models/invoice';
import { PaymentDatabase } from '../models/payment';
import { PaymentMethodDatabase } from '../models/payment-method';
import { TenantDatabase } from '../models/tenant';

export class PaymentScheduler extends Scheduler {
	public async run(): Promise<void> {
		const invoices = await InvoiceDatabase.find({ status: 'UNPAID' });

		for (const invoice of invoices) {
			const tenant = await TenantDatabase.findById(invoice.tenantId).orFail(new Error('transl.error.notFound.tenant'));
			const coreTenant = await TenantDatabase.findById(tenant.tenantId).orFail(new Error('transl.error.notFound.tenant'));

			const { defaultPaymentMethodId } = tenant;

			if (!defaultPaymentMethodId) {
				// eslint-disable-next-line no-continue
				continue;
			}

			const paymentMethod = await PaymentMethodDatabase.findById(defaultPaymentMethodId).orFail(
				new Error('transl.error.notFound.paymentMethod')
			);

			if (!paymentMethod) {
				// eslint-disable-next-line no-continue
				continue;
			}

			let amount = 0;

			for (const line of invoice.lines) {
				amount += line.amount;
			}

			if (paymentMethod.type === 'STRIPE') {
				const { paymentMethodId } = paymentMethod.details;
				if (!paymentMethodId || !tenant.stripeCustomerId || !coreTenant.stripePublishableKey || !coreTenant.stripeSecretKey) {
					// eslint-disable-next-line no-continue
					continue;
				}

				const stripe = new Stripe(coreTenant.stripeSecretKey, {
					apiVersion: '2020-08-27',
				});

				try {
					const paymentIntent = await stripe.paymentIntents.create({
						amount: 1099,
						currency: 'eur',
						customer: tenant.stripeCustomerId,
						payment_method: paymentMethodId,
						off_session: true,
						confirm: true,
					});
					await PaymentDatabase.create({
						tenantId: tenant._id,
						invoiceId: invoice._id,
						time: new Date(),
						status: 'SUCCESSFUL',
						details: { paymentIntentId: paymentIntent.id },
					});
					await invoice.updateOne({ status: 'PAID' });
				} catch (err) {
					await PaymentDatabase.create({
						tenantId: tenant._id,
						invoiceId: invoice._id,
						time: new Date(),
						status: 'FAILED',
						details: { error: err.code },
					});
				}
			}
		}
	}
}

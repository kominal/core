import { sendMail } from '@kominal/lib-node-mailer';
import { Types } from '@kominal/lib-node-mongodb-interface';
import { Scheduler } from '@kominal/lib-node-scheduler';
import { AnalyticsDatabase } from '../models/analytics';
import { EnvironmentDatabase } from '../models/environment';
import { ProjectDatabase } from '../models/project';
import { ReportDatabase } from '../models/report';
import { ServiceDatabase } from '../models/service';

export class ReportScheduler extends Scheduler {
	public async run(): Promise<void> {
		const date = new Date();
		date.setDate(date.getDate() - 7);
		const reports = await ReportDatabase.find({ $or: [{ lastUpdated: { $lt: date } }, { lastUpdated: undefined }] });

		for (const report of reports) {
			let text = '';

			const environment = await EnvironmentDatabase.findById(report.environmentId);
			const project = await ProjectDatabase.findById(report.projectId);

			for (const event of report.events) {
				const analytics: { _id: { type: string }; count: number }[] = await AnalyticsDatabase.aggregate([
					{ $match: { serviceId: Types.ObjectId(event.serviceId), type: { $in: event.types } } },
					{
						$group: {
							_id: { type: '$type' },
							count: { $sum: 1 },
						},
					},
				]);

				const service = await ServiceDatabase.findById(event.serviceId);
				text += `<h3>${project?.name} ${environment?.name} ${service?.name}</h3>`;
				for (const a of analytics) {
					text += `${a._id.type}: ${a.count}<br />`;
				}
			}

			await sendMail({ subject: 'Event Report', to: report.emails, text: `<!DOCTYPE html><html><body>${text}</body></html>` });
			report.lastUpdated = new Date();
			await ReportDatabase.findOneAndUpdate({ _id: report._id }, report);
		}
	}
}

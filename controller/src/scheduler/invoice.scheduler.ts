import { LicenseInvoiceLine } from '@kominal/core-common';
import { Scheduler } from '@kominal/lib-node-scheduler';
import { InvoiceDatabase } from '../models/invoice';
import { LicenseDatabase } from '../models/license';
import { TenantDatabase } from '../models/tenant';

export class InvoiceScheduler extends Scheduler {
	public async run(): Promise<void> {
		const tenants = await TenantDatabase.find();
		const licenses = await LicenseDatabase.find();

		const invoiceFrom = new Date();
		invoiceFrom.setDate(invoiceFrom.getDate() + 3);

		for (const tenant of tenants) {
			const lines: LicenseInvoiceLine[] = [];

			const tenantLicenses = tenant.licenses;

			for (const tenantLicense of tenantLicenses) {
				const invoicedUntil = tenantLicense.invoicedUntil || tenantLicense.added;

				if (invoicedUntil.getTime() < invoiceFrom.getTime()) {
					const tl = tenantLicense;
					const license = licenses.find((l) => String(l._id) === String(tl.licenseId));

					const invoiceUntil = new Date(invoicedUntil);
					invoiceUntil.setMonth(invoiceUntil.getMonth() + 1);

					if (license) {
						lines.push({
							description: license.name,
							amount: 1,
							price: license.price,
							tax: license.price,
							licenseId: license._id,
							from: invoicedUntil,
							until: invoiceUntil,
						});
					}
				}
			}

			if (lines.length > 0) {
				const parentTenant = tenants.find((t) => {
					console.log(t, String(t._id), tenant.tenantId, String(t._id) === String(tenant.tenantId));
					return String(t._id) === String(tenant.tenantId);
				});
				console.log(parentTenant, tenant.tenantId);

				if (parentTenant) {
					const invoiceNumber = (parentTenant.invoiceNumber || 0) + 1;
					await InvoiceDatabase.create({
						tenantId: tenant._id,
						number: String(invoiceNumber),
						time: new Date(),
						status: 'OPEN',
						lines,
					});
					await parentTenant.updateOne({ invoiceNumber });
					await tenant.updateOne({ licenses: tenantLicenses });
				}
			}
		}
	}
}

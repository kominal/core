import { info } from '@kominal/core-node-client';
import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { exit } from 'process';
import { startMQTTIngress } from './ingress';
import { analyticsRouter } from './routes/analytics';
import { checksRouter } from './routes/checks';
import { coreTenantsRouter } from './routes/core-tenants';
import { coreUsersRouter } from './routes/core-users';
import { emqxRouter } from './routes/emqx';
import { environmentTenantsRouter } from './routes/environment-tenants';
import { environmentsRouter } from './routes/environments';
import { invoicesRouter } from './routes/invoices';
import { paymentMethodsRouter } from './routes/payment-methods';
import { paymentsRouter } from './routes/payments';
import { projectFilesRouter } from './routes/project-files';
import { projectInvoicesRouter } from './routes/project-invoices';
import { projectLicensesRouter } from './routes/project-licenses';
import { projectPaymentMethodsRouter } from './routes/project-payment-methods';
import { projectPaymentsRouter } from './routes/project-payments';
import { projectTranslationsRouter } from './routes/project-translations';
import { projectVouchersRouter } from './routes/project-vouchers';
import { projectsRouter } from './routes/projects';
import { reportsRouter } from './routes/reports';
import { rolesRouter } from './routes/roles';
import { servicesRouter } from './routes/services';
import { sessionsRouter } from './routes/sessions';
import { tenantLicensesRouter } from './routes/tenant-licenses';
import { tenantsRouter } from './routes/tenants';
import { translationsRouter } from './routes/translations';
import { usersRouter } from './routes/users';
import { CheckScheduler } from './scheduler/check.scheduler';
import { InvoiceScheduler } from './scheduler/invoice.scheduler';
import { PaymentScheduler } from './scheduler/payment.scheduler';
import { ReportScheduler } from './scheduler/report.scheduler';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start(): Promise<void> {
	// startAnalytics();
	mongoDBInterface = new MongoDBInterface('controller');
	await mongoDBInterface.connect();
	expressRouter = new ExpressRouter({
		healthCheck: async (): Promise<boolean> => true,
		routes: [
			analyticsRouter,
			checksRouter,
			coreTenantsRouter,
			coreUsersRouter,
			emqxRouter,
			environmentsRouter,
			invoicesRouter,
			paymentMethodsRouter,
			paymentsRouter,
			projectFilesRouter,
			projectInvoicesRouter,
			projectLicensesRouter,
			projectPaymentMethodsRouter,
			projectPaymentsRouter,
			environmentTenantsRouter,
			projectTranslationsRouter,
			projectVouchersRouter,
			projectsRouter,
			rolesRouter,
			servicesRouter,
			sessionsRouter,
			tenantLicensesRouter,
			tenantsRouter,
			translationsRouter,
			usersRouter,
			reportsRouter,
		],
	});

	await expressRouter.start();
	await startMQTTIngress();
	await new CheckScheduler().start(10);
	await new InvoiceScheduler().start(3600);
	await new PaymentScheduler().start(3600);
	await new ReportScheduler().start(3600);
}
start();

process.on('SIGTERM', async () => {
	info("Received system signal 'SIGTERM'. Shutting down service...");
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
	exit(0);
});

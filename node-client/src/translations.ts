import axios from 'axios';
import { CORE_BASE_URL, CORE_PROJECT_ID, CORE_TENANT_ID } from './helper/environment';

export async function getTranslation(language: string, key: string, variables?: { [name: string]: any }): Promise<string> {
	if (!CORE_BASE_URL || !CORE_TENANT_ID || !CORE_PROJECT_ID) {
		return key;
	}

	const { data } = await axios.get<{ translation: string }>(
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/translations/languages/${language}/keys/${key}`
	);

	let { translation } = data;

	if (variables) {
		for (const [name, value] of Object.entries(variables)) {
			translation = translation.replace(new RegExp(name, 'g'), value);
		}
	}

	return translation;
}

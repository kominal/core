import { mqttClient } from '.';
import { CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_SERVICE_SLUG, CORE_TENANT_ID, TASK_ID } from './helper/environment';

// eslint-disable-next-line no-shadow
export enum Level {
	DEBUG = 'DEBUG',
	INFO = 'INFO',
	WARN = 'WARN',
	ERROR = 'ERROR',
}

function now(): string {
	return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
}

export function printConsoleMessage(level: Level, consoleMessage: string): void {
	let color = '';
	if (level === Level.DEBUG) {
		color = '\x1b[36m';
	} else if (level === Level.INFO) {
		color = '\x1b[32m';
	} else if (level === Level.WARN) {
		color = '\x1b[33m';
	} else if (level === Level.ERROR) {
		color = '\x1b[31m';
	}

	console.log(`${now()} | ${color}${level}\x1b[0m > ${consoleMessage}`);
}

export async function sendAnalytics(
	type: string,
	level: Level,
	content: any,
	options?: {
		tenantId?: string;
		projectId?: string;
		environmentId?: string;
		serviceSlug?: string;
		taskId?: string;
	}
): Promise<void> {
	if (!mqttClient) {
		return;
	}

	try {
		await mqttClient.publish(
			`${options?.tenantId || CORE_TENANT_ID}/${options?.projectId || CORE_PROJECT_ID}/${
				options?.environmentId || CORE_ENVIRONMENT_ID
			}/${options?.serviceSlug || CORE_SERVICE_SLUG}/${options?.taskId || TASK_ID}/analytics`,
			JSON.stringify({
				time: new Date(),
				type,
				level,
				content,
			}),
			{ qos: 0 }
		);
	} catch (e) {
		printConsoleMessage(Level.ERROR, `Could not forward event to Observer: ${e}`);
	}
}

export async function log(level: Level, message: any): Promise<void> {
	printConsoleMessage(level, message instanceof Error ? message.message : message);
	sendAnalytics('LOG', level, message);
}

export async function debug(message: any): Promise<void> {
	return log(Level.DEBUG, message);
}

export async function info(message: any): Promise<void> {
	return log(Level.INFO, message);
}

export async function warn(message: any): Promise<void> {
	return log(Level.WARN, message);
}

export async function error(message: any): Promise<void> {
	return log(Level.ERROR, message);
}

export async function handleError(e: any): Promise<void> {
	if (typeof e === 'string') {
		return error(e);
	}
	if (e.error?.error) {
		return error(e.error.error);
	}
	if (e.message) {
		console.trace(e);
		return error(e.message);
	}
	console.trace(e);
	return error('transl.error.unknown');
}

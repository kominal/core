import axios from 'axios';
import { CORE_BASE_URL } from './helper/environment';

export async function checkText(text: string, language: string): Promise<string> {
	return axios.post(`${CORE_BASE_URL}/language-tool/v2/check`, {
		text,
		language,
	});
}

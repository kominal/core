import axios, { AxiosResponse } from 'axios';
import { CORE_BASE_URL, CORE_ENVIRONMENT_ID, CORE_ENVIRONMENT_TOKEN, CORE_PROJECT_ID, CORE_TENANT_ID } from './helper/environment';

export async function createTenant(name: string, slug: string, userId: string, permissions: string[]): Promise<string> {
	return axios.post(
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/environments/${CORE_ENVIRONMENT_ID}/tenants`,
		{
			name,
			slug,
			permissions,
		},
		{
			headers: {
				Authorization: CORE_ENVIRONMENT_TOKEN,
				userId,
			},
		}
	);
}

export async function addLicense(tenantId: string, licenseId: string, userId: string): Promise<string> {
	return axios.put(
		// eslint-disable-next-line max-len
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/environments/${CORE_ENVIRONMENT_ID}/tenants/${tenantId}/licenses`,
		{
			licenseId,
		},
		{
			headers: {
				Authorization: CORE_ENVIRONMENT_TOKEN,
				userId,
			},
		}
	);
}

export async function removeLicense(tenantId: string, licenseId: string, userId: string): Promise<void> {
	return axios.delete(
		// eslint-disable-next-line max-len
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/environments/${CORE_ENVIRONMENT_ID}/tenants/${tenantId}/licenses/${licenseId}`,
		{
			headers: {
				Authorization: CORE_ENVIRONMENT_TOKEN,
				userId,
			},
		}
	);
}

export async function getLicenses(
	tenantId: string,
	userId: string
): Promise<
	AxiosResponse<
		{
			licenseId: string;
			added: Date;
			removed?: Date;
		}[]
	>
> {
	return axios.get(
		// eslint-disable-next-line max-len
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/environments/${CORE_ENVIRONMENT_ID}/tenants/${tenantId}/licenses`,
		{
			headers: {
				Authorization: CORE_ENVIRONMENT_TOKEN,
				userId,
			},
		}
	);
}

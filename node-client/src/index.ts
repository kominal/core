import { AsyncClient, connect } from 'async-mqtt';
import {
	CORE_ENVIRONMENT_ID,
	CORE_ENVIRONMENT_TOKEN,
	CORE_MQTT_BROKER,
	CORE_PROJECT_ID,
	CORE_SERVICE_SLUG,
	CORE_SERVICE_TOKEN,
	CORE_TENANT_ID,
	TASK_ID,
} from './helper/environment';

export * from './analytics';
export * from './language-tool';
export * from './tenants';
export * from './translations';

// eslint-disable-next-line import/no-mutable-exports
export let mqttClient: AsyncClient;

export function startAnalytics(): void {
	if (mqttClient || !CORE_MQTT_BROKER || !CORE_TENANT_ID || !CORE_PROJECT_ID || !CORE_ENVIRONMENT_ID || !CORE_SERVICE_SLUG || !TASK_ID) {
		return;
	}

	const username = `${CORE_TENANT_ID}/${CORE_PROJECT_ID}/${CORE_ENVIRONMENT_ID}/${CORE_SERVICE_SLUG}/${TASK_ID}`;

	let password;
	if (CORE_SERVICE_TOKEN) {
		password = `ServiceToken ${CORE_SERVICE_TOKEN}`;
	} else if (CORE_ENVIRONMENT_TOKEN) {
		password = `ServiceToken ${CORE_ENVIRONMENT_TOKEN}`;
	}

	mqttClient = connect(CORE_MQTT_BROKER, {
		username,
		password,
	});
}

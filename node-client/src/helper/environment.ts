import { getEnvironmentString } from '@kominal/lib-node-environment';

export const {
	CORE_TENANT_ID,
	CORE_PROJECT_ID,
	CORE_ENVIRONMENT_ID,
	CORE_SERVICE_SLUG,
	CORE_ENVIRONMENT_TOKEN,
	CORE_SERVICE_TOKEN,
	TASK_ID,
} = process.env;

export const CORE_MQTT_BROKER = getEnvironmentString('CORE_MQTT_BROKER', 'mqtts://mqtt.core.kominal.app:8884');
export const CORE_BASE_URL = getEnvironmentString('CORE_BASE_URL', 'https://core.kominal.app');

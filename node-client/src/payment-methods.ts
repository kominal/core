import axios from 'axios';
import { CORE_BASE_URL, CORE_ENVIRONMENT_ID, CORE_ENVIRONMENT_TOKEN, CORE_PROJECT_ID, CORE_TENANT_ID } from './helper/environment';

export async function createTenant(name: string, slug: string, userId: string, permissions: string[]): Promise<string> {
	return axios.put(
		`${CORE_BASE_URL}/controller/tenants/${CORE_TENANT_ID}/projects/${CORE_PROJECT_ID}/environments/${CORE_ENVIRONMENT_ID}/tenants`,
		{
			name,
			slug,
			permissions,
		},
		{
			headers: {
				Authorization: CORE_ENVIRONMENT_TOKEN,
				userId,
			},
		}
	);
}

import { Level, sendAnalytics } from '@kominal/core-node-client';
import { Scheduler } from '@kominal/lib-node-scheduler';
import Docker from 'dockerode';
import { CORE_ENVIRONMENT_ID, CORE_PROJECT_ID, CORE_TENANT_ID } from '../helper/environment';

export class ContainersScheduler extends Scheduler {
	private docker = new Docker();

	public async run(): Promise<void> {
		const info = await this.docker.info();

		let globalCpu = 0;
		let globalMemory = 0;

		const containers = await this.docker.listContainers();
		for (const container of containers) {
			const containerObject = this.docker.getContainer(container.Id);
			const containerInspect = await containerObject.inspect();
			const stats = await containerObject.stats({ stream: false });

			const cpuDelta = stats.cpu_stats.cpu_usage.total_usage - stats.precpu_stats.cpu_usage.total_usage;
			const systemCpuDelta = stats.cpu_stats.system_cpu_usage - stats.precpu_stats.system_cpu_usage;
			const numberOfCpus = stats.cpu_stats.cpu_usage.percpu_usage.length;

			const cpu = Math.round((cpuDelta / systemCpuDelta) * numberOfCpus * 100 * 100) / 100;

			const usedMemory = stats.memory_stats.usage - stats.memory_stats.stats.cache;
			const availableMemory = stats.memory_stats.limit;

			const memory = Math.round((usedMemory / availableMemory) * 100 * 100) / 100;

			globalCpu += cpu;
			globalMemory += memory;

			const env = containerInspect.Config.Env;
			// eslint-disable-next-line
			const res = env.map((e) => e.split('=')).reduce((acc, [key, value]) => ((acc[key] = value), acc), {} as any);

			const tenantId = res.CORE_TENANT_ID;
			const projectId = res.CORE_PROJECT_ID;
			const environmentId = res.CORE_ENVIRONMENT_ID;
			const serviceSlug = res.CORE_SERVICE_SLUG;
			const taskId = res.TASK_ID;

			if (tenantId && projectId && environmentId && serviceSlug && taskId) {
				sendAnalytics(
					'USAGE',
					Level.INFO,
					{
						cpu,
						memory,
					},
					{
						tenantId,
						projectId,
						environmentId,
						serviceSlug,
						taskId,
					}
				);
			}
		}

		sendAnalytics(
			'USAGE',
			Level.INFO,
			{
				cpu: globalCpu / containers.length,
				memory: globalMemory / containers.length,
			},
			{
				tenantId: CORE_TENANT_ID,
				projectId: CORE_PROJECT_ID,
				environmentId: CORE_ENVIRONMENT_ID,
				serviceSlug: info.Name,
				taskId: '',
			}
		);
	}
}

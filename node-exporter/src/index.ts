import { info, startAnalytics } from '@kominal/core-node-client';
import { exit } from 'process';
import { ContainersScheduler } from './scheduler/containers.scheduler';

async function start(): Promise<void> {
	startAnalytics();
	new ContainersScheduler().start(60);
}
start();

process.on('SIGTERM', async () => {
	info("Received system signal 'SIGTERM'. Shutting down service...");
	exit(0);
});

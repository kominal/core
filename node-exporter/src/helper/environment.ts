import { getEnvironmentString } from '@kominal/lib-node-environment';

export const CORE_MQTT_BROKER = getEnvironmentString('CORE_MQTT_BROKER', 'mqtts://mqtt.core.kominal.app:8884');
export const { CORE_TENANT_ID, CORE_PROJECT_ID, CORE_ENVIRONMENT_ID } = process.env;
export const CORE_SERVICE_TOKEN = getEnvironmentString('CORE_SERVICE_TOKEN', 'local');
export const SALT = getEnvironmentString('SALT', '$2a$10$Q6KTUibMgtVD4SzQbirDt.');

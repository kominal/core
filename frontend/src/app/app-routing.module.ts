import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@kominal/core-angular-client';
import { WelcomeGuard } from './core/guards/welcome/welcome.guard';
import { AuthenticationComponent } from './modules/authentication/authentication.component';
import { CockpitComponent } from './modules/cockpit/cockpit.component';
import { CoreComponent } from './modules/core/core.component';
import { EnvironmentComponent } from './modules/environment/environment.component';
import { ProjectComponent } from './modules/project/project.component';
import { ServiceComponent } from './modules/service/service.component';
import { TenantComponent } from './modules/tenant/tenant.component';
import { WelcomeComponent } from './modules/welcome/welcome.component';

const routes: Routes = [
	{
		path: 'authentication',
		component: AuthenticationComponent,
		loadChildren: (): Promise<any> => import('./modules/authentication/authentication.module').then((m) => m.AuthenticationModule),
	},
	{
		path: 'core',
		component: CoreComponent,
		loadChildren: (): Promise<any> => import('./modules/core/core.module').then((m) => m.CoreModule),
	},
	{
		path: 'cockpit/:tenantSlug/projects/:projectSlug/environments/:environmentSlug/tenants/:projectTenantSlug',
		component: TenantComponent,
		loadChildren: (): Promise<any> => import('./modules/tenant/tenant.module').then((m) => m.TenantModule),
	},
	{
		path: 'cockpit/:tenantSlug/projects/:projectSlug/environments/:environmentSlug/services/:serviceSlug',
		component: ServiceComponent,
		loadChildren: (): Promise<any> => import('./modules/service/service.module').then((m) => m.ServiceModule),
	},
	{
		path: 'cockpit/:tenantSlug/projects/:projectSlug/environments/:environmentSlug',
		component: EnvironmentComponent,
		loadChildren: (): Promise<any> => import('./modules/environment/environment.module').then((m) => m.EnvironmentModule),
		canActivate: [UserGuard],
	},
	{
		path: 'cockpit/:tenantSlug/projects/:projectSlug',
		component: ProjectComponent,
		loadChildren: (): Promise<any> => import('./modules/project/project.module').then((m) => m.ProjectModule),
		canActivate: [UserGuard],
	},
	{
		path: 'cockpit/:tenantSlug',
		component: CockpitComponent,
		loadChildren: (): Promise<any> => import('./modules/cockpit/cockpit.module').then((m) => m.CockpitModule),
		canActivate: [UserGuard],
	},
	{
		path: 'welcome',
		component: WelcomeComponent,
		canActivate: [WelcomeGuard],
	},
	{
		path: '**',
		redirectTo: 'authentication',
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularClientModule, Level, registerTranslationModule } from '@kominal/core-angular-client';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		AngularClientModule.forRoot({
			coreTenantId: '60a2a0d1b65dfe63b80dd249',
			coreProjectId: '60a2a174580c8fe02ce076ec',
			coreEnvironmentId: '60aa72398f2664041b8e19f6',
			languages: ['de', 'en'],
			defaultLanguage: 'de',
			serviceName: 'frontend',
			userHomeUrl: '/cockpit',
			guestHomeUrl: '/authentication/login',
			snackBarLogLevel: [Level.ERROR],
		}),
		registerTranslationModule(),
		BrowserAnimationsModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}

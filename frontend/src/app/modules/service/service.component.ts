import { Component } from '@angular/core';
import { Tab } from 'src/app/modules/shared/components/tab-bar/tab-bar.component';

@Component({
	selector: 'app-service',
	templateUrl: './service.component.html',
	styleUrls: ['./service.component.scss'],
})
export class ServiceComponent {
	public tabs: Tab[] = [
		{ name: 'analytics', icon: 'analytics', link: 'analytics' },
		{ name: 'checks', icon: 'checks', link: 'checks' },
	];
}

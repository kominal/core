import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ChecksComponent } from './pages/checks/checks.component';

const routes: Routes = [
	{
		path: 'analytics',
		component: AnalyticsComponent,
		loadChildren: (): Promise<any> => import('./analytics/analytics.module').then((m) => m.AnalyticsModule),
	},
	{
		path: 'checks',
		component: ChecksComponent,
	},
	{
		path: '**',
		redirectTo: 'analytics',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ServiceRoutingModule {}

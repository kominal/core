import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogComponent } from './pages/log/log.component';
import { StatusComponent } from './pages/status/status.component';

const routes: Routes = [
	{
		path: 'log',
		component: LogComponent,
	},
	{
		path: 'status',
		component: StatusComponent,
	},
	{
		path: '**',
		redirectTo: 'log',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AnalyticsRoutingModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularClientModule, MaterialModule, TranslateModule } from '@kominal/core-angular-client';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsComponent } from './analytics.component';
import { LogComponent } from './pages/log/log.component';
import { StatusComponent } from './pages/status/status.component';
import { LogEntryComponent } from './components/log-entry/log-entry.component';

@NgModule({
	declarations: [AnalyticsComponent, LogComponent, StatusComponent, LogEntryComponent],
	imports: [CommonModule, AnalyticsRoutingModule, SharedModule, MaterialModule, AngularClientModule, TranslateModule],
})
export class AnalyticsModule {}

import { Component } from '@angular/core';
import { Tab } from 'src/app/modules/shared/components/tab-bar/tab-bar.component';

@Component({
	selector: 'app-analytics',
	templateUrl: './analytics.component.html',
	styleUrls: ['./analytics.component.scss'],
})
export class AnalyticsComponent {
	public tabs: Tab[] = [
		{ name: 'log', icon: 'log', link: 'log' },
		{ name: 'status', icon: 'status', link: 'status' },
	];
}

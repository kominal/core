import { Component } from '@angular/core';
import { Analytics } from '@kominal/core-common';
import { AnalyticsService } from 'src/app/core/services/analytics/analytics.service';

@Component({
	selector: 'app-log',
	templateUrl: './log.component.html',
	styleUrls: ['./log.component.scss'],
})
export class LogComponent {
	public analytics: Analytics[] = [];

	public constructor(public analyticsService: AnalyticsService) {
		analyticsService.list().subscribe((v) => {
			this.analytics = v.items;
		});
	}
}

import { Component, Input } from '@angular/core';
import { Analytics } from '@kominal/core-common';

@Component({
	selector: 'app-log-entry',
	templateUrl: './log-entry.component.html',
	styleUrls: ['./log-entry.component.scss'],
})
export class LogEntryComponent {
	public JSON = JSON;

	@Input()
	public analytics: Analytics | null | undefined;
}

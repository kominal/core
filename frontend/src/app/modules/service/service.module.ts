import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	AngularClientModule,
	InputModule,
	MaterialModule,
	SingleSelectModule,
	TableModule,
	TranslateModule,
} from '@kominal/core-angular-client';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { CheckDialogComponent } from './components/check-dialog/check-dialog.component';
import { ChecksComponent } from './pages/checks/checks.component';
import { ServiceRoutingModule } from './service-routing.module';
import { ServiceComponent } from './service.component';

@NgModule({
	declarations: [ServiceComponent, ChecksComponent, CheckDialogComponent],
	imports: [
		CommonModule,
		ServiceRoutingModule,
		AngularClientModule,
		TranslateModule,
		MaterialModule,
		SharedModule,
		TableModule,
		InputModule,
		FormsModule,
		SingleSelectModule,
		ReactiveFormsModule,
	],
})
export class ServiceModule {}

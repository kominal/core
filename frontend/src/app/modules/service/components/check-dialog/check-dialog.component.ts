import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Check } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CheckService } from 'src/app/core/services/check/check.service';

@UntilDestroy()
@Component({
	selector: 'app-check-dialog',
	templateUrl: './check-dialog.component.html',
	styleUrls: ['./check-dialog.component.scss'],
})
export class CheckDialogComponent {
	public loading = false;

	public checkTypes = ['PING'];

	public configuration: any = new FormGroup({});

	public form = new FormGroup<Pick<Check, '_id' | 'active' | 'interval' | 'type' | 'configuration'>>({
		_id: new FormControl(),
		active: new FormControl(true),
		interval: new FormControl(60),
		type: new FormControl<string>(undefined, Validators.required),
		configuration: new FormGroup({}),
	});

	public constructor(
		private dialogRef: MatDialogRef<CheckDialogComponent>,
		private checkService: CheckService,
		@Inject(MAT_DIALOG_DATA) check: Check
	) {
		this.form.controls.type.valueChanges.subscribe(async (value) => {
			if (value === 'PING') {
				const configuration = new FormGroup({
					url: new FormControl<string>(),
					timeout: new FormControl<number>(),
				});
				this.configuration = configuration;
				this.form.controls.configuration = configuration;
			}
			this.form.patchValue({ configuration: check.configuration });
		});
		if (check) {
			this.form.patchValue(check);
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.checkService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Check, Translation } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { CheckService } from 'src/app/core/services/check/check.service';
import { CheckDialogComponent } from '../../components/check-dialog/check-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-checks',
	templateUrl: './checks.component.html',
	styleUrls: ['./checks.component.scss'],
})
export class ChecksComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Translation>;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['key'];

	public displayedColumns = ['status', 'type', 'lastActivity', 'actions'];

	public constructor(public checkService: CheckService, public userService: UserService, public matDialog: MatDialog) {}

	public list(paginationRequest: PaginationRequest): void {
		this.checkService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openCheckDialog(check?: Check): void {
		this.matDialog
			.open<CheckDialogComponent, Check, boolean>(CheckDialogComponent, { width: '900px', data: check })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteCheck(check: Check): void {
		this.checkService
			.delete(check)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}
}

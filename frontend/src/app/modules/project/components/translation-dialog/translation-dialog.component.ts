import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageToolService } from '@kominal/core-angular-client';
import { Translation } from '@kominal/core-common';
import { FormArray, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { debounceTime, first, map, shareReplay, startWith, switchMap } from 'rxjs/operators';
import { ProjectTranslationService } from 'src/app/core/services/project-translation/project-translation.service';
import { ProjectService } from 'src/app/core/services/project/project.service';

@UntilDestroy()
@Component({
	selector: 'app-translation-dialog',
	templateUrl: './translation-dialog.component.html',
	styleUrls: ['./translation-dialog.component.scss'],
})
export class TranslationDialogComponent {
	public loading = false;

	public translationTypes = ['SINGLE', 'TEXT', 'MULTI'];

	public values = new FormArray(
		['en', 'de'].map(
			(language) =>
				new FormGroup<{ language: string; value: string }>({
					language: new FormControl(language, Validators.required),
					value: new FormControl(),
				})
		)
	);

	public spellingValidators = new Map<string, Observable<string[]>>();

	public form = new FormGroup<Omit<Translation, 'tenantId' | 'projectId'>>({
		_id: new FormControl(),
		type: new FormControl('SINGLE', Validators.required),
		key: new FormControl<string>(undefined, Validators.required),
		values: this.values,
	});

	public constructor(
		private dialogRef: MatDialogRef<TranslationDialogComponent>,
		private projectService: ProjectService,
		private projectTranslationService: ProjectTranslationService,
		private languageToolService: LanguageToolService,
		@Inject(MAT_DIALOG_DATA) translation: Translation
	) {
		this.values.controls.forEach((control) => {
			const language = control.controls.language.value;
			this.spellingValidators.set(
				language,
				control.controls.value.valueChanges.pipe(
					startWith(control.controls.value.value),
					debounceTime(300),
					switchMap((value) => this.languageToolService.checkText(value, language === 'en' ? 'en-GB' : 'de-DE')),
					untilDestroyed(this),
					map((v: any) => {
						const response = [];
						if (v && v.matches) {
							for (const { message, offset, replacements } of v.matches) {
								response.push(`${offset}: ${message}${replacements?.length > 0 ? ` (${replacements[0].value})` : ''}\n`);
							}
						}
						return response;
					}),
					shareReplay(1)
				)
			);
		});
		if (translation) {
			for (const group of this.values.controls) {
				const value = translation.values.find((v) => v.language === group.controls.language.value);
				if (value) {
					group.controls.value.setValue(value.value);
				}
			}
			this.form.patchValue({ ...translation, values: undefined });
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.projectService.currentProject$
			.pipe(
				untilDestroyed(this),
				first((project) => !!project),
				switchMap(() =>
					this.projectTranslationService.createOrUpdate({
						...this.form.value,
						values: this.form.value.values.filter((v) => v.value?.length > 0),
					})
				)
			)
			.subscribe((s) => {
				if (s) {
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

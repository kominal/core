import { Component, ElementRef, HostListener, Inject, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { File as CoreFile } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FileService } from 'src/app/core/services/file/file.service';

@UntilDestroy()
@Component({
	selector: 'app-file-dialog',
	templateUrl: './file-dialog.component.html',
	styleUrls: ['./file-dialog.component.scss'],
})
export class FileDialogComponent {
	public loading = false;

	public Object = Object;

	public fileTypes = ['IMAGE'];

	@ViewChild('fileInput')
	public fileInput!: ElementRef<HTMLInputElement>;

	public dragging = false;

	public form = new FormGroup<Pick<CoreFile, '_id' | 'key' | 'type' | 'value'> & { value: string }>({
		_id: new FormControl(),
		key: new FormControl<string>(undefined, Validators.required),
		type: new FormControl<string>('IMAGE', Validators.required),
		value: new FormControl<string>(undefined, Validators.required),
	});

	public constructor(
		private dialogRef: MatDialogRef<FileDialogComponent>,
		private fileService: FileService,
		private domSanitizer: DomSanitizer,
		@Inject(MAT_DIALOG_DATA) file: CoreFile
	) {
		if (file) {
			this.form.patchValue(file);
		}
	}

	private toBase64 = (file: any): any =>
		new Promise((resolve, reject) => {
			const reader = new FileReader();
			reader.onload = (): any => resolve(reader.result);
			reader.onerror = (error: any): any => reject(error);
			reader.readAsDataURL(file);
		});

	public async onSubmit(): Promise<void> {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.fileService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}

	public async addSelectionFiles(event: any): Promise<void> {
		return this.addFile(event.target.files[0]);
	}

	public async addFile(file: File): Promise<void> {
		let preview;
		if (file.type.startsWith('image/')) {
			preview = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
		} else {
			preview = this.domSanitizer.bypassSecurityTrustUrl('./assets/logo.svg');
		}

		this.form.controls.value.setValue(await this.toBase64(file));
	}

	@HostListener('dragover', ['$event'])
	public onDragOver(e: Event): void {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = true;
	}

	@HostListener('dragleave', ['$event'])
	public onDragLeave(e: Event): void {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
	}

	@HostListener('drop', ['$event'])
	public async onDrop(e: any): Promise<void> {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
		this.form.controls.value.setValue(await this.toBase64(e.dataTransfer.files[0]));
	}
}

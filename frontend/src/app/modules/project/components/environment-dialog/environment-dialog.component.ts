import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Environment } from '@kominal/core-common';
import { FormArray, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ServiceService } from 'src/app/core/services/service/service.service';

@UntilDestroy()
@Component({
	selector: 'app-environment-dialog',
	templateUrl: './environment-dialog.component.html',
	styleUrls: ['./environment-dialog.component.scss'],
})
export class EnvironmentDialogComponent {
	public loading = false;

	public tokens = new FormArray<string>([]);

	public form = new FormGroup<Pick<Environment, '_id' | 'name' | 'slug' | 'tokens'>>({
		_id: new FormControl(),
		name: new FormControl<string>(undefined, Validators.required),
		slug: new FormControl<string>(undefined, Validators.required),
		tokens: this.tokens,
	});

	public constructor(
		private dialogRef: MatDialogRef<EnvironmentDialogComponent>,
		private environmentService: EnvironmentService,
		public serviceService: ServiceService,
		@Inject(MAT_DIALOG_DATA) public environment: Environment
	) {
		if (environment) {
			environment.tokens?.forEach(() => this.addTokenControl());
			this.form.patchValue(environment);
		}
	}

	public addTokenControl(): void {
		this.tokens.push(new FormControl('', Validators.required));
	}

	public removeTokenControl(index: number): void {
		this.tokens.removeAt(index);
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.environmentService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.environmentService.reload$.next();
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

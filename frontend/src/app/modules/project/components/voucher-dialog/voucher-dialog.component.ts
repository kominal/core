import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Environment, Voucher } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { untilDestroyed } from '@ngneat/until-destroy';
import { VoucherService } from 'src/app/core/services/voucher/voucher.service';

@Component({
	selector: 'app-voucher-dialog',
	templateUrl: './voucher-dialog.component.html',
	styleUrls: ['./voucher-dialog.component.scss'],
})
export class VoucherDialogComponent {
	public loading = false;

	public types = ['RECURRING', 'ONE_TIME'];

	public discountTypes = ['PERCENTAGE', 'FIXED'];

	public form = new FormGroup<Voucher>({
		_id: new FormControl(),
		code: new FormControl<string>(undefined, Validators.required),
		type: new FormControl<string>(undefined, Validators.required),
		discountType: new FormControl<string>(undefined, Validators.required),
		discountAmount: new FormControl<number>(0, Validators.required),
	});

	public constructor(
		private dialogRef: MatDialogRef<VoucherDialogComponent>,
		private voucherService: VoucherService,
		@Inject(MAT_DIALOG_DATA) environment: Environment
	) {
		if (environment) {
			this.form.patchValue(environment);
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.voucherService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

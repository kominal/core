import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Environment, License } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { LicenseService } from 'src/app/core/services/license/license.service';

@UntilDestroy()
@Component({
	selector: 'app-license-dialog',
	templateUrl: './license-dialog.component.html',
	styleUrls: ['./license-dialog.component.scss'],
})
export class LicenseDialogComponent {
	public loading = false;

	public form = new FormGroup<Pick<License, '_id' | 'name' | 'price' | 'tax'>>({
		_id: new FormControl(),
		name: new FormControl<string>(undefined, Validators.required),
		price: new FormControl<number>(0, Validators.required),
		tax: new FormControl<number>(0, Validators.required),
	});

	public constructor(
		private dialogRef: MatDialogRef<LicenseDialogComponent>,
		private licenseService: LicenseService,
		@Inject(MAT_DIALOG_DATA) environment: Environment
	) {
		if (environment) {
			this.form.patchValue(environment);
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.licenseService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

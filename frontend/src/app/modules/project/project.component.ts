import { Component } from '@angular/core';
import { Tab } from '../shared/components/tab-bar/tab-bar.component';

@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.scss'],
})
export class ProjectComponent {
	public tabs: Tab[] = [
		{ name: 'environments', icon: 'play_arrow', link: 'environments' },
		{ name: 'translations', icon: 'translate', link: 'translations' },
		{ name: 'licenses', icon: 'fact_check', link: 'licenses' },
		{ name: 'vouchers', icon: 'card_giftcard', link: 'vouchers' },
		{ name: 'files', icon: 'file', link: 'files' },
	];
}

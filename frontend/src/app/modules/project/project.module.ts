import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	AngularClientModule,
	InputModule,
	LoadingButtonModule,
	MaterialModule,
	SingleSelectModule,
	TableModule,
	TextAreaModule,
	TranslateModule,
} from '@kominal/core-angular-client';
import { SharedModule } from '../shared/shared.module';
import { EnvironmentDialogComponent } from './components/environment-dialog/environment-dialog.component';
import { LicenseDialogComponent } from './components/license-dialog/license-dialog.component';
import { TranslationDialogComponent } from './components/translation-dialog/translation-dialog.component';
import { VoucherDialogComponent } from './components/voucher-dialog/voucher-dialog.component';
import { EnvironmentsComponent } from './pages/environments/environments.component';
import { LicensesComponent } from './pages/licenses/licenses.component';
import { TranslationsComponent } from './pages/translations/translations.component';
import { VouchersComponent } from './pages/vouchers/vouchers.component';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { FilesComponent } from './pages/files/files.component';
import { FileDialogComponent } from './components/file-dialog/file-dialog.component';

@NgModule({
	declarations: [
		ProjectComponent,
		EnvironmentsComponent,
		VouchersComponent,
		TranslationsComponent,
		TranslationDialogComponent,
		EnvironmentDialogComponent,
		VoucherDialogComponent,
		LicensesComponent,
		LicenseDialogComponent,
		FilesComponent,
		FileDialogComponent,
	],
	imports: [
		CommonModule,
		ProjectRoutingModule,
		MaterialModule,
		SharedModule,
		AngularClientModule,
		TranslateModule,
		TableModule,
		FormsModule,
		ReactiveFormsModule,
		InputModule,
		TextAreaModule,
		SingleSelectModule,
		LoadingButtonModule,
	],
})
export class ProjectModule {}

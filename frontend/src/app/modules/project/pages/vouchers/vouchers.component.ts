import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Translation, Voucher } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { VoucherService } from 'src/app/core/services/voucher/voucher.service';
import { VoucherDialogComponent } from '../../components/voucher-dialog/voucher-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-vouchers',
	templateUrl: './vouchers.component.html',
	styleUrls: ['./vouchers.component.scss'],
})
export class VouchersComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Translation>;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['key'];

	public displayedColumns = ['status', 'type', 'lastActivity', 'actions'];

	public constructor(public voucherService: VoucherService, public userService: UserService, public matDialog: MatDialog) {}

	public list(paginationRequest: PaginationRequest): void {
		this.voucherService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openCheckDialog(voucher?: Voucher): void {
		this.matDialog
			.open<VoucherDialogComponent, Voucher, boolean>(VoucherDialogComponent, { width: '900px', data: voucher })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteVoucher(voucher: Voucher): void {
		this.voucherService
			.delete(voucher)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}
}

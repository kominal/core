import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent } from '@kominal/core-angular-client';
import { File } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { FileService } from 'src/app/core/services/file/file.service';
import { FileDialogComponent } from '../../components/file-dialog/file-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-files',
	templateUrl: './files.component.html',
	styleUrls: ['./files.component.scss'],
})
export class FilesComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<File>;

	public data = new Subject<PaginationResponse<File>>();

	public autoColumns = ['key'];

	public displayedColumns = ['key', 'actions'];

	public constructor(public fileService: FileService, private matDialog: MatDialog) {}

	public list(paginationRequest: PaginationRequest): void {
		this.fileService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openFileDialog(data?: File): void {
		this.matDialog
			.open<FileDialogComponent, File, boolean>(FileDialogComponent, { width: '900px', data })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteFile(file: File): void {
		this.fileService
			.delete(file)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}
}

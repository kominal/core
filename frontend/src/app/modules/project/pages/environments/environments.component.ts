import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-angular-client';
import { Environment } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { EnvironmentDialogComponent } from '../../components/environment-dialog/environment-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-environments',
	templateUrl: './environments.component.html',
	styleUrls: ['./environments.component.scss'],
})
export class EnvironmentsComponent {
	public constructor(
		public projectService: ProjectService,
		public environmentService: EnvironmentService,
		private userService: UserService,
		private router: Router,
		private matDialog: MatDialog
	) {}

	public navigateToEnvironment(environment: Environment): void {
		combineLatest([this.userService.currentTenantSlug$, this.projectService.currentProjectSlug$])
			.pipe(
				first(([tenantSlug, projectSlug]) => !!tenantSlug && !!projectSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug]) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${projectSlug}/environments/${environment.slug}`);
			});
	}

	public openEnvironmentDialog(environment?: Environment): void {
		this.matDialog.open<EnvironmentDialogComponent>(EnvironmentDialogComponent, { width: '500px', data: environment });
	}

	public deleteEnvironment(environment: Environment): void {
		this.environmentService
			.delete(environment)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.projectService.reload$.next());
	}
}

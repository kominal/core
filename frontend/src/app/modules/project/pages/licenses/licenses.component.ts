import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { License, Translation } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { LicenseService } from 'src/app/core/services/license/license.service';
import { LicenseDialogComponent } from '../../components/license-dialog/license-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-licenses',
	templateUrl: './licenses.component.html',
	styleUrls: ['./licenses.component.scss'],
})
export class LicensesComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Translation>;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = [];

	public displayedColumns = ['name', 'price', 'tax', 'actions'];

	public constructor(public licenseService: LicenseService, public userService: UserService, public matDialog: MatDialog) {}

	public list(paginationRequest: PaginationRequest): void {
		this.licenseService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openLicenseDialog(license?: License): void {
		this.matDialog
			.open<LicenseDialogComponent, License, boolean>(LicenseDialogComponent, { width: '900px', data: license })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteLicense(license: License): void {
		this.licenseService
			.delete(license)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}
}

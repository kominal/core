import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Translation } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { ProjectTranslationService } from 'src/app/core/services/project-translation/project-translation.service';
import { TranslationDialogComponent } from '../../components/translation-dialog/translation-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-translations',
	templateUrl: './translations.component.html',
	styleUrls: ['./translations.component.scss'],
})
export class TranslationsComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Translation>;

	public Object = Object;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['key'];

  public displayedColumns = ['key', 'value-de', 'value-en', 'type', 'actions'];

	public constructor(
		public projectTranslationService: ProjectTranslationService,
		public userService: UserService,
		public matDialog: MatDialog
	) {}

	public list(paginationRequest: PaginationRequest): void {
		this.projectTranslationService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openTranslationDialog(translation?: Translation): void {
		this.matDialog
			.open<TranslationDialogComponent, Translation, boolean>(TranslationDialogComponent, { width: '900px', data: translation })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteTranslation(translation: Translation): void {
		this.projectTranslationService
			.delete(translation)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public getLanguages(translation: Translation): string {
		return (
			translation.values
				?.map((v) => v.language)
				.join(', ')
				.toUpperCase() || ''
		);
	}
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnvironmentsComponent } from './pages/environments/environments.component';
import { FilesComponent } from './pages/files/files.component';
import { LicensesComponent } from './pages/licenses/licenses.component';
import { TranslationsComponent } from './pages/translations/translations.component';
import { VouchersComponent } from './pages/vouchers/vouchers.component';

const routes: Routes = [
	{
		path: 'environments',
		component: EnvironmentsComponent,
	},
	{
		path: 'translations',
		component: TranslationsComponent,
	},
	{
		path: 'licenses',
		component: LicensesComponent,
	},
	{
		path: 'vouchers',
		component: VouchersComponent,
	},
	{
		path: 'files',
		component: FilesComponent,
	},
	{
		path: '**',
		redirectTo: 'environments',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ProjectRoutingModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule, TranslateModule } from '@kominal/core-angular-client';
import { CardListComponent } from './components/card-list/card-list.component';
import { HeaderComponent } from './components/header/header.component';
import { TabBarComponent } from './components/tab-bar/tab-bar.component';

@NgModule({
	declarations: [HeaderComponent, CardListComponent, TabBarComponent],
	imports: [CommonModule, RouterModule, MaterialModule, TranslateModule],
	exports: [HeaderComponent, CardListComponent, TabBarComponent],
})
export class SharedModule {}

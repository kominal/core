import { Component, ContentChild, EventEmitter, Input, Output, TemplateRef } from '@angular/core';

@Component({
	selector: 'app-card-list',
	templateUrl: './card-list.component.html',
	styleUrls: ['./card-list.component.scss'],
})
export class CardListComponent<T> {
	@Input()
	public cards: T[] | null | undefined;

	@Input()
	public allowNew = false;

	@ContentChild(TemplateRef)
	public templateRef: TemplateRef<any> | null = null;

	@Output()
	public readonly onNew = new EventEmitter();
}

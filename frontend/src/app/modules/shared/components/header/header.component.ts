import { Component, Input } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { TenantService, TranslateService, UserService } from '@kominal/core-angular-client';
import { Tenant } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ProjectTenantService } from 'src/app/core/services/project-tenant/project-tenant.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { ServiceService } from 'src/app/core/services/service/service.service';

@UntilDestroy()
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
	@Input() public drawer?: MatDrawer;

	public isTenantSelectable: Observable<boolean>;

	public constructor(
		public userService: UserService<Tenant>,
		public translateService: TranslateService,
		public tenantService: TenantService<Tenant>,
		public projectService: ProjectService,
		public environmentService: EnvironmentService,
		public serviceService: ServiceService,
		public projectTenantService: ProjectTenantService,
		private router: Router
	) {
		this.isTenantSelectable = combineLatest([userService.currentTenant$, userService.tenants$]).pipe(
			map(([tenant, tenants]) => !!tenant || !tenants || tenants.length > 0)
		);
	}

	public selectLanguage(language: string): void {
		this.translateService.use(language);
		localStorage.setItem('language', language);
	}

	public navigateToEnvironment(url?: string): void {
		combineLatest([
			this.userService.currentTenantSlug$,
			this.projectService.currentProjectSlug$,
			this.environmentService.currentEnvironmentSlug$,
		])
			.pipe(
				first(([tenantSlug, projectSlug, environmentSlug]) => !!tenantSlug && !!projectSlug && !!environmentSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug, environmentSlug]) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${projectSlug}/environments/${environmentSlug}/${url}`);
			});
	}

	public navigateToProject(url?: string): void {
		combineLatest([this.userService.currentTenantSlug$, this.projectService.currentProjectSlug$])
			.pipe(
				first(([tenantSlug, projectSlug]) => !!tenantSlug && !!projectSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug]) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${projectSlug}/${url}`);
			});
	}

	public navigateToTenant(url?: string): void {
		this.userService.currentTenantSlug$.pipe(first((tenantSlug) => !!tenantSlug)).subscribe((tenantSlug) => {
			this.router.navigateByUrl(`cockpit/${tenantSlug}${url ? `/${url}` : ''}`);
		});
	}
}

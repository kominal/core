import { Component, Input } from '@angular/core';

export interface Tab {
	name: string;
	icon: string;
	link: string;
}

@Component({
	selector: 'app-tab-bar',
	templateUrl: './tab-bar.component.html',
	styleUrls: ['./tab-bar.component.scss'],
})
export class TabBarComponent {
	@Input()
	public tabs: Tab[] = [];
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsComponent } from './pages/reports/reports.component';
import { ServicesComponent } from './pages/services/services.component';
import { TenantsComponent } from './pages/tenants/tenants.component';

const routes: Routes = [
	{
		path: 'services',
		component: ServicesComponent,
	},
	{
		path: 'tenants',
		component: TenantsComponent,
	},
	{
		path: 'reports',
		component: ReportsComponent,
	},
	{
		path: '**',
		redirectTo: 'services',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class EnvironmentRoutingModule {}

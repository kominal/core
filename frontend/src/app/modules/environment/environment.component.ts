import { Component } from '@angular/core';
import { Tab } from 'src/app/modules/shared/components/tab-bar/tab-bar.component';

@Component({
	selector: 'app-environment',
	templateUrl: './environment.component.html',
	styleUrls: ['./environment.component.scss'],
})
export class EnvironmentComponent {
	public tabs: Tab[] = [
		{ name: 'services', icon: 'folder', link: 'services' },
		{ name: 'tenants', icon: 'business', link: 'tenants' },
		{ name: 'reports', icon: 'article', link: 'reports' },
	];
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	AngularClientModule,
	InputModule,
	MaterialModule,
	MultiSelectModule,
	SingleSelectModule,
	TableModule,
	TranslateModule,
} from '@kominal/core-angular-client';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReportDialogComponent } from './components/report-dialog/report-dialog.component';
import { ServiceDialogComponent } from './components/service-dialog/service-dialog.component';
import { TenantDialogComponent } from './components/tenant-dialog/tenant-dialog.component';
import { EnvironmentRoutingModule } from './environment-routing.module';
import { EnvironmentComponent } from './environment.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { ServicesComponent } from './pages/services/services.component';
import { TenantsComponent } from './pages/tenants/tenants.component';

@NgModule({
	declarations: [
		EnvironmentComponent,
		TenantsComponent,
		ServicesComponent,
		TenantDialogComponent,
		ServiceDialogComponent,
		ReportDialogComponent,
		ReportsComponent,
	],
	imports: [
		CommonModule,
		EnvironmentRoutingModule,
		SharedModule,
		MaterialModule,
		AngularClientModule,
		TranslateModule,
		TableModule,
		FormsModule,
		ReactiveFormsModule,
		InputModule,
		MultiSelectModule,
		SingleSelectModule,
	],
})
export class EnvironmentModule {}

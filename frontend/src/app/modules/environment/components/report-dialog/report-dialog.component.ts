import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Report } from '@kominal/core-common/models/report';
import { FormArray, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ReportService } from 'src/app/core/services/report/report.service';
import { ServiceService } from 'src/app/core/services/service/service.service';

@UntilDestroy()
@Component({
	selector: 'app-report-dialog',
	templateUrl: './report-dialog.component.html',
	styleUrls: ['./report-dialog.component.scss'],
})
export class ReportDialogComponent {
	public loading = false;

	public emails = new FormControl<string[]>([]);

	public events: any = new FormArray<{ serviceId: string; types: string[] }>([]);

	public form = new FormGroup<Pick<Report, '_id' | 'emails' | 'events'>>({
		_id: new FormControl(),
		emails: this.emails,
		events: this.events,
	});

	public separatorKeysCodes = [ENTER, COMMA];

	public constructor(
		private dialogRef: MatDialogRef<ReportDialogComponent>,
		public serviceService: ServiceService,
		public reportService: ReportService,
		@Inject(MAT_DIALOG_DATA) report: Report
	) {
		if (report) {
			for (const e of report.events) {
				this.addEventControl();
			}
			this.form.patchValue(report);
		}
	}

	public addEventControl(): void {
		this.events.controls.push(
			new FormGroup({
				serviceId: new FormControl(),
				types: new FormControl(),
			})
		);
	}

	public removeEventControl(index: number): void {
		this.events.removeAt(index);
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.reportService
			.createOrUpdate({ ...this.form.value, lastUpdated: null as any })
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.serviceService.reload$.next();
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

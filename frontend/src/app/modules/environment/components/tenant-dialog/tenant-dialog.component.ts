import { Component } from '@angular/core';

@Component({
	selector: 'app-tenant-dialog',
	templateUrl: './tenant-dialog.component.html',
	styleUrls: ['./tenant-dialog.component.scss'],
})
export class TenantDialogComponent {}

import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Service } from '@kominal/core-common';
import { FormArray, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ServiceService } from 'src/app/core/services/service/service.service';

@UntilDestroy()
@Component({
	selector: 'app-service-dialog',
	templateUrl: './service-dialog.component.html',
	styleUrls: ['./service-dialog.component.scss'],
})
export class ServiceDialogComponent {
	public loading = false;

	public tokens = new FormArray<string>([], Validators.required);

	public form = new FormGroup<Pick<Service, '_id' | 'name' | 'slug' | 'tokens'>>({
		_id: new FormControl(),
		name: new FormControl<string>(undefined, Validators.required),
		slug: new FormControl<string>(undefined, Validators.required),
		tokens: this.tokens,
	});

	public constructor(
		private dialogRef: MatDialogRef<ServiceDialogComponent>,
		private serviceService: ServiceService,
		@Inject(MAT_DIALOG_DATA) service: Service
	) {
		if (service) {
			this.form.patchValue(service);
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.serviceService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.serviceService.reload$.next();
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

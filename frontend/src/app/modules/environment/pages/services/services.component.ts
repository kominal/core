import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-angular-client';
import { Service } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { ServiceService } from 'src/app/core/services/service/service.service';
import { ServiceDialogComponent } from '../../components/service-dialog/service-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-services',
	templateUrl: './services.component.html',
	styleUrls: ['./services.component.scss'],
})
export class ServicesComponent {
	public constructor(
		public projectService: ProjectService,
		public environmentService: EnvironmentService,
		public serviceService: ServiceService,
		private userService: UserService,
		private router: Router,
		private matDialog: MatDialog
	) {}

	public navigateToService(service: Service): void {
		combineLatest([
			this.userService.currentTenantSlug$,
			this.projectService.currentProjectSlug$,
			this.environmentService.currentEnvironmentSlug$,
		])
			.pipe(
				first(([tenantSlug, projectSlug, environmentSlug]) => !!tenantSlug && !!projectSlug && !!environmentSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug, environmentSlug]) => {
				this.router.navigateByUrl(
					`cockpit/${tenantSlug}/projects/${projectSlug}/environments/${environmentSlug}/services/${service.slug}`
				);
			});
	}

	public openServiceDialog(service?: Service): void {
		this.matDialog.open<ServiceDialogComponent>(ServiceDialogComponent, { width: '500px', data: service });
	}

	public deleteService(service: Service): void {
		this.serviceService
			.delete(service)
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.projectService.reload$.next());
	}
}

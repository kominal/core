import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Tenant } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { CoreTenantService } from 'src/app/core/services/core-tenant/core-tenant.service';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { TenantDialogComponent } from '../../components/tenant-dialog/tenant-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-tenants',
	templateUrl: './tenants.component.html',
	styleUrls: ['./tenants.component.scss'],
})
export class TenantsComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Tenant>;

	public Object = Object;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['name', 'slug'];

	public displayedColumns = ['name', 'slug', 'actions'];

	public constructor(
		public coreTenantService: CoreTenantService,
		public userService: UserService,
		public matDialog: MatDialog,
		public projectService: ProjectService,
		public environmentService: EnvironmentService,
		private router: Router
	) {}

	public list(paginationRequest: PaginationRequest): void {
		this.coreTenantService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openTenantDialog(tenant?: Tenant): void {
		this.matDialog
			.open<TenantDialogComponent, Tenant, boolean>(TenantDialogComponent, { width: '900px', data: tenant })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteTenant(tenant: Tenant): void {
		this.coreTenantService
			.delete(tenant, { confirm: true })
			.pipe(untilDestroyed(this), first())
			.subscribe(() => this.table.triggerUpdate());
	}

	public navigateToTenant(tenant: Tenant): void {
		combineLatest([
			this.userService.currentTenantSlug$,
			this.projectService.currentProjectSlug$,
			this.environmentService.currentEnvironmentSlug$,
		])
			.pipe(
				first(([tenantSlug, projectSlug, environmentSlug]) => !!tenantSlug && !!projectSlug && !!environmentSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug, environmentSlug]) => {
				this.router.navigateByUrl(
					`cockpit/${tenantSlug}/projects/${projectSlug}/environments/${environmentSlug}/tenants/${tenant.slug}`
				);
			});
	}
}

import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Report } from '@kominal/core-common/models/report';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { ReportService } from 'src/app/core/services/report/report.service';
import { ReportDialogComponent } from '../../components/report-dialog/report-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-reports',
	templateUrl: './reports.component.html',
	styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Report>;

	public Object = Object;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = [''];

	public displayedColumns = ['emails', 'events', 'actions'];

	public constructor(public reportService: ReportService, public userService: UserService, public matDialog: MatDialog) {}

	public list(paginationRequest: PaginationRequest): void {
		this.reportService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openReportDialog(report?: Report): void {
		this.matDialog
			.open<ReportDialogComponent, Report, boolean>(ReportDialogComponent, { width: '500px', data: report })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteReport(report: Report): void {
		this.reportService
			.delete(report)
			.pipe(untilDestroyed(this), first())
			.subscribe(() => this.table.triggerUpdate());
	}
}

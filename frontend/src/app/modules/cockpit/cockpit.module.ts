import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {
	AngularClientModule,
	InputModule,
	LoadingButtonModule,
	MaterialModule,
	SingleSelectModule,
	TableModule,
	TextAreaModule,
	TranslateModule,
} from '@kominal/core-angular-client';
import { SharedModule } from '../shared/shared.module';
import { CockpitRoutingModule } from './cockpit-routing.module';
import { CockpitComponent } from './cockpit.component';
import { ProjectDialogComponent } from './components/project-dialog/project-dialog.component';
import { TranslationDialogComponent } from './components/translation-dialog/translation-dialog.component';
import { UserDialogComponent } from './components/user-dialog/user-dialog.component';
import { EnvironmentComponent } from './pages/environment/environment.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { TenantCreationComponent } from './pages/tenant-creation/tenant-creation.component';
import { TenantUsersComponent } from './pages/tenant-users/tenant-users.component';
import { TenantComponent } from './pages/tenant/tenant.component';
import { TranslationsComponent } from './pages/translations/translations.component';
import { UsersComponent } from './pages/users/users.component';

@NgModule({
	declarations: [
		CockpitComponent,
		TenantCreationComponent,
		UsersComponent,
		ProjectsComponent,
		TranslationsComponent,
		ProjectDialogComponent,
		TranslationDialogComponent,
		EnvironmentComponent,
		UserDialogComponent,
		TenantUsersComponent,
		TenantComponent,
		SettingsComponent,
	],
	imports: [
		CommonModule,
		CockpitRoutingModule,
		MaterialModule,
		TranslateModule,
		AngularClientModule,
		TableModule,
		InputModule,
		SingleSelectModule,
		AngularEditorModule,
		FormsModule,
		ReactiveFormsModule,
		TextAreaModule,
		SharedModule,
		LoadingButtonModule,
	],
})
export class CockpitModule {}

import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Project, Translation } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ProjectService } from 'src/app/core/services/project/project.service';

@UntilDestroy()
@Component({
	selector: 'app-project-dialog',
	templateUrl: './project-dialog.component.html',
	styleUrls: ['./project-dialog.component.scss'],
})
export class ProjectDialogComponent {
	public loading = false;

	public form = new FormGroup<Omit<Project, 'tenantId'>>({
		_id: new FormControl(),
		name: new FormControl<string>(undefined, Validators.required),
		slug: new FormControl<string>(undefined, Validators.required),
	});

	public constructor(
		private dialogRef: MatDialogRef<ProjectDialogComponent>,
		private projectService: ProjectService,
		@Inject(MAT_DIALOG_DATA) translation: Translation
	) {
		if (translation) {
			this.form.patchValue(translation);
		}
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.projectService
			.createOrUpdate(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.projectService.reload$.next();
					this.dialogRef.close(true);
				} else {
					this.loading = false;
					this.form.enable({ emitEvent: false });
				}
			});
	}
}

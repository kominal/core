import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsComponent } from './pages/projects/projects.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { TranslationsComponent } from './pages/translations/translations.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
	{
		path: 'projects',
		component: ProjectsComponent,
	},
	{
		path: 'users',
		component: UsersComponent,
	},
	{
		path: 'translations',
		component: TranslationsComponent,
	},
	{
		path: 'settings',
		component: SettingsComponent,
	},
	{
		path: '**',
		redirectTo: 'projects',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class CockpitRoutingModule {}

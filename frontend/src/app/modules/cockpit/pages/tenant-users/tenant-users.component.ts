import { Component } from '@angular/core';

@Component({
	selector: 'app-tenant-users',
	templateUrl: './tenant-users.component.html',
	styleUrls: ['./tenant-users.component.scss'],
})
export class TenantUsersComponent {}

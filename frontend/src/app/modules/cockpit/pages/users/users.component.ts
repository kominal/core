import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PaginationRequest, PaginationResponse, TableComponent, UserService } from '@kominal/core-angular-client';
import { Translation } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { CoreUserService } from 'src/app/core/services/core-user/core-user.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { TranslationDialogComponent } from '../../components/translation-dialog/translation-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<Translation>;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['email'];

	public displayedColumns = ['email', 'languages', 'actions'];

	public constructor(
		public coreUserService: CoreUserService,
		public userService: UserService,
		public matDialog: MatDialog,
		public projectService: ProjectService
	) {}

	public list(paginationRequest: PaginationRequest): void {
		this.coreUserService
			.list(paginationRequest)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openTranslationDialog(translation?: Translation): void {
		this.matDialog
			.open<TranslationDialogComponent, Translation, boolean>(TranslationDialogComponent, { width: '900px', data: translation })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteTranslation(translation: Translation): void {
		this.coreUserService
			.delete(translation, { confirm: true, type: 'translation', identifier: 'name' })
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public getLanguages(translation: Translation): string {
		return (
			translation.values
				?.map((v) => v.language)
				.join(', ')
				.toUpperCase() || ''
		);
	}
}

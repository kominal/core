import { Component } from '@angular/core';
import { TenantService, UserService } from '@kominal/core-angular-client';
import { Tenant } from '@kominal/core-common';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
	public loading = false;

	public form: any = new FormGroup<Pick<Tenant, '_id' | 'stripePublishableKey' | 'stripeSecretKey'>>({
		_id: new FormControl(),
		stripePublishableKey: new FormControl<string | undefined>(undefined),
		stripeSecretKey: new FormControl<string | undefined>(undefined),
	});

	public constructor(private userService: UserService, private tenantService: TenantService) {
		userService.currentTenant$.pipe(untilDestroyed(this)).subscribe((tenant) => {
			if (tenant) {
				this.form.patchValue(tenant);
			}
		});
	}

	public onSubmit(): void {
		this.form.disable({ emitEvent: false });
		this.loading = true;

		this.tenantService
			.update(this.form.value)
			.pipe(untilDestroyed(this))
			.subscribe((s) => {
				if (s) {
					this.userService.reloadCurrentTenant$.next();
				}
				this.form.enable({ emitEvent: false });
				this.loading = false;
			});
	}
}

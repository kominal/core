import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-angular-client';
import { Project } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, first } from 'rxjs/operators';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { ProjectDialogComponent } from '../../components/project-dialog/project-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
	styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent {
	public constructor(
		public projectService: ProjectService,
		private userService: UserService,
		private router: Router,
		private matDialog: MatDialog
	) {}

	public navigateToProject(project: Project): void {
		this.userService.currentTenantSlug$
			.pipe(
				first((tenantSlug) => !!tenantSlug),
				untilDestroyed(this)
			)
			.subscribe((tenantSlug) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${project.slug}`);
			});
	}

	public openProjectDialog(project?: Project): void {
		this.matDialog.open<ProjectDialogComponent>(ProjectDialogComponent, { width: '500px', data: project });
	}

	public deleteProject(project: Project): void {
		this.projectService
			.delete(project, { confirm: true, type: 'project', identifier: 'name' })
			.pipe(
				untilDestroyed(this),
				first(),
				filter((v) => v)
			)
			.subscribe(() => this.projectService.reload$.next());
	}
}

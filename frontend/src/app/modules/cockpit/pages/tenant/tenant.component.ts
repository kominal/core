import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-angular-client';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProjectService } from 'src/app/core/services/project/project.service';

@UntilDestroy()
@Component({
	selector: 'app-tenant',
	templateUrl: './tenant.component.html',
	styleUrls: ['./tenant.component.scss'],
})
export class TenantComponent {
	public constructor(public projectService: ProjectService, private userService: UserService, private router: Router) {}

	public navigateToProject(projectSlug?: string): void {
		this.userService.currentTenantSlug$
			.pipe(
				first((tenantSlug) => !!tenantSlug),
				untilDestroyed(this)
			)
			.subscribe((tenantSlug) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${projectSlug}`);
			});
	}

	public navigateToEnvironment(environmentSlug?: string): void {
		this.navigateTo(`environments/${environmentSlug}`);
	}

	public navigateTo(url: string): void {
		combineLatest([this.userService.currentTenantSlug$, this.projectService.currentProjectSlug$])
			.pipe(
				first(([tenantSlug, projectSlug]) => !!tenantSlug && !!projectSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug]) => {
				this.router.navigateByUrl(`cockpit/${tenantSlug}/projects/${projectSlug}/${url}`);
			});
	}

	public navigateToTenant(url?: string): void {
		this.userService.currentTenantSlug$.pipe(first((tenantSlug) => !!tenantSlug)).subscribe((tenantSlug) => {
			this.router.navigateByUrl(`cockpit/${tenantSlug}${url ? `/${url}` : ''}`);
		});
	}
}

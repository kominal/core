import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LayoutService, UserService } from '@kominal/core-angular-client';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Tab } from '../shared/components/tab-bar/tab-bar.component';

@UntilDestroy()
@Component({
	selector: 'app-cockpit',
	templateUrl: './cockpit.component.html',
	styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent {
	public tabs: Tab[] = [
		{ name: 'projects', icon: 'folder', link: 'projects' },
		{ name: 'translations', icon: 'translate', link: 'translations' },
		{ name: 'users', icon: 'person', link: 'users' },
		{ name: 'settings', icon: 'settings', link: 'settings' },
	];

	public constructor(public userService: UserService, public router: Router, public layoutService: LayoutService, titleService: Title) {
		titleService.setTitle('Core - Cockpit');
	}
}

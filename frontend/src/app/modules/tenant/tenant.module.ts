import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularClientModule, InputModule, MaterialModule, TableModule, TranslateModule } from '@kominal/core-angular-client';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { AddUserDialogComponent } from './components/add-user-dialog/add-user-dialog.component';
import { PermissionsDialogComponent } from './components/permissions-dialog/permissions-dialog.component';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { PaymentMethodsComponent } from './pages/payment-methods/payment-methods.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { UsersComponent } from './pages/users/users.component';
import { TenantRoutingModule } from './tenant-routing.module';
import { TenantComponent } from './tenant.component';

@NgModule({
	declarations: [
		TenantComponent,
		PaymentMethodsComponent,
		InvoicesComponent,
		PaymentsComponent,
		UsersComponent,
		PermissionsDialogComponent,
		AddUserDialogComponent,
	],
	imports: [
		CommonModule,
		TenantRoutingModule,
		CommonModule,
		SharedModule,
		MaterialModule,
		AngularClientModule,
		TranslateModule,
		TableModule,
		FormsModule,
		ReactiveFormsModule,
		InputModule,
	],
})
export class TenantModule {}

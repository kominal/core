import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogService, PaginationRequest, PaginationResponse, TableComponent, User, UserService } from '@kominal/core-angular-client';
import { Tenant } from '@kominal/core-common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, Subject } from 'rxjs';
import { filter, first, map, switchMap } from 'rxjs/operators';
import { EnvironmentService } from 'src/app/core/services/environment/environment.service';
import { ProjectTenantService } from 'src/app/core/services/project-tenant/project-tenant.service';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { AddUserDialogComponent } from '../../components/add-user-dialog/add-user-dialog.component';
import { PermissionsDialogComponent } from '../../components/permissions-dialog/permissions-dialog.component';

@UntilDestroy()
@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
	@ViewChild(TableComponent)
	public table!: TableComponent<User>;

	public Object = Object;

	public data = new Subject<PaginationResponse<any>>();

	public autoColumns = ['email'];

	public displayedColumns = ['email', 'actions'];

	public constructor(
		public projectTenantService: ProjectTenantService,
		public userService: UserService,
		public matDialog: MatDialog,
		public projectService: ProjectService,
		public environmentService: EnvironmentService,
		private router: Router,
		private dialogService: DialogService
	) {}

	public list(paginationRequest: PaginationRequest): void {
		this.projectTenantService.currentProjectTenant$
			.pipe(
				first((tenant) => !!tenant),
				switchMap((tenant) => this.userService.getUsers(tenant!._id, paginationRequest))
			)
			.pipe(untilDestroyed(this), first())
			.subscribe((v) => this.data.next(v));
	}

	public openAddUserDialog(): void {
		this.matDialog
			.open<AddUserDialogComponent, User, boolean>(AddUserDialogComponent, { width: '500px' })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public openPermissionsDialog(user: User): void {
		this.matDialog
			.open<PermissionsDialogComponent, User, boolean>(PermissionsDialogComponent, { data: user })
			.afterClosed()
			.pipe(
				filter((v) => !!v),
				untilDestroyed(this)
			)
			.subscribe(() => this.table.triggerUpdate());
	}

	public deleteUser(user: User): void {
		this.dialogService
			.openConfirmDeleteDialog(
				this.projectTenantService.currentProjectTenant$.pipe(
					first((tenant) => !!tenant),
					switchMap((tenant) => this.userService.deleteUser(user._id!, tenant!._id)),
					map(() => true),
					untilDestroyed(this)
				),
				'user',
				user.email
			)
			.pipe(filter((v) => v))
			.subscribe(() => this.table.triggerUpdate());
	}

	public navigateToTenant(tenant: Tenant): void {
		combineLatest([
			this.userService.currentTenantSlug$,
			this.projectService.currentProjectSlug$,
			this.environmentService.currentEnvironmentSlug$,
		])
			.pipe(
				first(([tenantSlug, projectSlug, environmentSlug]) => !!tenantSlug && !!projectSlug && !!environmentSlug),
				untilDestroyed(this)
			)
			.subscribe(([tenantSlug, projectSlug, environmentSlug]) => {
				this.router.navigateByUrl(
					`cockpit/${tenantSlug}/projects/${projectSlug}/environments/${environmentSlug}/tenants/${tenant.slug}`
				);
			});
	}
}

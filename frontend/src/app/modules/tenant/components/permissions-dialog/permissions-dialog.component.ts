import { Component } from '@angular/core';

@Component({
	selector: 'app-permissions-dialog',
	templateUrl: './permissions-dialog.component.html',
	styleUrls: ['./permissions-dialog.component.scss'],
})
export class PermissionsDialogComponent {}

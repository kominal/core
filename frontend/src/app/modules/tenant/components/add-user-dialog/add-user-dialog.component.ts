import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AnalyticsService, UserService } from '@kominal/core-angular-client';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { ProjectTenantService } from 'src/app/core/services/project-tenant/project-tenant.service';

@UntilDestroy()
@Component({
	selector: 'app-add-user-dialog',
	templateUrl: './add-user-dialog.component.html',
	styleUrls: ['./add-user-dialog.component.scss'],
})
export class AddUserDialogComponent {
	public form = new FormGroup<{ email: string }>({
		email: new FormControl('', [Validators.required, Validators.email]),
	});

	public constructor(
		public dialogRef: MatDialogRef<AddUserDialogComponent>,
		public userService: UserService,
		public projectTenantService: ProjectTenantService,
		private analyticsService: AnalyticsService
	) {}

	public async onSubmit(): Promise<void> {
		this.form.disable({ emitEvent: false });

		combineLatest([this.projectTenantService.currentProjectTenant$, this.userService.findUserByEmail(this.form.value.email)])
			.pipe(
				first(([tenant]) => !!tenant),
				switchMap(([tenant, user]) => this.userService.setPermissions(tenant!._id, user._id, [])),
				untilDestroyed(this),
				map(() => true),
				catchError((e) => {
					this.analyticsService.handleError(e);
					this.form.enable({ emitEvent: false });
					return of(false);
				}),
				first((v) => !!v)
			)
			.subscribe(() => this.dialogRef.close(true));
	}
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { PaymentMethodsComponent } from './pages/payment-methods/payment-methods.component';
import { PaymentsComponent } from './pages/payments/payments.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
	{
		path: 'users',
		component: UsersComponent,
	},
	{
		path: 'invoices',
		component: InvoicesComponent,
	},
	{
		path: 'payment-methods',
		component: PaymentMethodsComponent,
	},
	{
		path: 'payments',
		component: PaymentsComponent,
	},
	{
		path: '**',
		redirectTo: 'users',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TenantRoutingModule {}

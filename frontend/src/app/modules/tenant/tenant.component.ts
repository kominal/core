import { Component } from '@angular/core';
import { Tab } from '../shared/components/tab-bar/tab-bar.component';

@Component({
	selector: 'app-tenant',
	templateUrl: './tenant.component.html',
	styleUrls: ['./tenant.component.scss'],
})
export class TenantComponent {
	public tabs: Tab[] = [
		{ name: 'users', icon: 'person', link: 'users' },
		{ name: 'invoices', icon: 'receipt', link: 'invoices' },
		{ name: 'payment-methods', icon: 'credit_card', link: 'payment-methods' },
		{ name: 'payments', icon: 'money', link: 'payments' },
	];
}

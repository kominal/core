import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputModule, MaterialModule, TranslateModule } from '@kominal/core-angular-client';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RegisterComponent } from './pages/register/register.component';
import { RequestPasswordResetComponent } from './pages/request-password-reset/request-password-reset.component';

@NgModule({
	declarations: [AuthenticationComponent, LoginComponent, RegisterComponent, RequestPasswordResetComponent, PasswordResetComponent],
	imports: [CommonModule, AuthenticationRoutingModule, InputModule, MaterialModule, TranslateModule, FormsModule, ReactiveFormsModule],
})
export class AuthenticationModule {}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuestGuard } from '@kominal/core-angular-client';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RegisterComponent } from './pages/register/register.component';
import { RequestPasswordResetComponent } from './pages/request-password-reset/request-password-reset.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent, canActivate: [GuestGuard] },
	{ path: 'register', component: RegisterComponent, canActivate: [GuestGuard] },
	{ path: 'request-password-reset', component: RequestPasswordResetComponent, canActivate: [GuestGuard] },
	{ path: 'password-reset', component: PasswordResetComponent, canActivate: [GuestGuard] },
	{ path: '**', redirectTo: '/welcome' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AuthenticationRoutingModule {}

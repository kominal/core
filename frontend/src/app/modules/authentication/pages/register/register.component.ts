import { Component } from '@angular/core';
import { Validators } from '@angular/forms';

import { FormControl, FormGroup } from '@ngneat/reactive-forms';

import { AnalyticsService, UserService } from '@kominal/core-angular-client';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
	public loading = false;

	public form = new FormGroup<{ firstname: string; lastname: string; email: string; password: string }>({
		firstname: new FormControl('', [Validators.maxLength(256)]),
		lastname: new FormControl('', [Validators.maxLength(256)]),
		email: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(256)]),
		password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(256)]),
	});

	public constructor(public userService: UserService, private analyticsService: AnalyticsService) {}

	public async onSubmit(): Promise<void> {
		this.loading = true;

		const { email, password } = this.form.value;
		try {
			this.userService.register(email, password).subscribe();
		} catch (e) {
			this.analyticsService.handleError(e);
			this.loading = false;
		}
	}
}

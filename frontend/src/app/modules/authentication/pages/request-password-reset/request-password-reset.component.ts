import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AnalyticsService, UserService } from '@kominal/core-angular-client';
import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
	selector: 'app-request-password-reset',
	templateUrl: './request-password-reset.component.html',
	styleUrls: ['./request-password-reset.component.scss'],
})
export class RequestPasswordResetComponent {
	public loading = false;

	public form = new FormGroup<{ email: string; password: string }>({
		email: new FormControl('', {
			updateOn: 'submit',
			validators: [Validators.required, Validators.email, Validators.maxLength(256)],
		}),
		password: new FormControl('', {
			updateOn: 'submit',
			validators: [Validators.required, Validators.minLength(8), Validators.maxLength(256)],
		}),
	});

	public constructor(
		public userService: UserService,
		private activatedRoute: ActivatedRoute,
		private analyticsService: AnalyticsService,
		private router: Router
	) {}

	public onSubmit(): void {
		if (this.form.invalid) {
			return;
		}
		this.loading = true;
		const { email, password } = this.form.value;
		// this.userService
		// 	.login(email, password, { redirect: false })
		// 	.pipe(
		// 		catchError((e) => {
		// 			this.analyticsService.handleError(e);
		// 			this.loading = false;
		// 			return of();
		// 		})
		// 	)
		// 	.subscribe(() => this.router.navigate(['/welcome']));
	}
}

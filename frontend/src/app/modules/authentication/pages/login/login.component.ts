import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { FormControl, FormGroup } from '@ngneat/reactive-forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { AnalyticsService, UserService } from '@kominal/core-angular-client';

@UntilDestroy()
@Component({
	selector: 'lib-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
	public loading = false;

	public form = new FormGroup<{ email: string; password: string }>({
		email: new FormControl('', {
      updateOn: 'submit',
      validators: [Validators.required, Validators.email, Validators.maxLength(256)],
    }),
		password: new FormControl('', {
      updateOn: 'submit',
      validators: [Validators.required, Validators.minLength(8), Validators.maxLength(256)],
    }),
	});

	public constructor(
		public userService: UserService,
		private activatedRoute: ActivatedRoute,
		private analyticsService: AnalyticsService,
		private router: Router
	) {
		this.activatedRoute.queryParams.pipe(untilDestroyed(this)).subscribe(async (params) => {
			if (params.userId && params.token) {
				this.userService
					.verifyEmail(params.userId, params.token, { redirect: true })
					.pipe(
						catchError((e) => {
							this.analyticsService.handleError(e);
							return of();
						})
					)
					.subscribe();
			}
		});
	}

	public onSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const { email, password } = this.form.value;
    this.userService
			.login(email, password, { redirect: false })
			.pipe(
				catchError((e) => {
					this.analyticsService.handleError(e);
					this.loading = false;
					return of();
				})
			)
			.subscribe(() => this.router.navigate(['/welcome']));
	}
}

import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { GUEST_HOME_URL, UserService } from '@kominal/core-angular-client';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class WelcomeGuard implements CanActivate {
	public constructor(
		private userService: UserService,
		private router: Router,
		@Inject(GUEST_HOME_URL) private guestHomeUrl: string | undefined
	) {}

	public canActivate(route: ActivatedRouteSnapshot): boolean | Observable<boolean> {
		if (!this.userService.isLoggedIn()) {
			this.router.navigate([this.guestHomeUrl || '/']);
			return false;
		}

		if (route.queryParams.preventRedirect) {
			return true;
		}

		return this.userService.tenants$.pipe(
			filter((tenants) => !!tenants),
			map((tenants) => {
				if (tenants && tenants?.length > 0) {
					this.router.navigate(['cockpit', tenants[0].slug]);
					return false;
				}
				return true;
			})
		);
	}
}

import { TestBed } from '@angular/core/testing';

import { ProjectTenantService } from './project-tenant.service';

describe('ProjectTenantService', () => {
	let service: ProjectTenantService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ProjectTenantService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

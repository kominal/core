import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	RouterService,
	UserService,
} from '@kominal/core-angular-client';
import { Service } from '@kominal/core-common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, concatMap, filter, first, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { ProjectTenantHttpService } from '../../http/project-tenant/project-tenant-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class ProjectTenantService {
	public reload$ = new BehaviorSubject<void>(undefined);

	public documents$: Observable<Service[] | undefined>;

	public currentProjectTenantSlug$: Observable<string | undefined>;

	public currentProjectTenant$: Observable<Service | undefined>;

	public reloadCurrentService$ = new BehaviorSubject<void>(undefined);

	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private projectTenantHttpService: ProjectTenantHttpService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService,
		routerService: RouterService
	) {
		this.documents$ = combineLatest([this.userService.currentTenant$, this.reload$]).pipe(
			filter(([tenant]) => !!tenant),
			concatMap(() => [of(undefined), this.list().pipe(map((v) => v.items))]),
			switchAll(),
			shareReplay(2)
		);
		this.currentProjectTenantSlug$ = routerService.observePathParameter('projectTenantSlug');

		this.currentProjectTenant$ = combineLatest([
			userService.currentTenant$,
			projectService.currentProject$,
			environmentService.currentEnvironment$,
			this.currentProjectTenantSlug$,
			this.reloadCurrentService$,
		]).pipe(
			concatMap(([tenant, project, environment, serviceSlug]) =>
				tenant && project && environment && serviceSlug
					? [of(undefined), projectTenantHttpService.getBySlug(tenant._id, project._id, environment._id, serviceSlug)]
					: [of(undefined)]
			),
			switchAll(),
			shareReplay(1)
		);
		this.currentProjectTenant$.subscribe();
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Service>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			filter(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.projectTenantHttpService.list(tenant!._id, project!._id, environment!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Service | undefined> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.projectTenantHttpService.get(tenant!._id, project!._id, environment!._id, documentId)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.projectTenantHttpService.create(tenant!._id, project!._id, environment!._id, document)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Service>): Observable<boolean> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.projectTenantHttpService.update(tenant!._id, project!._id, environment!._id, document)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Service> & { _id: Service['_id'] }): Observable<boolean> {
		const observable = combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.projectTenantHttpService.delete(tenant!._id, project!._id, environment!._id, document._id)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'service', document.name || 'unknown');
	}
}

import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { License } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { LicenseHttpService } from '../../http/license/license-http.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class LicenseService {
	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private licenseHttpService: LicenseHttpService,
		private projectService: ProjectService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<License>> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.licenseHttpService.list(tenant!._id, project!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<License | undefined> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.licenseHttpService.get(tenant!._id, project!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<License>): Observable<false | { _id: License['_id'] }> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.licenseHttpService.create(tenant!._id, project!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<License>): Observable<boolean> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.licenseHttpService.update(tenant!._id, project!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<License>): Observable<false | { _id: License['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: License): Observable<boolean> {
		const observable = combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.licenseHttpService.delete(tenant!._id, project!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'license', document.name || 'unknown');
	}
}

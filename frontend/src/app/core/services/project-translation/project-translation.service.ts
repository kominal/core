import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { Translation } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { ProjectTranslationHttpService } from '../../http/project-translation/project-translation-http.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class ProjectTranslationService {
	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private projectTranslationHttpService: ProjectTranslationHttpService,
		private projectService: ProjectService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Translation>> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.projectTranslationHttpService.list(tenant!._id, project!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Translation | undefined> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.projectTranslationHttpService.get(tenant!._id, project!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Translation>): Observable<false | { _id: Translation['_id'] }> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.projectTranslationHttpService.create(tenant!._id, project!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Translation>): Observable<boolean> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.projectTranslationHttpService.update(tenant!._id, project!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Translation>): Observable<false | { _id: Translation['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Translation> & { _id: Translation['_id'] }): Observable<boolean> {
		const observable = combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.projectTranslationHttpService.delete(tenant!._id, project!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'translation', document.key || 'unknown');
	}
}

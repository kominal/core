import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	CrudWithTenantHttpService,
	CrudWithTenantService,
	DialogService,
	User,
	UserService,
} from '@kominal/core-angular-client';

@Injectable({
	providedIn: 'root',
})
export class CoreUserService extends CrudWithTenantService<User> {
	public constructor(userService: UserService, dialogService: DialogService, analyticsService: AnalyticsService, httpClient: HttpClient) {
		super(new CrudWithTenantHttpService(httpClient, '/controller', 'core-users'), userService, dialogService, analyticsService);
	}
}

import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	RouterService,
	UserService,
} from '@kominal/core-angular-client';
import { Environment, Project, Service } from '@kominal/core-common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, concatMap, filter, first, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { ServiceHttpService } from '../../http/service/service-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class ServiceService {
	public reload$ = new BehaviorSubject<void>(undefined);

	public documents$: Observable<Service[] | undefined>;

	public currentServiceSlug$: Observable<string | undefined>;

	public currentService$: Observable<Service | undefined>;

	public reloadCurrentService$ = new BehaviorSubject<void>(undefined);

	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private serviceHttpService: ServiceHttpService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService,
		routerService: RouterService
	) {
		this.documents$ = combineLatest([this.userService.currentTenant$, this.reload$]).pipe(
			filter(([tenant]) => !!tenant),
			concatMap(() => [of(undefined), this.list().pipe(map((v) => v.items))]),
			switchAll(),
			shareReplay(2)
		);
		this.currentServiceSlug$ = routerService.observePathParameter('serviceSlug');

		this.currentService$ = combineLatest([
			userService.currentTenant$,
			projectService.currentProject$,
			environmentService.currentEnvironment$,
			this.currentServiceSlug$,
			this.reloadCurrentService$,
		]).pipe(
			concatMap(([tenant, project, environment, serviceSlug]) =>
				tenant && project && environment && serviceSlug
					? [of(undefined), serviceHttpService.getBySlug(tenant._id, project._id, environment._id, serviceSlug)]
					: [of(undefined)]
			),
			switchAll(),
			shareReplay(1)
		);
		this.currentService$.subscribe();
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Service>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			filter(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.list(tenant!._id, project!._id, environment!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public listWithProperties(
		options: { paginationRequest?: PaginationRequest; project?: Project; environment?: Environment } = {}
	): Observable<PaginationResponse<Service>> {
		return combineLatest([
			this.userService.currentTenant$,
			options.project ? of(options.project) : this.projectService.currentProject$,
			options.environment ? of(options.environment) : this.environmentService.currentEnvironment$,
		]).pipe(
			filter(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.list(tenant!._id, project!._id, environment!._id, options.paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Service | undefined> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.get(tenant!._id, project!._id, environment!._id, documentId)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.create(tenant!._id, project!._id, environment!._id, document)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Service>): Observable<boolean> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.update(tenant!._id, project!._id, environment!._id, document)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Service> & { _id: Service['_id'] }): Observable<boolean> {
		const observable = combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.serviceHttpService.delete(tenant!._id, project!._id, environment!._id, document._id)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'service', document.name || 'unknown');
	}
}

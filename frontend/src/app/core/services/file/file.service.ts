import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { File } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { FileHttpService } from '../../http/file/file-http.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class FileService {
	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private fileHttpService: FileHttpService,
		private projectService: ProjectService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<File>> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.fileHttpService.list(tenant!._id, project!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<File | undefined> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.fileHttpService.get(tenant!._id, project!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<File>): Observable<false | { _id: File['_id'] }> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.fileHttpService.create(tenant!._id, project!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<File>): Observable<boolean> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.fileHttpService.update(tenant!._id, project!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<File>): Observable<false | { _id: File['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<File> & { _id: File['_id'] }): Observable<boolean> {
		const observable = combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.fileHttpService.delete(tenant!._id, project!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'file', document.key || 'unknown');
	}
}

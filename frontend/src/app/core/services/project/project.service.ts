import { Injectable } from '@angular/core';
import { AnalyticsService, CrudWithTenantService, DialogService, RouterService, UserService } from '@kominal/core-angular-client';
import { Project } from '@kominal/core-common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { concatMap, distinctUntilChanged, filter, map, shareReplay, switchAll } from 'rxjs/operators';
import { ProjectHttpService } from '../../http/project/project-http.service';

@Injectable({
	providedIn: 'root',
})
export class ProjectService extends CrudWithTenantService<Project> {
	public reload$ = new BehaviorSubject<void>(undefined);

	public documents$: Observable<Project[] | undefined>;

	public currentProjectSlug$: Observable<string | undefined>;

	public currentProject$: Observable<Project | undefined>;

	public reloadCurrentProject$ = new BehaviorSubject<void>(undefined);

	public constructor(
		userService: UserService,
		dialogService: DialogService,
		analyticsService: AnalyticsService,
		routerService: RouterService,
		projectHttpService: ProjectHttpService
	) {
		super(projectHttpService, userService, dialogService, analyticsService);
		this.documents$ = combineLatest([this.userService.currentTenant$, this.reload$]).pipe(
			filter(([tenant]) => !!tenant),
			concatMap(() => [of(undefined), this.list().pipe(map((v) => v.items))]),
			switchAll(),
			shareReplay(2)
		);
		this.currentProjectSlug$ = routerService.observePathParameter('projectSlug');

		this.currentProject$ = combineLatest([userService.currentTenant$, this.currentProjectSlug$, this.reloadCurrentProject$]).pipe(
			concatMap(([tenant, projectSlug]) =>
				tenant && projectSlug ? [of(undefined), projectHttpService.getBySlug(tenant._id, projectSlug)] : [of(undefined)]
			),
			switchAll(),
			distinctUntilChanged(),
			shareReplay(1)
		);
		this.currentProject$.subscribe();
	}
}

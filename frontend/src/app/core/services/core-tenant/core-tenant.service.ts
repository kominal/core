import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	RouterService,
	UserService,
} from '@kominal/core-angular-client';
import { Tenant, Translation } from '@kominal/core-common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, concatMap, first, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { CoreTenantHttpService } from '../../http/core-tenant/core-tenant-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class CoreTenantService {
	public currentProjectTenantSlug$: Observable<string | undefined>;

	public currentProjectTenant$: Observable<Tenant | undefined>;

	public reloadCurrentService$ = new BehaviorSubject<void>(undefined);

	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private coreTenantHttpService: CoreTenantHttpService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService,
		routerService: RouterService
	) {
		this.currentProjectTenantSlug$ = routerService.observePathParameter('serviceSlug');

		this.currentProjectTenant$ = combineLatest([
			userService.currentTenant$,
			projectService.currentProject$,
			environmentService.currentEnvironment$,
			this.currentProjectTenantSlug$,
			this.reloadCurrentService$,
		]).pipe(
			concatMap(([tenant, project, environment, serviceSlug]) =>
				tenant && project && environment && serviceSlug
					? [of(undefined), coreTenantHttpService.get(tenant._id, project._id, environment._id, serviceSlug)]
					: [of(undefined)]
			),
			switchAll(),
			shareReplay(1)
		);
		this.currentProjectTenant$.subscribe();
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Tenant>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.coreTenantHttpService.list(tenant!._id, project!._id, environment!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Tenant | undefined> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.coreTenantHttpService.get(tenant!._id, project!._id, environment!._id, documentId)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Translation>): Observable<false | { _id: Tenant['_id'] }> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.coreTenantHttpService.create(tenant!._id, project!._id, environment!._id, document)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Tenant>): Observable<boolean> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.coreTenantHttpService.update(tenant!._id, project!._id, environment!._id, document)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Tenant>): Observable<false | { _id: Tenant['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(
		document: Partial<Translation> & { _id: Tenant['_id'] },
		options?: { confirm?: boolean; type?: string; identifier?: string }
	): Observable<boolean> {
		const observable = combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.coreTenantHttpService.delete(tenant!._id, project!._id, environment!._id, document._id)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		if (options?.confirm) {
			return this.dialogService.openConfirmDeleteDialog(observable, options.type || 'unknown', options.identifier || 'unknown');
		}
		return observable;
	}
}

import { TestBed } from '@angular/core/testing';

import { CoreTenantService } from './core-tenant.service';

describe('CoreTenantService', () => {
	let service: CoreTenantService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(CoreTenantService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	RouterService,
	UserService,
} from '@kominal/core-angular-client';
import { Environment } from '@kominal/core-common';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, concatMap, filter, first, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { EnvironmentHttpService } from '../../http/environment/environment-http.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class EnvironmentService {
	public reload$ = new BehaviorSubject<void>(undefined);

	public documents$: Observable<Environment[] | undefined>;

	public currentEnvironmentSlug$: Observable<string | undefined>;

	public currentEnvironment$: Observable<Environment | undefined>;

	public reloadCurrentEnvironment$ = new BehaviorSubject<void>(undefined);

	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private environmentHttpService: EnvironmentHttpService,
		private projectService: ProjectService,
		routerService: RouterService
	) {
		this.documents$ = combineLatest([this.userService.currentTenant$, this.reload$]).pipe(
			filter(([tenant]) => !!tenant),
			concatMap(() => [of(undefined), this.list().pipe(map((v) => v.items))]),
			switchAll(),
			shareReplay(2)
		);
		this.currentEnvironmentSlug$ = routerService.observePathParameter('environmentSlug');

		this.currentEnvironment$ = combineLatest([
			userService.currentTenant$,
			projectService.currentProject$,
			this.currentEnvironmentSlug$,
			this.reloadCurrentEnvironment$,
		]).pipe(
			concatMap(([tenant, project, environmentSlug]) =>
				tenant && project && environmentSlug
					? [of(undefined), environmentHttpService.getBySlug(tenant._id, project._id, environmentSlug)]
					: [of(undefined)]
			),
			switchAll(),
			shareReplay(1)
		);
		this.currentEnvironment$.subscribe();
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Environment>> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			filter(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.environmentHttpService.list(tenant!._id, project!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Environment | undefined> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.environmentHttpService.get(tenant!._id, project!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Environment>): Observable<false | { _id: Environment['_id'] }> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.environmentHttpService.create(tenant!._id, project!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Environment>): Observable<boolean> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.environmentHttpService.update(tenant!._id, project!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Environment>): Observable<false | { _id: Environment['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Environment> & { _id: Environment['_id'] }): Observable<boolean> {
		const observable = combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.environmentHttpService.delete(tenant!._id, project!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'environment', document.name || 'unknown');
	}
}

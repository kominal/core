import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { Check, Service } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { CheckHttpService } from '../../http/check/check-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';
import { ServiceService } from '../service/service.service';

@Injectable({
	providedIn: 'root',
})
export class CheckService {
	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService,
		private serviceService: ServiceService,
		private checkHttpService: CheckHttpService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Check>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.checkHttpService.list(tenant!._id, project!._id, environment!._id, service!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Check | undefined> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.checkHttpService.get(tenant!._id, project!._id, environment!._id, service!._id, documentId)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.checkHttpService.create(tenant!._id, project!._id, environment!._id, service!._id, document)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Service>): Observable<boolean> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.checkHttpService.update(tenant!._id, project!._id, environment!._id, service!._id, document)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Service>): Observable<false | { _id: Service['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Service> & { _id: Service['_id'] }): Observable<boolean> {
		const observable = combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.checkHttpService.delete(tenant!._id, project!._id, environment!._id, service!._id, document._id)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'service', document.name || 'unknown');
	}
}

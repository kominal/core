import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { Translation, Voucher } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, map, switchMap } from 'rxjs/operators';
import { VoucherHttpService } from '../../http/voucher/voucher-http.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class VoucherService {
	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private voucherHttpService: VoucherHttpService,
		private projectService: ProjectService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Voucher>> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.voucherHttpService.list(tenant!._id, project!._id, paginationRequest)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Voucher | undefined> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.voucherHttpService.get(tenant!._id, project!._id, documentId)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Voucher>): Observable<false | { _id: Voucher['_id'] }> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.voucherHttpService.create(tenant!._id, project!._id, document)),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Voucher>): Observable<boolean> {
		return combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.voucherHttpService.update(tenant!._id, project!._id, document)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Translation>): Observable<false | { _id: Voucher['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Voucher> & { _id: Voucher['_id'] }): Observable<boolean> {
		const observable = combineLatest([this.userService.currentTenant$, this.projectService.currentProject$]).pipe(
			first(([tenant, project]) => !!tenant && !!project),
			switchMap(([tenant, project]) => this.voucherHttpService.delete(tenant!._id, project!._id, document._id)),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'voucher', document.code || 'unknown');
	}
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	CrudWithTenantHttpService,
	CrudWithTenantService,
	DialogService,
	UserService,
} from '@kominal/core-angular-client';
import { Translation } from '@kominal/core-common';

@Injectable({
	providedIn: 'root',
})
export class TranslationService extends CrudWithTenantService<Translation> {
	public constructor(userService: UserService, dialogService: DialogService, analyticsService: AnalyticsService, httpClient: HttpClient) {
		super(new CrudWithTenantHttpService(httpClient, '/controller', 'translations'), userService, dialogService, analyticsService);
	}
}

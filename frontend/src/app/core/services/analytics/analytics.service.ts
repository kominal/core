import { Injectable } from '@angular/core';
import {
	AnalyticsService as AnalyticsClientService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { Analytics } from '@kominal/core-common';
import { combineLatest, Observable, of } from 'rxjs';
import { catchError, first, switchMap } from 'rxjs/operators';
import { AnalyticsHttpService } from '../../http/analytics/analytics-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';
import { ServiceService } from '../service/service.service';

@Injectable({
	providedIn: 'root',
})
export class AnalyticsService {
	public constructor(
		private userService: UserService,
		private analyticsService: AnalyticsClientService,
		private analyticsHttpService: AnalyticsHttpService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService,
		private serviceService: ServiceService
	) {}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Analytics>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
			this.serviceService.currentService$,
		]).pipe(
			first(([tenant, project, environment, service]) => !!tenant && !!project && !!environment && !!service),
			switchMap(([tenant, project, environment, service]) =>
				this.analyticsHttpService.list(tenant!._id, project!._id, environment!._id, service!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}
}

import { Injectable } from '@angular/core';
import {
	AnalyticsService,
	DialogService,
	EMPTY_PAGINATION_RESPONSE,
	PaginationRequest,
	PaginationResponse,
	UserService,
} from '@kominal/core-angular-client';
import { Environment, Project, Service } from '@kominal/core-common';
import { Report } from '@kominal/core-common/models/report';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { catchError, concatMap, filter, first, map, shareReplay, switchAll, switchMap } from 'rxjs/operators';
import { ReportHttpService } from '../../http/report/report-http.service';
import { EnvironmentService } from '../environment/environment.service';
import { ProjectService } from '../project/project.service';

@Injectable({
	providedIn: 'root',
})
export class ReportService {
	public reload$ = new BehaviorSubject<void>(undefined);

	public documents$: Observable<Report[] | undefined>;

	public constructor(
		private userService: UserService,
		private dialogService: DialogService,
		private analyticsService: AnalyticsService,
		private reportHttpService: ReportHttpService,
		private projectService: ProjectService,
		private environmentService: EnvironmentService
	) {
		this.documents$ = combineLatest([this.userService.currentTenant$, this.reload$]).pipe(
			filter(([tenant]) => !!tenant),
			concatMap(() => [of(undefined), this.list().pipe(map((v) => v.items))]),
			switchAll(),
			shareReplay(2)
		);
	}

	public list(paginationRequest?: PaginationRequest): Observable<PaginationResponse<Report>> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			filter(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.list(tenant!._id, project!._id, environment!._id, paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public listWithProperties(
		options: { paginationRequest?: PaginationRequest; project?: Project; environment?: Environment } = {}
	): Observable<PaginationResponse<Report>> {
		return combineLatest([
			this.userService.currentTenant$,
			options.project ? of(options.project) : this.projectService.currentProject$,
			options.environment ? of(options.environment) : this.environmentService.currentEnvironment$,
		]).pipe(
			filter(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.list(tenant!._id, project!._id, environment!._id, options.paginationRequest)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(EMPTY_PAGINATION_RESPONSE);
			})
		);
	}

	public get(documentId: string): Observable<Report | undefined> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.get(tenant!._id, project!._id, environment!._id, documentId)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of(undefined);
			})
		);
	}

	public create(document: Partial<Service>): Observable<false | { _id: Report['_id'] }> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.create(tenant!._id, project!._id, environment!._id, document)
			),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public update(document: Partial<Report>): Observable<boolean> {
		return combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.update(tenant!._id, project!._id, environment!._id, document)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
	}

	public createOrUpdate(tenant: Partial<Report>): Observable<false | { _id: Report['_id'] }> {
		return tenant._id ? this.update(tenant).pipe(map((r) => (r && tenant._id ? { _id: tenant._id } : false))) : this.create(tenant);
	}

	public delete(document: Partial<Report> & { _id: Report['_id'] }): Observable<boolean> {
		const observable = combineLatest([
			this.userService.currentTenant$,
			this.projectService.currentProject$,
			this.environmentService.currentEnvironment$,
		]).pipe(
			first(([tenant, project, environment]) => !!tenant && !!project && !!environment),
			switchMap(([tenant, project, environment]) =>
				this.reportHttpService.delete(tenant!._id, project!._id, environment!._id, document._id)
			),
			map(() => true),
			catchError((e) => {
				this.analyticsService.handleError(e);
				return of<false>(false);
			})
		);
		return this.dialogService.openConfirmDeleteDialog(observable, 'report', document._id || 'unknown');
	}
}

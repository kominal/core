import { TestBed } from '@angular/core/testing';

import { VoucherHttpService } from './voucher-http.service';

describe('VoucherHttpService', () => {
	let service: VoucherHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(VoucherHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

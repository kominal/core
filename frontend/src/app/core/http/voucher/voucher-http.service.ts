import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Project, Tenant, Voucher } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class VoucherHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Voucher>> {
		return this.httpClient.get<PaginationResponse<Voucher>>(`/controller/tenants/${tenantId}/projects/${projectId}/vouchers`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Voucher['_id']): Observable<Voucher> {
		return this.httpClient.get<Voucher>(
			`/controller/tenants/${tenantId}/projects/${projectId}/vouchers/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Voucher>): Observable<{ _id: Voucher['_id'] }> {
		return this.httpClient.post<{ _id: Voucher['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/vouchers`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Voucher>): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/vouchers/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Voucher['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/vouchers/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

import { TestBed } from '@angular/core/testing';

import { CoreTenantHttpService } from './core-tenant-http.service';

describe('CoreTenantHttpService', () => {
	let service: CoreTenantHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(CoreTenantHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

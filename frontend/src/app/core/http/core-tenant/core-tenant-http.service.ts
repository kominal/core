import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Environment, Project, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class CoreTenantHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Tenant>> {
		return this.httpClient.get<PaginationResponse<Tenant>>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/core-tenants`,
			{
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			}
		);
	}

	public get(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Tenant['_id']
	): Observable<Tenant> {
		return this.httpClient.get<Tenant>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/core-tenants/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Project>
	): Observable<{ _id: Tenant['_id'] }> {
		return this.httpClient.post<{ _id: Tenant['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/core-tenants`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Tenant>
	): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/core-tenants/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Tenant['_id']
	): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/core-tenants/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

import { TestBed } from '@angular/core/testing';

import { ProjectTenantHttpService } from './project-tenant-http.service';

describe('ProjectTenantHttpService', () => {
	let service: ProjectTenantHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ProjectTenantHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

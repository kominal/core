import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Environment, Project, Service, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ProjectTenantHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Service>> {
		return this.httpClient.get<PaginationResponse<Service>>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants`,
			{
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			}
		);
	}

	public get(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Service['_id']
	): Observable<Service> {
		return this.httpClient.get<Service>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Service>
	): Observable<{ _id: Service['_id'] }> {
		return this.httpClient.post<{ _id: Service['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Service>
	): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Service['_id']
	): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public getBySlug(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Project['_id'],
		slug: Service['slug']
	): Observable<Service> {
		return this.httpClient.get<Service>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/tenants/by-slug/${slug}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Environment, Project, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class EnvironmentHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Environment>> {
		return this.httpClient.get<PaginationResponse<Environment>>(`/controller/tenants/${tenantId}/projects/${projectId}/environments`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Environment['_id']): Observable<Environment> {
		return this.httpClient.get<Environment>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		document: Partial<Environment>
	): Observable<{ _id: Environment['_id'] }> {
		return this.httpClient.post<{ _id: Environment['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Environment>): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Environment['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public getBySlug(tenantId: Tenant['_id'], projectId: Project['_id'], slug: Environment['slug']): Observable<Environment> {
		return this.httpClient.get<Environment>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/by-slug/${slug}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

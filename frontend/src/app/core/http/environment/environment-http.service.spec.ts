import { TestBed } from '@angular/core/testing';

import { EnvironmentHttpService } from './environment-http.service';

describe('EnvironmentHttpService', () => {
	let service: EnvironmentHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(EnvironmentHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

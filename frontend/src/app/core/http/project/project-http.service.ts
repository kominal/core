import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED_OPTIONS, CrudWithTenantHttpService } from '@kominal/core-angular-client';
import { Project, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ProjectHttpService extends CrudWithTenantHttpService<Project> {
	public constructor(httpClient: HttpClient) {
		super(httpClient, '/controller', 'projects');
	}

	public getBySlug(tenantId: Tenant['_id'], slug: Project['slug']): Observable<Project> {
		return this.httpClient.get<Project>(
			`${this.baseUrl}/tenants/${tenantId}/${this.collection}/by-slug/${slug}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

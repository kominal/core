import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Project, Tenant, Translation } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ProjectTranslationHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Translation>> {
		return this.httpClient.get<PaginationResponse<Translation>>(`/controller/tenants/${tenantId}/projects/${projectId}/translations`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Translation['_id']): Observable<Translation> {
		return this.httpClient.get<Translation>(
			`/controller/tenants/${tenantId}/projects/${projectId}/translations/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Project>): Observable<{ _id: Translation['_id'] }> {
		return this.httpClient.post<{ _id: Translation['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/translations`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Translation>): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/translations/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: Translation['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/translations/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

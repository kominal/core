import { TestBed } from '@angular/core/testing';

import { ProjectTranslationHttpService } from './project-translation-http.service';

describe('ProjectTranslationHttpService', () => {
	let service: ProjectTranslationHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ProjectTranslationHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

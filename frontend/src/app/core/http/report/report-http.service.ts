import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Environment, Project, Tenant } from '@kominal/core-common';
import { Report } from '@kominal/core-common/models/report';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ReportHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Report>> {
		return this.httpClient.get<PaginationResponse<Report>>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/reports`,
			{
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			}
		);
	}

	public get(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Report['_id']
	): Observable<Report> {
		return this.httpClient.get<Report>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/reports/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Report>
	): Observable<{ _id: Report['_id'] }> {
		return this.httpClient.post<{ _id: Report['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/reports`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		document: Partial<Report>
	): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/reports/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		documentId: Report['_id']
	): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/reports/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

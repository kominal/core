import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { File, Project, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class FileHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<File>> {
		return this.httpClient.get<PaginationResponse<File>>(`/controller/tenants/${tenantId}/projects/${projectId}/files`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: File['_id']): Observable<File> {
		return this.httpClient.get<File>(
			`/controller/tenants/${tenantId}/projects/${projectId}/files/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<Project>): Observable<{ _id: File['_id'] }> {
		return this.httpClient.post<{ _id: File['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/files`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<File>): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/files/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: File['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/files/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

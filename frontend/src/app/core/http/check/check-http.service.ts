import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { Check, Environment, Project, Service, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class CheckHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Check>> {
		return this.httpClient.get<PaginationResponse<Check>>(
			// eslint-disable-next-line max-len
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/checks`,
			{
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			}
		);
	}

	public get(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		documentId: Check['_id']
	): Observable<Check> {
		return this.httpClient.get<Check>(
			// eslint-disable-next-line max-len
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/checks/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		document: Partial<Check>
	): Observable<{ _id: Check['_id'] }> {
		return this.httpClient.post<{ _id: Check['_id'] }>(
			// eslint-disable-next-line max-len
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/checks`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		document: Partial<Check>
	): Observable<void> {
		return this.httpClient.put<void>(
			// eslint-disable-next-line max-len
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/checks/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		documentId: Check['_id']
	): Observable<void> {
		return this.httpClient.delete<void>(
			// eslint-disable-next-line max-len
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/checks/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

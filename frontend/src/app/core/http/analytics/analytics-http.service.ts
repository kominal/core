import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/core-angular-client';
import { Analytics, Environment, Project, Service, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class AnalyticsHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		environmentId: Environment['_id'],
		serviceId: Service['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<Analytics>> {
		return this.httpClient.get<PaginationResponse<Analytics>>(
			`/controller/tenants/${tenantId}/projects/${projectId}/environments/${environmentId}/services/${serviceId}/analytics`,
			{
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			}
		);
	}
}

import { TestBed } from '@angular/core/testing';

import { AnalyticsHttpService } from './analytics-http.service';

describe('AnalyticsHttpService', () => {
	let service: AnalyticsHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(AnalyticsHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

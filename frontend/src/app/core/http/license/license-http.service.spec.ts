import { TestBed } from '@angular/core/testing';

import { LicenseHttpService } from './license-http.service';

describe('LicenseHttpService', () => {
	let service: LicenseHttpService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(LicenseHttpService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});

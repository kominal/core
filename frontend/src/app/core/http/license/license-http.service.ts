import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
	AUTHENTICATION_REQUIRED,
	AUTHENTICATION_REQUIRED_OPTIONS,
	PaginationRequest,
	PaginationResponse,
	toPaginationParams,
} from '@kominal/core-angular-client';
import { License, Project, Tenant } from '@kominal/core-common';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class LicenseHttpService {
	public constructor(protected httpClient: HttpClient) {}

	public list(
		tenantId: Tenant['_id'],
		projectId: Project['_id'],
		paginationRequest?: PaginationRequest
	): Observable<PaginationResponse<License>> {
		return this.httpClient.get<PaginationResponse<License>>(`/controller/tenants/${tenantId}/projects/${projectId}/licenses`, {
			params: toPaginationParams(paginationRequest),
			headers: AUTHENTICATION_REQUIRED,
		});
	}

	public get(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: License['_id']): Observable<License> {
		return this.httpClient.get<License>(
			`/controller/tenants/${tenantId}/projects/${projectId}/licenses/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public create(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<License>): Observable<{ _id: License['_id'] }> {
		return this.httpClient.post<{ _id: License['_id'] }>(
			`/controller/tenants/${tenantId}/projects/${projectId}/licenses`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public update(tenantId: Tenant['_id'], projectId: Project['_id'], document: Partial<License>): Observable<void> {
		return this.httpClient.put<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/licenses/${document._id}`,
			document,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}

	public delete(tenantId: Tenant['_id'], projectId: Project['_id'], documentId: License['_id']): Observable<void> {
		return this.httpClient.delete<void>(
			`/controller/tenants/${tenantId}/projects/${projectId}/licenses/${documentId}`,
			AUTHENTICATION_REQUIRED_OPTIONS
		);
	}
}

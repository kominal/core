export interface Role {
	_id: string;
	tenantId: string;
	name: string;
	permissions: string[];
	default: boolean;
}

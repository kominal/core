export interface Tenant {
	_id: string;
	tenantId: string;
	projectId: string;
	environmentId: string;
	name: string;
	slug: string;
	enableEmailVerification: boolean;
	jsonWebTokenKey: string;
	invoiceNumber: number;
	stripePublishableKey?: string;
	stripeSecretKey?: string;
	stripeCustomerId?: string;
	defaultPaymentMethodId?: string;
	licenses: { licenseId: string; added: Date; removed?: Date; invoicedUntil?: Date }[];
	vouchers: { voucherId: string; added: Date; removed?: Date }[];
	companyName: string;
	street: string;
	postcode: string;
	city: string;
}

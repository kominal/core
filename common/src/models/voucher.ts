export interface Voucher {
	_id: string;
	code: string;
	type: string;
	discountType: string;
	discountAmount: number;
}

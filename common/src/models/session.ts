export interface Session {
	_id: string;
	environmentId: string;
	userId: string;
	keyHash: string;
	device: string;
	created: Date;
	lastActive: Date;
}

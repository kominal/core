import { Environment } from './environment';
import { Project } from './project';
import { Tenant } from './tenant';

export interface Report {
	_id: string;
	tenantId: Tenant['_id'];
	projectId: Project['_id'];
	environmentId: Environment['_id'];
	emails: string[];
	events: { serviceId: string; types: string[] }[];
	lastUpdated: Date;
}

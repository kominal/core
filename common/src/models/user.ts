export interface User {
	_id: string;
	tenantId: string;
	email: string;
	passwordHash: string;
	verified: boolean;
	token: string | undefined;
	roles: string[];
}

export interface File {
	_id: string;
	tenantId: string;
	projectId: string;
	type: string;
	key: string;
	value: any;
}

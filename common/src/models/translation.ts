export interface Translation {
	_id: string;
	tenantId: string;
	projectId?: string;
	type: string;
	key: string;
	values: { language: string; value: string }[];
}

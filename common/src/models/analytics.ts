export interface Analytics {
	_id: string;
	tenantId: string;
	projectId: string;
	environmentId: string;
	serviceId: string;
	checkId?: string;
	taskId?: string;
	time: Date;
	type: string;
	level: string;
	content: any;
}

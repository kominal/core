export interface License {
	_id: string;
	tenantId: string;
	projectId: string;
	name: string;
	price: number;
	tax: number;
}

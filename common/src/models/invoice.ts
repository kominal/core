export interface InvoiceLine {
	description: string;
	amount: number;
	price: number;
	tax: number;
}

export interface LicenseInvoiceLine extends InvoiceLine {
	licenseId: string;
	from: Date;
	until: Date;
}

export interface Invoice {
	_id: string;
	tenantId: string;
	number: string;
	time: Date;
	status: string;
	lines: InvoiceLine[];
}

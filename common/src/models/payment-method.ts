export type PaymentMethodType = 'STRIPE';

export interface PaymentMethod {
	_id: string;
	tenantId: string;
	type: PaymentMethodType;
	details: any;
}

export interface Check {
	_id: string;
	tenantId: string;
	projectId: string;
	environmentId: string;
	serviceId: string;
	type: string;
	configuration: any;
	status: 'OK' | 'WARN' | 'CRIT';
	interval: number;
	active: boolean;
	lastActivity: Date;
}

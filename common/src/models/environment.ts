export interface Environment {
	_id: string;
	tenantId: string;
	projectId: string;
	name: string;
	slug: string;
	tokens: string[];
}

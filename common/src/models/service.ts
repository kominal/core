export interface Service {
	_id: string;
	tenantId: string;
	projectId: string;
	environmentId: string;
	name: string;
	slug: string;
	tokens: string[];
}

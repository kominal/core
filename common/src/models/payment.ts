export interface Payment {
	_id: string;
	tenantId: string;
	type: string;
	name: string;
	companyName: string;
	details: any;
}
